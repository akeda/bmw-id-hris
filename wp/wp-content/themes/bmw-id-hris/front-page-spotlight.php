<?php
$spotlight_news = new WP_Query( array(
	'post_type'   => HRIS_Spotlight_News_Post_Type::NAME,
) );
?>
<?php if ( $spotlight_news->have_posts() ) : ?>
<h2><?php _e( 'Spotlight News', 'bmw-id-hris' ) ?></h2>
<div id="spotlight-news-carousel" class="spotlight-news carousel slide" data-ride="carousel">
	<div class="carousel-inner">
	<?php $slide_to = 0; ?>
	<?php while ( $spotlight_news->have_posts() ) : $spotlight_news->the_post(); ?>
		<?php $slide_class = ( $slide_to++ ) ? 'item' : 'item active'; ?>
		<div class="<?php echo esc_attr( $slide_class ) ?>">
			<?php the_post_thumbnail( 'large' ); ?>
			<div class="carousel-caption">
				<h3><?php the_title(); ?></h3>
				<?php echo the_content(); ?>
			</div>
		</div>
	<?php endwhile; ?>
	</div>
	<!-- / carousel-inner -->

	<a class="left carousel-control" href="#spotlight-news-carousel" data-slide="prev">
		<span class="glyphicon glyphicon-chevron-left"></span>
	</a>
	<a class="right carousel-control" href="#spotlight-news-carousel" data-slide="next">
		<span class="glyphicon glyphicon-chevron-right"></span>
	</a>
</div>
<!-- / spotlight-news -->
<?php endif; ?>

<?php wp_reset_postdata(); ?>
