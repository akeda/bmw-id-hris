<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package BMW Indonesia HRIS
 */
?>

	</div><!-- #content -->

</div><!-- #page -->

<footer id="colophon" class="site-footer" role="contentinfo">
	<div class="site-info">
		<?php do_action( 'bmw_id_hris_credits' ); ?>
		<?php printf( __( 'Copyright &copy; 2013 %1$s.', 'bmw-id-hris' ), 'BMW Indonesia HRIS' ); ?>
	</div><!-- .site-info -->
</footer><!-- #colophon -->

<?php wp_footer(); ?>

</body>
</html>