<?php
/**
 * BMW Indonesia HRIS configuration.
 *
 * @package BMW Indonesia HRIS
 */

return array(
	'theme_support' => array(
		/**
		 * Add default posts and comments RSS feed links to head
		 */
		'automatic-feed-links' => true,

		/**
		 * Enable support for Post Formats
		 */
		'post-formats' => array_fill_keys( array( 'aside', 'image', 'video', 'quote', 'link' ), true ),

		/**
		 * Enable support for Post Thumbnails on posts and pages
		 *
		 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
		 */
		'post-thumbnails' => true,

		/**
		 * Core custom background feature
		 */
		'custom-background' => array(
			'default-color' => 'ffffff',
			'default-image' => '',
		),

		'html5' => array_fill_keys( array( 'comment-form', 'comment-list', ), true ),

	),

	'image_sizes' => array(
		'article_thumbnail' => array(
			'width'  => 300,
			'height' => 150,
			'crop'   => true,
		),
		'spotlight_image' => array(
			'width'  => 653,
			'height' => 250,
			'crop'   => true,
		),
	// 	'archive_half' => array(
	// 		'width'  => 915,
	// 		'height' => 610,
	// 		'crop'   => true,
	// 	),
	// 	'article_thumbnail' => array(
	// 		'width'  => 258,
	// 		'height' => 212,
	// 		'crop'   => true,
	// 	),
	// 	'artist_thumbnail' => array( // used in related artists
	// 		'width'  => 150,
	// 		'height' => 143,
	// 		'crop'   => true,
	// 	),
	// 	'gallery_large' => array(
	// 		'width'  => 768,
	// 		'height' => 520,
	// 		'crop'   => false,
	// 	),
	// 	'gallery_medium' => array(
	// 		'width'  => 300,
	// 		'height' => 200,
	// 		'crop'   => true,
	// 	),
	// 	'gallery_small' => array(
	// 		'width'  => 146,
	// 		'height' => 100,
	// 		'crop'   => true,
	// 	),
	),

	'menus' => array(
		'primary' => __( 'Primary Menu', 'bmw-id-hris' ),
		'footer'  => __( 'Footer Menu', 'bmw-id-hris' ),
	),

	'sidebars' => array(
		'main' => array(
			'name'          => __( 'Main Sidebar', 'bmw-id-hris' ),
			'description'   => __( 'Left hand sidebar.', 'bmw-id-hris' ),
			'before_title'  => '<h2>',
			'after_title'   => '</h2>',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
		),
		'secondary' => array(
			'name'          => __( 'Secondary Sidebar', 'bmw-id-hris' ),
			'description'   => __( 'Right hand sidebar.', 'bmw-id-hris' ),
			'before_title'  => '<h2>',
			'after_title'   => '</h2>',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
		),
	),

	'styles' => array(
		'hris-bootstrap' => array(
			'src'     => get_template_directory_uri() . '/css/bootstrap.min.css',
			'enqueue' => true,
		),
		'hris-style' => array(
			'src'     => get_template_directory_uri() . '/css/main.css',
			'enqueue' => true,
			'deps'    => array( 'hris-bootstrap' ),
		),
	),

	'scripts' => array(
		'hris-jquery' => array(
			'src'       => get_template_directory_uri() . '/js/jquery-1.10.2.min.js',
			'enqueue'   => true,
			'in_footer' => true,
		),
		'hris-bootstrap-js' => array(
			'src'       => get_template_directory_uri() . '/js/bootstrap.min.js',
			'enqueue'   => true,
			'in_footer' => true,
			'deps'      => array( 'hris-jquery' ),
		),
		'hris-main' => array(
			'src'       => get_template_directory_uri() . '/js/main.js',
			'enqueue'   => true,
			'in_footer' => true,
			'deps'      => array( 'hris-jquery', 'hris-bootstrap-js' ),
		),
	),

);
