<?php

$post_type           = 'post';
$featured_lineup_id  = 'homepage';
$other_wp_query_args = array();

$lineup = XTeam_Lineup::get_featured_lineup( $post_type, $featured_lineup_id, $other_wp_query_args );
?>

<?php if ( $lineup->have_posts() ) : ?>

	<?php /* Start the Loop */ ?>
	<?php while ( $lineup->have_posts() ) : $lineup->the_post(); ?>
		<?php get_template_part( 'content-front', get_post_format() ); ?>
	<?php endwhile; ?>

<?php else : ?>

	<?php get_template_part( 'content', 'none' ); ?>

<?php endif; ?>

<?php wp_reset_postdata(); ?>
