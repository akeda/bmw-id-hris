<?php
/**
 * The Sidebar containing the main widget areas.
 *
 * @package BMW Indonesia HRIS
 */
?>
	<div id="sidebar-main" class="widget-area col-md-2 col-md-pull-7" role="complementary">
		<?php do_action( 'before_sidebar' ); ?>
		<?php dynamic_sidebar( 'main' ); ?>
	</div><!-- #sidebar-main -->

	<div id="sidebar-secondary" class="widget-area col-md-3" role="complementary">
		<?php do_action( 'before_sidebar' ); ?>
		<?php dynamic_sidebar( 'secondary' ); ?>
	</div><!-- #sidebar-secondary -->
