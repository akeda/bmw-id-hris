<?php
/**
 * BMW Indonesia HRIS functions and definitions
 *
 * @package BMW Indonesia HRIS
 */

$theme_config = array();

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function bmw_id_hris_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on BMW Indonesia HRIS, use a find and replace
	 * to change 'bmw-id-hris' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'bmw-id-hris', get_template_directory() . '/languages' );

	global $theme_config;
	$theme_config = new WP_Config_Array( include( TEMPLATEPATH . '/config.php' ) );
	if ( TEMPLATEPATH !== STYLESHEETPATH && file_exists( STYLESHEETPATH . '/config.php' ) ) {
		$theme_config->extend( include( STYLESHEETPATH . '/config.php' ) );
	}
	// @todo The configs should indicate which configs they extend

	$theme_config = apply_filters( 'grammy_theme_config', $theme_config );

	foreach ( $theme_config['theme_support'] as $feature => $options ) {
		if ( $options === false ) {
			remove_theme_support( $feature );
		}
		else if ( is_array($options) ) {
			// Convert mergeable associative array into ordered array
			if ( ! isset( $options[0] ) && in_array( $feature, array( 'post-formats', 'html5' ) ) ) {
				$options = array_keys( array_filter( $options ) );
			}
			add_theme_support($feature, $options);
		}
		else {
			add_theme_support($feature);
		}
	}

	register_nav_menus( $theme_config['menus'] );

	foreach ( $theme_config['image_sizes'] as $name => $size_info ) {
		extract( array_merge(
			compact( 'name' ),
			array(
				'crop' => false,
				'width' => 9999,
				'height' => 9999,
			),
			$size_info
		));
		add_image_size( $name, $width, $height, $crop );
	}

	add_filter( 'head_meta_tags', function ( $meta ) {
		$meta['viewport'] = 'width=device-width,initial-scale=1.0';
		return $meta;
	});

}
add_action( 'after_setup_theme', 'bmw_id_hris_setup' );

/**
 * Register widgetized area and update sidebar with default widgets.
 */
function bmw_id_hris_widgets_init() {
	global $theme_config;

	foreach ( $theme_config['sidebars'] as $id => $options ) {
		$options += compact('id');
		register_sidebar($options);
	}
}
add_action( 'widgets_init', 'bmw_id_hris_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function bmw_id_hris_scripts() {
	global $theme_config;

	$default_args = array(
		'deps'      => array(),
		'ver'       => null,
		'in_footer' => false,
		'enqueue'   => false,
	);
	$home_host = parse_url( home_url(), PHP_URL_HOST );
	foreach ( $theme_config['scripts'] as $handle => $args ) {
		$args = array_merge( $default_args, $args );
		if ( ! empty( $args['src'] ) ) {
			if ( empty( $args['ver'] ) && parse_url( $args['src'], PHP_URL_HOST ) === $home_host ) {
				$args['ver'] = filemtime( ABSPATH . parse_url( $args['src'], PHP_URL_PATH ) );
			}
			wp_register_script(
				$handle,
				$args['src'],
				$args['deps'],
				$args['ver'],
				$args['in_footer']
			);
		}
		if ( is_callable( $args['enqueue'] ) ) {
			$args['enqueue'] = call_user_func( $args['enqueue'] );
		}
		if ( $args['enqueue'] ) {
			wp_enqueue_script( $handle );
		}
	}
}
add_action( 'wp_enqueue_scripts', 'bmw_id_hris_scripts' );

/**
 * Register and enqueue styles
 */
function bmw_id_hris_styles() {
	global $theme_config;

	$default_args = array(
		'deps'    => array(),
		'ver'     => null,
		'enqueue' => false,
		'media'   => 'all',
	);
	$home_host = parse_url( home_url(), PHP_URL_HOST );
	foreach ( $theme_config['styles'] as $handle => $args ) {
		$args = array_merge( $default_args, $args );
		if ( ! empty( $args['src'] ) ) {
			if ( empty( $args['ver'] ) && parse_url( $args['src'], PHP_URL_HOST ) === $home_host ) {
				$args['ver'] = filemtime( ABSPATH . parse_url( $args['src'], PHP_URL_PATH ) );
			}
			wp_register_style(
				$handle,
				$args['src'],
				$args['deps'],
				$args['ver'],
				$args['media']
			);
		}
		if ( is_callable( $args['enqueue'] ) ) {
			$args['enqueue'] = call_user_func( $args['enqueue'] );
		}
		if ( $args['enqueue'] ) {
			wp_enqueue_style( $handle );
		}
	}
}
add_action( 'wp_enqueue_scripts', 'bmw_id_hris_styles' );

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';
