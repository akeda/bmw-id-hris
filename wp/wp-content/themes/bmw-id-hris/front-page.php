<?php
/**
 * Front page template.
 *
 * @package BMW Indonesia HRIS
 */

get_header(); ?>

	<div id="primary" class="content-area col-md-7 col-md-push-2">
		<main id="main" class="site-main" role="main">
		<?php get_template_part( 'front-page', 'lineup' ); ?>
		</main>
		<!-- #main -->

		<?php get_template_part( 'front-page', 'spotlight' ); ?>
	</div>
	<!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
