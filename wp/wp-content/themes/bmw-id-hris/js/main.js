(function($){

var HRIS = {
	init: function() {
		$('html').removeClass('no-js').addClass('js');

		this.wigetCategoriesCollapsibleTree();
	},

	/**
	 * On each category checks if category has children or not. If yes, alter the
	 * class.
	 *
	 * @link http://cssdeck.com/labs/twitter-bootstrap-plain-collapsible-tree-menu
	 */
	wigetCategoriesCollapsibleTree: function() {
		var $ul = $('.widget_categories ul'),
				$li = $('li', $ul);

		$li.each(function() {
			var $l = $(this),
					$c = $('ul:first', $l);

			// This category has children
			if ($c.length) {
				var $a = $('a:first', $l);

				// Adds .tree-toggler class
				$a.addClass('tree-toggler');

				$l.prepend('<span class="glyphicon glyphicon-circle-arrow-right"></span>');

				// Adds .tree class to ul
				$c.addClass('tree');
			} else {
				$l.prepend('<span class="glyphicon glyphicon-minus"></span>');
			}

		});

		// Binds a handler to toggle .tree-toggler.
		$('.tree-toggler').on('click', $ul, function(e) {
			e.preventDefault();

			var $a = $(this);
			$a.parent().children('ul.tree').toggle( 0, function() {
				$a.toggleClass('collapsed');
				$(this).toggleClass('collapsed');
			});
		}).trigger('click');


		// Show when active.
		var $current_active_cat = $('.current-cat:first');
		if ( $current_active_cat.length ) {
			var $parent = $current_active_cat.parent(),
					maxIteration = 10,
					iteration = 0;

			// Exapand the tree of all parents.
			while ( ! $parent.hasClass( 'widget_categories' ) && iteration < maxIteration ) {

				if ( $parent.hasClass('cat-item')  ) {
					var $tree = $('.tree:first', $parent),
							$toggler = $('.tree-toggler:first', $parent);

					if ( $tree.length && $toggler.length ) {
						$toggler.trigger('click');
					}
				}

				$parent = $parent.parent();
				iteration++;
			}
		}
	}
}; // HRIS

$(function() {
	HRIS.init();
});

}(jQuery));
