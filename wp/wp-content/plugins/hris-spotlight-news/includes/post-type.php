<?php

class HRIS_Spotlight_News_Post_Type {

	const NAME = 'hris_spotlight_news';

	public function load() {
		// Register custom post type on the 'init' hook.
		add_action( 'init', array( $this, 'register_post_type' ) );
	}

	/**
	 * Registers post type needed by this plugin.
	 *
	 * @since 0.1.0
	 * @access public
	 * @return void
	 */
	public function register_post_type() {
		// Set up the arguments for the portfolio item post type.
		$args = array(
			'description'         => '',
			'public'              => true,
			'publicly_queryable'  => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'exclude_from_search' => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'menu_position'       => 5,
			'can_export'          => true,
			'delete_with_user'    => true,
			'hierarchical'        => false,
			'has_archive'         => false,
			'query_var'           => false,

			// What features the post type supports.
			'supports' => array(
				'author', 'title', 'editor', 'thumbnail',
			),

			// Labels used when displaying the posts.
			'labels' => array(
				'name'               => __( 'Spotlight news',                   'hris-spotlight-news' ),
				'singular_name'      => __( 'Spotlight news',                   'hris-spotlight-news' ),
				'menu_name'          => __( 'Spotlight news',                   'hris-spotlight-news' ),
				'name_admin_bar'     => __( 'Spotlight news',                   'hris-spotlight-news' ),
				'add_new'            => __( 'Add New',                          'hris-spotlight-news' ),
				'add_new_item'       => __( 'Add New Spotlight news',           'hris-spotlight-news' ),
				'edit_item'          => __( 'Edit Spotlight news',              'hris-spotlight-news' ),
				'new_item'           => __( 'New Spotlight news',               'hris-spotlight-news' ),
				'view_item'          => __( 'View Spotlight news',              'hris-spotlight-news' ),
				'search_items'       => __( 'Search Spotlight news',            'hris-spotlight-news' ),
				'not_found'          => __( 'No spotlight news found',          'hris-spotlight-news' ),
				'not_found_in_trash' => __( 'No spotlight news found in trash', 'hris-spotlight-news' ),
				'all_items'          => __( 'Spotlight news',                   'hris-spotlight-news' ),
			)
		);

		// Register the post type.
		register_post_type( self::NAME, $args );
	}

}
