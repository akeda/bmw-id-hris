(function($){


	var self = {
		submitButton: '<div><input type="submit" class="button primary" value="Submit"></div>',

		// Key is field in a row, and value is total field (at the bottom).
		totalMaps: {
			'[data-field_name="amount"] .input': '#acf-field-total_amount',
			'[data-field_name="review"] .input': '#acf-field-total_review'
		},

		init: function() {
			self.addSubmitButton();
			self.removeElements();
			self.hideElements();
			self.bindEvents();
			self.adjustTotalFields();
		},

		removeElements: function() {
			$('#postbox-container-1').remove();
		},

		hideElements: function() {
			// Hides header and total_review.
			$('.acf-th-review').hide();
			$('.acf-th-remarks').hide();
			$('.acf-th-approval').hide();
			$('.acf-th-reason').hide();
			$('#acf-field-total_review').hide();
			$('[data-field_name="review"]').hide();
			$('[data-field_name="remarks"]').hide();
			$('[data-field_name="approval"]').hide();
			$('[data-field_name="reason"]').hide();
		},

		addSubmitButton: function() {
			$('.acf_postbox .inside').append( self.submitButton );
		},

		bindEvents: function() {
			// Binds blur event on amount and review fields to calculate total.
			$.each( self.totalMaps, function(k, v) {
				$(document).on('blur', k, {rowField: k, totalField: v}, self.calculateTotal);
			} );

			// Binds jQuery number to total fields.
			if ('function' === typeof $.fn.number) {
				$.each( self.totalMaps, function(k, v) {
					$(v).number(true, 2, ',', '.');
				} );
			}

			// Bind row removal in repeater type.
			$(document).on('click', '.acf-button-remove', function(e) {
				var $tr = $(e.target).closest('tr');

				e.data = e.data || {};

				$.each( self.totalMaps, function(k, v) {
					// Removes row fields immediately
					$(k, $tr).remove();

					e.data.rowField   = k;
					e.data.totalField = v;
					self.calculateTotal(e);
				} );
			});
			if (HRIS_MEDICAL_LAYOUT.hr_review) {
				
				
				$(document).on('click', '.primary', function() {

					var eachEl = $('[data-field_name="review"] input').filter(':visible');
					
					$.each( eachEl, function(m, n) {
						if($(n).val().length < 1 ) {
							alert("Field HR-Review Can't Empty!!");
							return false;
						}
					} );

					//console.log( eachEl );return false;


				});

			}
			

		},

		/**
		 * Calculate total of amount and review in rows.
		 */
		calculateTotal: function(e) {
			var $rowFields = $(e.data.rowField).filter( ':visible' ),
					total = 0;

			$rowFields.each( function() {
				total += ( $(this).val() * 1 );
			} );

			$( e.data.totalField ).val( total );
		},



		/**
		 * Reposition total fields and set it readonly.
		 */
		adjustTotalFields: function() {
			var total = ['total_amount', 'total_review'];

			$.each(total, function(i, v) {
				var field = $('.acf-input-wrap', '#acf-' + v),
						total = $('#acf-' + v),
						total_field = $('#acf-field-' + v);

				// Move total field inside field's input wrapper.
				if ( field.length && total_field.length ) {
					if ( v == 'total_review' ) {
						$('.acf-input-wrap', '#acf-total_amount').append(total_field);
						total_field.css('margin-left', 20);
					} else {
						total_field.css('margin-left', 360);
					}
					total_field.attr('readonly','readonly');
				}
			});
		}
	};

	$(self.init);

}(jQuery));
