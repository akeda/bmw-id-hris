(function($){

	acf = acf || {};
	acf.fields = acf.fields || {};

	acf.fields.author_family = {
		init: function() {
			acf.fields.author_family.bindChange(document);
		},

		bindChange: function(container) {
			$('.acf-author_family', container).each( function() {
				var $target_id = $('select', this);

				$target_id.on('change', acf.fields.author_family.changeHandler).trigger('change');
			} );
		},

		changeHandler: function(e) {
			var $target_id = $(e.target),
					$option = $('option:selected', $target_id),
					$target_name = $target_id.parent().find('.target_name'),
					$target_type = $target_id.parent().find('.target_type');

			$target_name.val( $option.text() );
			$target_type.val( $option.data('target_type') );
		},

	};

	// Init!
	$(acf.fields.author_family.init);

}(jQuery));
