<?php

/**
 * Setting limit for this post type.
 *
 * @package HRIS Medical
 * @since 0.1.0
 * @author Akeda Bagus <admin@gedex.web.id>
 */
class HRIS_Medical_Setting_Limit implements HRIS_Medical_Component_Interface {

	const NAME = 'hris_medical_setting';
	const SLUG = 'hris_medical_setting';

	/**
	 * Setting options.
	 *
	 * @since 0.1.0
	 * @var array
	 */
	private $options;

	public function __construct() {
		$this->options = array(
			'parent_slug'   => sprintf( 'edit.php?post_type=%s', HRIS_Medical_Post_Type::NAME ),
			'page_title'    => __( 'Limit Settings', 'hris-medical' ),
			'menu_title'    => __( 'Limit Settings', 'hris-medical' ),
			'capability'    => 'manage_hris_medical',
			'menu_slug'     => self::SLUG,
			'option_group'  => self::SLUG,
			'option_name'   => self::SLUG,
			'section_page'  => self::SLUG,
			'section_id'    => 'default',
			'section_title' => '',
			'fields'        => array(
				'medical_limit_users' => array(
					'title'   => __( 'Limit per employee', 'hris-medical' ),
					'type'    => 'medical_limit_matrix',
				),
			),
		);
	}

	/**
	 * Called by plugin's main-file in plugins_load action.
	 *
	 * @since 0.1.0
	 * @return void
	 */
	public function load() {
		add_action( 'admin_menu', array( $this, 'register_setting_menu' ) );
		add_action( 'admin_init', array( $this, 'register_settings' ) );

		add_filter( 'option_page_capability_hris_medical_setting', array( $this, 'cap_for_setting_page' ) );

		add_action('admin_enqueue_scripts', array( $this, 'scripts_and_styles' ) );
	}

	/**
	 * Register setting menu.
	 *
	 * @since 0.1.0
	 * @action admin_menu
	 * @return void
	 */
	public function register_setting_menu() {

		// See vars passed into add_submenu_page
		extract( $this->options );

		add_submenu_page(
			$parent_slug,
			$page_title,
			$menu_title,
			$capability,
			$menu_slug,
			array( $this, 'setting_page_callback' )
		);

	}

	/**
	 * Method that calls various WordPress Settings API functions. This method registers
	 * section, setting and fields.
	 *
	 * @since 0.1.0
	 * @action admin_init
	 * @return void
	 */
	public function register_settings() {

		extract( $this->options );
		
		// Adds section to group the fields.
		add_settings_section( $section_id, $section_title, '__return_empty_string', $section_page );

		$sanitize_callback = array( $this, 'sanitize_callback' );

		foreach ( $fields as $field => $prop ) {
			register_setting( $option_group, $option_name, $sanitize_callback );

			$field_name     = sprintf( '%s[%s]', $option_name, $field );
			$field_callback = array( $this, 'field_' . $prop['type'] );
			$field_args     = array(
				'name'  => $field_name,
				'id'    => $field_name,
				'key'   => $field,
				'desc'  => isset( $prop['desc'] ) ? $prop['desc'] : '',
				'value' => $this->_get_setting( $field ),
			);

			if ( $prop['type'] === 'select' ) {
				$field_args['options'] = ( isset( $prop['options'] ) && is_array( $prop['options'] ) ) ? $prop['options'] : array();
			}
			add_settings_field( $field, $prop['title'], $field_callback, $menu_slug, $section_id, $field_args );

			// Custom sanitize callback for field
			$hook = 'on_sanitizing_' . $field;
			if ( method_exists( $this, $hook ) ) {
				add_action( $hook, array( $this, $hook ), 10, 2 );
			}
		}
	}

	public function cap_for_setting_page() {
		return $this->options['capability'];
	}

	/**
	 * Render setting page. Callback for 'add_submenu_page'.
	 *
	 * @since 0.1.0
	 * @return void
	 */
	public function setting_page_callback() {
		// Automatically called in options-head.php
		if ( get_current_screen()->parent_base !== 'options-general' ) {
			settings_errors();
		}

		extract( $this->options );
		?>
		<div class="wrap">
		<?php screen_icon( 'options-general' ); ?>
		<h2><?php echo esc_html( $page_title ); ?></h2>
			<form action="options.php" method="post">
				<?php settings_fields( $option_group ); ?>
				<?php do_settings_sections( self::SLUG ); ?>
				<?php submit_button(); ?>
			</form>
		</div>
		<?php
	}


	/**
	 * Callback for `add_settings_field` where field's type is select.
	 *
	 * @since 0.1.0
	 * @param array $args Field args passed to `add_settings_field` call
	 * @return void
	 */
	public function field_select( $args ) {
		extract( $args );
		if ( ! isset( $args['options'] ) )
			$options = array();
		?>
		<select id="<?php echo esc_attr( $id ); ?>" name="<?php echo esc_attr( $name ); ?>">
			<?php foreach ($options as $option_val => $option_label): ?>
				<option value="<?php echo esc_attr( $option_val ); ?>" <?php selected( $option_val, $value ); ?>>
					<?php echo esc_html( $option_label ); ?>
				</option>
			<?php endforeach; ?>
		</select>
		<?php if ( $desc ): ?>
		<p class="description"><?php echo esc_html( $desc ); ?></p>
		<?php endif;
	}

	/**
	 * Callback for `add_settings_field` where field's type is text.
	 *
	 * @since 0.1.0
	 * @param array $args Field args passed to `add_settings_field` call
	 * @return void
	 */
	public function field_text( $args ) {
		extract( $args );

		if ( ! isset( $args['type'] ) )
			$type = 'text';
		?>
		<input
			type="<?php echo esc_attr( $type ) ?>"
			name="<?php echo esc_attr( $name ) ?>"
			id="<?php echo esc_attr( $id ) ?>"
			class="regular-text limit-setting"
			value="<?php echo esc_attr( $value ); ?>">
		<?php if ( $desc ): ?>
		<p class="description"><?php echo esc_html( $desc ); ?></p>
		<?php endif;
	}

	/**
	 * Callback for `add_settings_field` where field's type is number.
	 *
	 * @since 0.1.0
	 * @param array $args Field args passed to `add_settings_field` call
	 * @return void
	 */
	public function field_number( $args ) {
		$args['type'] = 'number';
		$this->field_text( $args );
	}

	/**
	 * Callback for `add_settings_field` where field's type is medical_limit_matrix.
	 *
	 * @since 0.1.0
	 * @param array $args Field args passed to `add_settings_field` call
	 * @return void
	 */
	public function field_medical_limit_matrix( $args ) {
		extract( $args );
		//temporary manual create need confirmation with akeda 
		$category = array('out-patient','in-patient','frame','lense',
					 'biofocus-lense','soft-lense','maternity-norm','maternity-sc');
		$categories = get_terms( HRIS_Medical_Type_Taxonomy::NAME, array(
 			'orderby'    => 'count',
 			'hide_empty' => 0,
 			'fields'        => 'all', 
 		) );

 		
		$users      = get_users();
		
		$field_name = '';
		$values     = $this->_get_setting( 'medical_limit_users' );
		
		$constCat   = $this->_constCat( $categories );
		 
		
		?>
		<div>Employee Limit Settings</div>
		<table class="widefat form-table">
			<thead>

				<tr>
					<th rowspan="3">Name</th>
					<th class="mid-level" colspan="8">Limit</th>
				</tr>
				<tr>
					<th colspan="2">Medical</th>
					<th colspan="8">Glasses</th>
					<th colspan="2">Maternity</th>
				</tr>
				<tr>
				<?php foreach ($constCat as $key => $value): ?>
				
					<th>
					<?php 
					if( preg_match('/^spouse_/', $value['slug'])) {
						echo esc_attr(  'Sp ' . $value['name'] );
					} else {
						echo esc_attr(  $value['name'] );	
					}
					?>
					</th>
					
				<!--	<th>Outpatient</th>
					<th>Inpatient</th>

					<th>Frame / 2 Years</th>
					<th>Lenses</th>
					<th>Befocus lenses</th>
					<th>Softlenses</th>

					<th>Frame / 2 Years</th>
					<th>Lenses</th>
					<th>Befocus lenses</th>
					<th>Softlenses</th>

					<th>Normal</th>
					<th>S.C</th>-->
				<?php endforeach; ?>
				</tr>
			</thead>
			<tbody>
				<?php foreach ( $users as $user ) : ?>
				<?php
				// WordPress admin doesn't need to be an employee.

				if ( $user->has_cap( 'manage_options' ) ) {
					continue;
				}
				$i=0;
				/*$default   = 0;
				$value     = isset( $values[ $user->ID ] ) ? $values[ $user->ID ] : $default;
				$field_id  = sprintf( '%s[%s][%s]', $name, $user->ID, $category[$i] );
				$field_arg = array(
					'name'  => $field_id,
					'id'    => $field_id,
					'desc'  => '',
					'value' => $value,
					'style' => 'width : 50px;',
				);*/
				?>
				<tr>
					<th><label><?php echo esc_html( $user->display_name ) ?></label></th>
					<?php
					
		$category = array('out_patient','in_patient','frame','lenses',
						 'befocuslenses','softlenses','maternity_norm','maternity_sc');
					
					foreach ($constCat as $catkey => $catval) {

					if(isset($catval)) {	

					$default   = 0;
					
					$value     = (isset( $values[ $user->ID ]) && isset( $values[ $user->ID ][$catval['id']]) && !is_null($values[ $user->ID ]
						[$catval['id']]) ) ? $values[ $user->ID ][$catval['id']]['value'] : 
						$default;
					$field_id  = sprintf( '%s[%s][%s]', $name, $user->ID, $catval['id'] );
					$field_arg = array(
						'name'  => $field_id. '[value]',
						'id'    => $field_id,
						'desc'  => '',
						'value' => $value,
						'class' => 'number_format custom_setting',
						'style' => 'width : 50px;',
					);
					$value_exp = ( $catval['slug']=='frame' || $catval['slug']=='spouse_frame'  ) ? 2 : 1;
					$field_expired = array(
						'name'  => $field_id.'[expired]',
						'id'    => $field_id.'[expired]',
						'desc'  => '',
						'type'  => 'hidden',
						'value' => $value_exp,
						'style' => 'width : 50px;',
					);
					?>
					<td valign="top">
						<?php $this->field_number( $field_arg ) ?>
						<?php $this->field_text( $field_expired ) ?>
					</td>
					<?php } ?>
					<?php } ?>
				</tr>
				
				<?php endforeach; ?>
			</tbody>
		</table>
		
		<?php
	}

	/**
	 * Callback for `register_setting`.
	 *
	 * @since 0.1.0
	 * @param array $new Submitted data
	 * @return array Sanitized data
	 */
	public function sanitize_callback( $new ) {
		$old = $this->_get_settings();

		if ( ! is_array( $new ) )
			return $old;

		if ( empty( $old ) )
			$old = array();

		//pr($new); die;

		foreach ( $this->options['fields'] as $field => $prop ) {
			if ( ! isset( $new[ $field ] ) )
				continue;

			if ( $prop['type'] === 'number' ) {
				$new[ $field ] = intval( $new[ $field ] );
			} else if ( $prop['type'] === 'select' && ! in_array( $new[ $field ], array_keys( $prop['options'] ) ) ) {
				$new[ $field ] = $prop['default'];
			}

			do_action( 'on_sanitizing_' . $field, $new, $old );
		}

		return array_merge( $old, $new );
	}

	public function get_setting( $key ) {
		return $this->_get_setting( $key );
	}

	/**
	 * Gets settings.
	 *
	 * @since 0.1.0
	 * @return array Setting values
	 */
	protected function _get_settings() {
		$defaults = array_combine(
			array_keys( $this->options['fields'] ),
			array_map( function( $arr ) { return isset( $arr['default'] ) ? $arr['default'] : ''; }, $this->options['fields'] )
		);

		return array_merge( $defaults, get_option( $this->options['option_name'], array() ) );
	}

	protected function _update_settings( $value ) {
		update_option( $this->options['option_name'], $value );
	}

	protected function _get_setting( $key ) {
		$settings = $this->_get_settings();
		return $settings[ $key ];
	}
	public function scripts_and_styles() {
		wp_enqueue_style( 'hris-medical-setting-limit-css', HRIS_MEDICAL_URI . '/css/setting-limit.css', array(), HRIS_MEDICAL_VERSION, 'all' );
		wp_enqueue_script( 'hris-medical-setting-limit-js', HRIS_MEDICAL_URI . '/js/setting-limit.js', array( 'jquery', ), HRIS_MEDICAL_VERSION );
	}

	protected function _constCat($data) {
		$out = array();
		$i=0;
		$category = array('out-patient','in-patient','frame','lense',
					 'biofocus-lense','soft-lense','normal','caesarean');
							
		foreach ($data as $key => $val) {
			# algorithm is always put position out-patien and in-patien in array index...
			if( in_array($val->slug, $category ) ) {
			if ($val->slug=='out-patient') {
				if(isset($out[0])) {
					$out[$i] = $out[0];
				}
				$out[0]['id'] = $val->term_id;
				$out[0]['slug'] = $val->slug;
				$out[0]['name'] = $val->name;
				
			} else if ($val->slug=='in-patient') {
				if(isset($out[1])) {
					$out[$i] = $out[1];
				}
				$out[1]['id'] = $val->term_id;
				$out[1]['slug'] = $val->slug;
				$out[1]['name'] = $val->name;
			 } else if ($val->slug=='frame') {
			 	if(isset($out[1])) {
					$out[$i] = $out[1];
				}
			 	$out[$i]['id'] = $val->term_id;
				$out[$i]['slug'] = $val->slug;
				$out[$i]['name'] = $val->name;
				$out[$i+4]['id'] = 'sp_'.$val->term_id;
				$out[$i+4]['slug'] = 'spouse_'.$val->slug;
				$out[$i+4]['name'] = $val->name;
			 }  else if ($val->slug=='lense') {
			 	$out[$i]['id'] = $val->term_id;
				$out[$i]['slug'] = $val->slug;
				$out[$i]['name'] = $val->name;
				$out[$i+4]['id'] = 'sp_'.$val->term_id;
				$out[$i+4]['slug'] = 'spouse_'.$val->slug;
				$out[$i+4]['name'] = $val->name;
			 }  else if ($val->slug=='biofocus-lense') {
			 	$out[$i]['id'] = $val->term_id;
				$out[$i]['slug'] = $val->slug;
				$out[$i]['name'] = $val->name;
				$out[$i+4]['id'] = 'sp_'.$val->term_id;
				$out[$i+4]['slug'] = 'spouse_'.$val->slug;
				$out[$i+4]['name'] = $val->name;
			 }   else if ($val->slug =='soft-lense') {

			 	$out[$i]['id'] = $val->term_id;
				$out[$i]['slug'] = $val->slug;
				$out[$i]['name'] = $val->name;
				$out[$i+4]['id'] = 'sp_'.$val->term_id;
				$out[$i+4]['slug'] = 'spouse_'.$val->slug;
				$out[$i+4]['name'] = $val->name;
				
			 }else {
			 	if(isset($out[$i])) {
			 		$i+=2;
			 	}
				$out[$i]['id'] = $val->term_id;
				$out[$i]['slug'] = $val->slug;
				$out[$i]['name'] = $val->name;
			}

			}

			$i++;
		}
		ksort($out);
		return $out;
	}

}

