<?php

/**
 * Any customizations need to be done in edit table (list of leave and/or leave types).
 *
 * @package HRIS Medical
 * @since 0.1.0
 * @author Fandy Fardian <shifty.power@gmail.com>
 */
class HRIS_Medical_Edit_Table implements HRIS_Medical_Component_Interface {

	/**
	 * Called by plugin's main-file in plugins_load action.
	 *
	 * @since 0.1.0
	 * @return void
	 */
	public function load() {
		// Customizes columns header.
		add_filter( 'manage_edit-' . HRIS_Medical_Post_Type::NAME . '_columns', array( $this, 'custom_columns_header' ) );
		//add_filter( 'manage_edit-' . HRIS_Medical_Type_Taxonomy::NAME . '_columns', array( $this, 'tax_custom_columns_header' ) );

		// Remove actions on edit.php.
		// This needs to be on its own hook because of late priority of 'Approve' / 'Reject' buttons.
		add_filter( 'manage_edit-' . HRIS_Medical_Post_Type::NAME . '_columns', array( $this, 'remove_actions_col' ), 100 );

		// Fills customized columns.
		add_action( 'manage_' . HRIS_Medical_Post_Type::NAME . '_posts_custom_column', array( $this, 'custom_columns_row' ), 10, 2 );
		//add_action( 'manage_' . HRIS_Medical_Type_Taxonomy::NAME . '_custom_column', array( $this, 'tax_custom_columns_row' ), 10, 3 );

		// Shows avatar in author col.
		add_filter( 'the_author', array( $this, 'add_avatar' ) );

		// Hides subsub top navigation above the table if user is not an administrator.
		// All (x) | Draft (x).
		add_filter( 'views_edit-' . HRIS_Medical_Post_Type::NAME, array( $this, 'hide_subsubsub' ) );

		// Shows leave entries by author only if an user doesn't have 'manage_hris_leave' cap.
		add_filter( 'pre_get_posts', array( $this, 'shows_medical_entries_by_author_only' ) );

		// Enqueues scripts and styles.
		add_action( 'load-edit.php', array( $this, 'scripts_and_styles' ) );

	}

	/**
	 * Customizes columns header of leave post type edit table.
	 *
	 * @since 0.1.0
	 * @filter manage_edit-{post_type}_columns
	 * @param array $columns Default columns header
	 * @return array Customized columns header
	 */
	public function custom_columns_header( $columns ) {
		// no Columns to remove.
		$remove_columns = array();

		// New and/or renamed columns.
		$new_columns = array(
			'author'       => __( 'Employee',      'hris-medical' ),
			'status'       => __( 'Status',        'hris-medical' ),
			'title'        => __( 'Medical Code',  'hris-medical' ),
			'total_amount' => __( 'Total Amount',  'hris-medical' ),
			'total_review' => __( 'Total Review',  'hris-medical' ),
			'print'        => __( 'Print',         'hris-medical' ),
		);

		// No need to show author for 'hris_requestor' role because the columns shows his/her name all the time.
		if ( ! current_user_can( 'manage_hris_medical' ) ) {
			$remove_columns[] = 'author';
			unset( $new_columns['author'] );
		}

		foreach ( $remove_columns as $col ) {
			if ( isset( $columns[ $col ] ) ) {
				unset( $columns[ $col ] );
			}
		}

		foreach ( $new_columns as $k => $v ) {
			$columns[ $k ] = $v;
		}

		return $columns;
	}

	public function remove_actions_col( $columns ) {
		if ( isset( $columns['actions'] ) )
			unset( $columns['actions'] );

		return $columns;
	}

	public function tax_custom_columns_header( $columns ) {
		if ( isset( $columns['posts'] ) ) {
			unset( $columns['posts'] );
		}

		$columns['is_planned'] = __( 'Is planned', 'hris-medical' );

		return $columns;
	}

	/**
	 * Customizes row of leave post type edit table.
	 *
	 * @since 0.1.0
	 * @action manage_{post_type}_posts_custom_column
	 * @param string $column Column being passed
	 * @param int $post_id Post ID
	 * @return void
	 */
	public function custom_columns_row( $column, $post_id ) {
		$expected_columns = array( 'total_amount','total_review','title','status', 'print' );
		$date_types       = array( 'request_date', 'start_date', 'end_date' );
		$title 			  = array( 'title' );

		/*$money_types       = array( 'out_patient','in_patient','glass_employee_lenses',
									'glass_employee_befocuslenses','glass_employee_softlenses',
									'glass_dependent_frame','glass_dependent_lenses',
									'glass_dependent_befocuslenses','glass_dependent_softlenses',
									'maternity_norm','maternity_sc' ); */
		$money_types = array();

		// Functions defined by advanced-custom-fields plugin.
		if ( ! function_exists( 'the_field' ) || ! function_exists( 'get_field' ) ) {
			return;
		}

		if ( in_array( $column, $expected_columns ) ) {
			if ( in_array( $column, $date_types ) ) {
				echo esc_html( date( 'd/m/Y', strtotime( get_field( $column ) ) ) );
			} else if ( in_array( $column, $money_types ) ) {
				echo esc_html(	number_format( get_field( $column ), 2, '.', ',' ) );
			} else if ( 'print' === $column ) {
				if ( HRIS_Approval_Setting::APPROVED === get_post_status( $post_id ) ) {
					$tpl = '<a href="%s" class="button">%s</a>';
					echo sprintf( $tpl, hris_get_print_post_link( $post_id ), __( 'Print', 'hris-medical' ) );
				}
			} else {
				the_field( $column );
			}
		}


	}

	/**
	 * Customizes row of leave types edit table.
	 *
	 * @since 0.1.0
	 * @action manage_{post_type}_posts_custom_column
	 * @param string $empty
	 * @param string $column Column being passed
	 * @param int $post_id Post ID
	 * @return void
	 */
	public function tax_custom_columns_row( $empty, $column, $term_id ) {
		$term_meta = get_option( 'taxonomy_' . $term_id );
		if ( 'is_planned' === $column && isset( $term_meta['is_planned'] ) ) {
			echo esc_html( $term_meta['is_planned'] );
		}
	}

	/**
	 * Shows leave entries by current user, except adminstrator.
	 *
	 * @since 0.1.0
	 * @filter pre_get_posts
	 * @param object $query Instance of WP_Query
	 * @return object
	 */
	public function shows_medical_entries_by_author_only( $query ) {
		if ( ! is_admin() )
			return $query;

		if ( HRIS_Medical_Post_Type::NAME !== $query->get( 'post_type' ) )
			return $query;

		// We only need to filter leave records in edit base.
		$screen = get_current_screen();
		if ( 'edit' !== $screen->base ) {
			return $query;
		}

		// If a user is adminstrator  then no need to filter the records.
		if ( hris_check_user_role( 'administrator' ) ) {
			return $query;
		}

		// Filter records by current user.
		$user = wp_get_current_user();
		$query->set( 'author', $user->ID );

		return $query;
	}

	/**
	 * Adds avatar in adition of user name.
	 * @filter the_author
	 * @param string $name
	 * @return string
	 */
	public function add_avatar( $name ) {
		global $post;

		// Makes sure this is in admin page.
		if ( ! is_admin() )
			return;

		$screen = get_current_screen();

		$expected_screen_ids = array(
			'edit-' . HRIS_Medical_Post_Type::NAME,
			HRIS_Medical_Post_Type::NAME . '_page_' . HRIS_Medical_Approval::SLUG,
		);

		if ( ! in_array( $screen->id, $expected_screen_ids ) )
			return $name;

		return get_avatar( $post->post_author, 32 ) . ' ' . $name;
	}

	/**
	 * Hides subsubsub top nav "All (x) | Draft (x)" for non-adminstrator.
	 *
	 * @since 0.1.0
	 * @filter views_edit-{post_type}
	 * @param array $views
	 * @return array
	 */
	public function hide_subsubsub( $views ) {
		if ( ! hris_check_user_role( 'administrator' ) ) {
			return array();
		}

		return $views;
	}

	/**
	 * Enqueues scripts and styles on edit.php for this post type.
	 *
	 * @since 0.1.0
	 * @action
	 */
	public function scripts_and_styles() {
		if ( ! isset( $_GET['post_type'] ) )
			return;

		if ( HRIS_Medical_Post_Type::NAME !== $_GET['post_type'] )
			return;

		wp_enqueue_script( 'hris-medical-edit-table', HRIS_MEDICAL_URI . '/js/edit-table.js', array( 'jquery', ), HRIS_LEAVE_VERSION );

		// Enqueue variables
		wp_localize_script(
			'hris-medical-edit-table',
			'HRIS_MEDICAL_EDIT_TABLE',
			array(
				'remove_title_anchor' => ! hris_check_user_role( 'administrator' ),
				'remove_checkbox'     => ! hris_check_user_role( 'administrator' ),
			)
		);
	}

}
