<?php

/**
 * Interface that plugin-component's class MUST implement.
 *
 * @package HRIS Medical
 * @since 0.1.0
 * @author Akeda Bagus <admin@gedex.web.id>
 */
interface HRIS_Medical_Component_Interface {
	public function load();
}
