<?php

class HRIS_Medical_Report implements HRIS_Medical_Component_Interface {
	const SLUG = 'hris-medical-report';

	/**
	 * Used to cache terms in reporting.
	 *
	 * @var array
	 */
	protected $terms;

	public function load() {
		add_action( 'admin_menu', array( $this, 'register_report_menu' ) );

		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_statics' ) );
	}

	public function register_report_menu() {
		$terms = $this->get_terms();
		// Summary only for hris-manager and administrator.
		if ( hris_check_user_role( 'hris_manager' ) || hris_check_user_role( 'administrator' ) ) {
			
			require_once HRIS_MEDICAL_INCLUDES . 'report-summary.php';
			foreach ( $terms as $term ) {
				$report_summary = new HRIS_Medical_Report_Summary();
				$report_summary->set_term( $term );
				add_submenu_page(
					'edit.php?post_type=' . HRIS_Medical_Post_Type::NAME,
					sprintf(__( 'Report Summary %s', 'hris-medical' ), $term->name ),
					sprintf(__( 'Report Summary %s', 'hris-medical' ), $term->name ), 
					'edit_hris_medical_records',
					'summary-'. self::SLUG . '-' . $term->slug,
					array( $report_summary, 'render' )
				);
			}	
		}

		

		// Child reports based on HRIS_Medical_Type_Taxonomy.
		require_once HRIS_MEDICAL_INCLUDES . 'report-by-term.php';

		foreach ( $terms as $term ) {
			$report_by_type = new HRIS_Medical_Report_By_Term();
			$report_by_type->set_term( $term );

			add_submenu_page(
				'edit.php?post_type=' . HRIS_Medical_Post_Type::NAME,
				sprintf( __( 'Report %s', 'hris-medical' ), $term->name ),
				sprintf( __( 'Report %s', 'hris-medical' ), $term->name ),
				'edit_hris_medical_records',
				self::SLUG . '-' . $term->slug,
				array( $report_by_type, 'render' )
			);
		}
	}

	public function enqueue_statics( $hook ) {
		$expected_hooks = array(
			HRIS_Medical_Post_Type::NAME . '_page_' . self::SLUG,
		);

		$terms = $this->get_terms();
		foreach ( $terms as $term ) {
			$expected_hooks[] = HRIS_Medical_Post_Type::NAME . '_page_' . self::SLUG . '-' . $term->slug;
		}

		if ( ! in_array( $hook, $expected_hooks ) ) {
			return;
		}

		wp_enqueue_style( 'hris_medical_report', HRIS_MEDICAL_URI . '/css/report.css' );
		wp_enqueue_style( 'hris-jquery-ui-theme' );
		wp_enqueue_style( 'hris-common' );

		wp_enqueue_script( 'hris_medical_report', HRIS_MEDICAL_URI . '/js/report.js', array( 'jquery', 'jquery-ui-datepicker' ) );
	}

	protected function get_terms() {
		if ( ! $this->terms ) {
			$this->terms = get_terms( HRIS_Medical_Type_Taxonomy::NAME, array(
				'parent'     => 0,
				'hide_empty' => false,
			) );

			if ( ! $this->terms || is_wp_error( $this->terms ) ) {
				$this->terms = array();
			}
		}

		return $this->terms;
	}
}
