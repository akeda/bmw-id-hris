<?php

class acf_field_author_family extends acf_field
{

	public $settings; // will hold info such as dir / path
	public $defaults; // will hold default field options


	/**
	 *  __construct
	 *
	 *  Set name / label needed for actions / filters
	 *
	 *  @since	3.6
	 *  @date	23/01/13
	 */
	public function __construct() {
		// vars
		$this->name     = 'author_family';
		$this->label    = __( 'Author Family' );
		$this->category = __( 'Choice', 'acf'); // Basic, Content, Choice, etc

		// do not delete!
    parent::__construct();

    // settings
		$this->settings = array(
			'path'    => apply_filters( 'acf/helpers/get_path', __FILE__ ),
			'dir'     => apply_filters( 'acf/helpers/get_dir',  __FILE__ ),
			'version' => '1.0.0'
		);
	}

	/**
	 *  create_field()
	 *
	 *  Create the HTML interface for your field
	 *
	 *  @param	$field - an array holding all the field's data
	 *
	 *  @type	action
	 *  @since	3.6
	 *  @date	23/01/13
	 */
	public function create_field( $field ) {
		global $post;

		$user = get_user_by( 'id', $post->post_author );

		$field['value'] = wp_parse_args( $field['value'], array(
			'target_id'   => $post->post_author,
			'target_name' => $user->display_name,
			'target_type' => 'employee', // Can be spouse or children
		) );

		if ( ! in_array( $field['value']['target_type'], array( 'employee', 'spouse', 'children' ) ) ) {
			$field['value']['target_type'] = 'employee';
		}
		$spouse   = get_user_meta( $user->ID, 'spouse', true );
		$children = get_user_meta( $user->ID, 'children', true );
		// create Field HTML
		?>
		<div class="acf-author_family">
			<select name="<?php echo esc_attr( $field['name'] ); ?>[target_id]" >
				<optgroup label="Employee">
					<option value="<?php echo esc_attr( $user->ID ); ?>" <?php if ( $field['value']['target_type'] === 'employee' ) { selected( $user->ID, $field['value']['target_id'] ); } ?> data-target_type="employee"><?php echo esc_html( $user->display_name ); ?></option>
				</optgroup>

				<?php if ( ! empty( $spouse ) ) : ?>
				<optgroup label="Spouse">
					<option value="<?php echo esc_attr( $spouse['name'] ); ?>" <?php selected( $spouse['name'], $field['value']['target_id'] ); ?> data-target_type="spouse"><?php echo esc_html( $spouse['name'] ); ?></option>
				</optgroup>
				<?php endif; ?>

				<?php if ( ! empty( $children ) ) : ?>
				<optgroup label="Children">
					<?php foreach ( $children as $index => $val ) : ?>
						<option value="<?php echo esc_attr( $index ); ?>" <?php if ( $field['value']['target_type'] === 'children' ) { selected( $index, $field['value']['target_id'] ); } ?> data-target_type="children"><?php echo esc_html( $val['name'] ); ?></option>
					<?php endforeach; ?>
				</optgroup>
				<?php endif; ?>
			</select>

			<input type="hidden" value="<?php echo esc_attr( $field['value']['target_name'] ); ?>" name="<?php echo esc_attr( $field['name'] ); ?>[target_name]" class="input-alt target_name">
			<input type="hidden" value="<?php echo esc_attr( $field['value']['target_type'] ); ?>" name="<?php echo esc_attr( $field['name'] ); ?>[target_type]" class="input-alt target_type">

		</div>
		<?php
	}


	/**
	 *  input_admin_enqueue_scripts()
	 *
	 *  This action is called in the admin_enqueue_scripts action on the edit screen where your field is created.
	 *  Use this action to add css + javascript to assist your create_field() action.
	 *
	 *  $info	http://codex.wordpress.org/Plugin_API/Action_Reference/admin_enqueue_scripts
	 *  @type	action
	 *  @since	3.6
	 *  @date	23/01/13
	 */
	public function input_admin_enqueue_scripts() {
		extract( array(
			'handle'    => 'acf-author_family',
			'src'       => HRIS_MEDICAL_URI . 'js/acf-author_family.js',
			'deps'      => array( 'jquery' ),
			'ver'       => HRIS_MEDICAL_VERSION,
			'in_footer' => false,
		) );
		wp_enqueue_script( $handle, $src, $deps, $ver, $in_footer );
	}

}

