<?php

class HRIS_Medical_Report_Summary {

	/**
	 * @var object
	 */
	private $term;

	public function render() {
		if ( ! $this->term ) {
			wp_die( __( 'Term MUST BE SET before render with <code>set_term( $term )</code> method', 'hris-medical' ) );
			exit();
		}	
		$GLOBALS['report_data'] = $this->get_data();
		

		$template = HRIS_MEDICAL_TEMPLATES . 'report_summary_' . $this->term->slug . '.php';

		include $template;
	}

	public function set_term( $term ) {
		$this->term = $term;
	}

	public function get_data() {
		$query     = null;
		$date_from = '';
		$date_to   = '';

		// Check requested date to fill the query.
		if ( isset( $_REQUEST['date_from'] ) && isset( $_REQUEST['date_to'] ) ) {
			$date_from = new DateTime( $_REQUEST['date_from'] );
			$date_to   = new DateTime( $_REQUEST['date_to'] );

			// Makes sure $date_from and $date_to within the same year
			// and $date_from is less than $date_to. If no $date_to will
			// be set to $date_from.
			if ( ( $date_from->format('Y') !== $date_to->format('Y') ) ||
			       $date_from > $date_to )
			{
				$date_to->setDate(
					intval( $date_from->format('Y') ),
					intval( $date_from->format('m') ),
					intval( $date_from->format('d') )
				);
			}
			$users   = get_users();

			$query = new WP_Query( array(
				'post_type'  => HRIS_Medical_Post_Type::NAME,
				'date_query' => array(
					'after' => array(
						'year'  => $date_from->format('Y'),
						'month' => $date_from->format('n'),
						'day'   => $date_from->format('j'),
					),
					'before' => array(
						'year'  => $date_to->format('Y'),
						'month' => $date_to->format('n'),
						'day'   => $date_to->format('j'),
					),
					'inclusive' => true,
				),
			) );
		}

		$term = $this->term;
		$self = $this;
		return compact( 'query', 'term', 'self', 'date_from', 'date_to' );
	}



	public function get_hidden_fields() {
		return str_replace(
			array(
				'{post_type}',
				'{page}',
				//'{user_id}'
			),
			array(
				esc_attr( HRIS_Medical_Post_Type::NAME ),
				esc_attr( "summary-". HRIS_Medical_Report::SLUG. '-' . $this->term->slug ),
		//		isset( $_REQUEST['user_id'] ) ? esc_attr( $_REQUEST['user_id'] ) : '',
			),
			'
			<input type="hidden" name="post_type" value="{post_type}">
			<input type="hidden" name="page" value="{page}">
			<!--<input type="hidden" name="user_id" value="{user_id}"> -->
			'
		);
	}

	public function the_hidden_fields() {
		echo $this->get_hidden_fields();
	}
}
