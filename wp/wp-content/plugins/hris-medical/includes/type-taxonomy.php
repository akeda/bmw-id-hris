<?php

class HRIS_Medical_Type_Taxonomy implements HRIS_Medical_Component_Interface {
	const NAME = 'hris_medical_type';

	public function load() {
		// Register custom post type on the 'init' hook.
		add_action( 'init', array( $this, 'register_taxonomy' ) );
	}

	public function register_taxonomy() {
		$args = array(
			'public'            => true,
			'show_ui'           => true,
			'show_in_nav_menus' => true,
			'show_tagcloud'     => false,
			'show_admin_column' => true,
			'hierarchical'      => true,
			'query_var'         => false,

			'capabilities' => array(
				'manage_terms' => 'manage_hris_medical',
				'edit_terms'   => 'edit_hris_medical_types',
				'delete_terms' => 'manage_hris_medical',
				'assign_terms' => 'edit_hris_medical_types',
			),

			'rewrite' => false,

			/* Labels used when displaying taxonomy and terms. */
			'labels' => array(
				'name'                       => __( 'Medical Type',                           'hris-medical' ),
				'singular_name'              => __( 'Medical Type',                            'hris-medical' ),
				'menu_name'                  => __( 'Medical Types',                           'hris-medical' ),
				'name_admin_bar'             => __( 'Medical Type',                            'hris-medical' ),
				'search_items'               => __( 'Search Medical Types',                    'hris-medical' ),
				'popular_items'              => __( 'Popular Medical Types',                   'hris-medical' ),
				'all_items'                  => __( 'All Medical Types',                       'hris-medical' ),
				'edit_item'                  => __( 'Edit Medical Type',                       'hris-medical' ),
				'view_item'                  => __( 'View Medical Type',                       'hris-medical' ),
				'update_item'                => __( 'Update Medical Type',                     'hris-medical' ),
				'add_new_item'               => __( 'Add New Medical Type',                    'hris-medical' ),
				'new_item_name'              => __( 'New Medical Types Name',                  'hris-medical' ),
				'separate_items_with_commas' => __( 'Separate Medical Types with commas',      'hris-medical' ),
				'add_or_remove_items'        => __( 'Add or remove Medical Types',             'hris-medical' ),
				'choose_from_most_used'      => __( 'Choose from the most used Medical Types', 'hris-medical' ),
			)
		);

		// Register the taxonomy.
		register_taxonomy( self::NAME, array( HRIS_Medical_Post_Type::NAME ), $args );
	}
}
