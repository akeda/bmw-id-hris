<?php
/**
 * Set employee name and spouse fields on post-new screen.
 *
 * @package HRIS Medical
 * @since 0.1.0
 * @author Fandy Fardian <shifty.power@gmail.com>
 */
class HRIS_Medical_Auto_Employee_Name_And_Spouse implements HRIS_Medical_Component_Interface {

	/**
	 * Called by plugin's main-file in plugins_load action.
	 *
	 * @since 0.1.0
	 * @return void
	 */
	public function load() {
		add_filter( 'acf/load_field/name=user', array( $this,'my_acf_load_field' ) );
	}

	/**
	 * Set employee's name and department fields on post-new screen.
	 *
	 * @action acf/load_value
	 * @param string $value
	 * @param int $post_id
	 * @param array $field
	 * @return string
	 */
	public function my_acf_load_field( $field ) {
		$screen = get_current_screen();
		if ( $screen->id !== HRIS_Medical_Post_Type::NAME )
			return $field;
		//pr($screen);
		if ( 'add' === $screen->action ) {
			$user     = wp_get_current_user();
			$spouse   = get_user_meta( $user->ID, 'spouse' );
			$children = get_user_meta( $user->ID, 'children' );

			$dt_list = array();
			$dt_list[ $user->data->display_name ] = $user->data->display_name;

			if ( 0 < count( $spouse ) && $spouse[0]['name']) {
				foreach ($spouse as $key => $value) {
					$dt_list[ $value['name'] ] = $value['name'];
				}
			}
			if ( 0 < count($children) ) {
				foreach ($children[0] as $keychildern => $valchildern) {
					$dt_list[ $valchildern['name'] ] = $valchildern['name'];
				}
			}

	    $field['choices'] = $dt_list;

	    return $field;
		} else if ( 'post' === $screen->base ) {
			
			$post     = get_post();
			$user     = get_userdata( $post->post_author );
			$spouse   = get_user_meta( $post->post_author, 'spouse' );
			$children = get_user_meta( $post->ID, 'children' );
			$dt_list  = array();
			$dt_list[$user->data->display_name] = $user->data->display_name;
			if ( 0 < count( $spouse ) && $spouse[0]['name'] ) {
				foreach ($spouse as $key => $value) {
					$dt_list[ $value['name'] ]= $value['name'];
				}
			}
			if ( 0 < count( $children ) ) {
				foreach ($children[0] as $keychildern => $valchildern) {
					$dt_list[ $valchildern['name'] ] = $valchildern['name'];
				}
			}

	    $field['choices'] = $dt_list;
			return $field;
		}

		return $field;
	}

	private function _is_edit_page($new_edit = null) {
	    global $pagenow;
	    //make sure we are on the backend
	    if (!is_admin()) return false;

	    if ( $new_edit == "edit" )
	        return in_array( $pagenow, array( 'post.php',  ) );
	    else if ( $new_edit == "new" ) //check for new post page
	        return in_array( $pagenow, array( 'post-new.php' ) );
	    else //check for either new or edit
	        return in_array( $pagenow, array( 'post.php', 'post-new.php' ) );
	}

}
