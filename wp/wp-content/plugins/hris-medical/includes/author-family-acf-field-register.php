<?php

class HRIS_Medical_Author_Family_Acf_Field_Register {
	public function load() {
		add_action( 'acf/register_fields', array( $this, 'register_fields' ) );
	}

	public function register_fields() {
		require_once HRIS_MEDICAL_INCLUDES . 'author_family.php';
		return new acf_field_author_family();
	}
}

