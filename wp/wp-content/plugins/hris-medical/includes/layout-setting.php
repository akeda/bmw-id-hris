<?php
/**
 * Set layout for add, edit or approve and spouse fields on post-new or edit screen.
 *
 * @package HRIS Medical
 * @since 0.1.0
 * @author Fandy Fardian <shifty.power@gmail.com>
 */
class HRIS_Medical_Layout_Setting implements HRIS_Medical_Component_Interface {

	/**
	 * Called by plugin's main-file in plugins_load action.
	 *
	 * @since 0.1.0
	 * @return void
	 */
	public function load() {
		
		add_action('admin_enqueue_scripts', array( $this, 'scripts_and_styles' ) );
	}
	private function _is_edit_page($new_edit = null){
	    global $pagenow;
	    //make sure we are on the backend
	    if (!is_admin()) return false;


	    if($new_edit == "edit")
	        return in_array( $pagenow, array( 'post.php',  ) );
	    else if($new_edit == "new") //check for new post page
	        return in_array( $pagenow, array( 'post-new.php' ) );
	    else //check for either new or edit
	        return in_array( $pagenow, array( 'post.php', 'post-new.php' ) );
	}

	public function scripts_and_styles () {
		$screen = get_current_screen();
		
		if($screen->action !== 'add') {
			
			if(isset($_GET['post'])) {
				$post_id = $_GET['post'];
				$post_obj = get_post( $post_id );
				$post_status = get_post_status( $post_id );
				$user = wp_get_current_user();
				$approver = $this->_get_status_by_user ($post_obj->ID, $post_obj->post_author, $user->ID);
		
				wp_enqueue_script( 'hris-medical-layout', HRIS_MEDICAL_URI . '/js/layout-setting.js', array( 'jquery', ), HRIS_MEDICAL_VERSION );
				
				wp_localize_script(
				'hris-medical-layout',
				'HRIS_MEDICAL_LAYOUT',
							array(
								//'hr_review' =>  ($user->ID==$aprover && $post_status=='hr-review'),
								//'supervisor_review'     =>  ($user->ID==$aprover && $post_status=='supervisor-review'),
								'hr_review'         =>  (isset($approver[$user->ID]) && $approver[$user->ID] === 'hr-review'),
								'supervisor_review' =>  (isset($approver[$user->ID]) && $approver[$user->ID] === 'supervisor-review'),
								'hr_manager_review' =>  (isset($approver[$user->ID]) && $approver[$user->ID] === 'hr-manager-review'),
								'hrm_review' =>  (isset($approver[$user->ID]) && $approver[$user->ID] === 'hrm-review'),
								'disabled_edit'     =>  ($user->ID != $post_obj->post_author),
								
							)
				);
				}
			} else {
				
				wp_enqueue_script( 'hris-medical-layout', HRIS_MEDICAL_URI . '/js/layout-setting.js', array( 'jquery', ), HRIS_MEDICAL_VERSION );
				
				wp_localize_script(
				'hris-medical-layout',
				'HRIS_MEDICAL_LAYOUT',
					array(
						'ajaxurl'           		=>  admin_url( 'admin-ajax.php' ),
						'hr_review'         		=>  false,
						'supervisor_review' 		=>  false,
						'hr_manager_review' 		=>  false,
						'hrm_review' 		=>  false,
					)
				);

			}

		
	}

	

	protected function _get_status_by_user ($page_id, $author, $user_id) {
		$setting   = hris_get_component_from_module( 'approval', 'Setting' );
		//$approver = $setting->get_user_approvers(HRIS_Purchase_Post_Type::NAME, $author );
		$list_statuses = $setting->get_post_statuses( HRIS_Medical_Post_Type::NAME );
		
		$user_approve = array();
		foreach ($list_statuses as $key => $val ) {
			
			$user_approve[hris_get_approver_by_status( HRIS_Medical_Post_Type::NAME, $author, $val )] = $val;
		} 
		return $user_approve;
	}

}