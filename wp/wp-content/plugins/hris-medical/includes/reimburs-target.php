<?php

class HRIS_Medical_Reimburs_Target implements HRIS_Medical_Component_Interface {
    const NAME = 'hris_medical_reimburs_target';
    const SLUG = 'hris_medical_reimburs_target';

    public function load() {
        // Register custom post type on the 'init' hook.
        add_action( 'init', array( $this, 'register_taxonomy' ) );
    }

    public function register_taxonomy() {

        $args = array( 
           'public'             => true,
            'show_ui'           => true,
            'show_in_nav_menus' => true,
            'show_tagcloud'     => false,
            'show_admin_column' => true,
            'hierarchical'      => true,
            'query_var'         => false,

            'capabilities' => array(
                'manage_terms' => 'manage_hris_medical',
                'edit_terms'   => 'edit_hris_medical_reimburs_target',
                'delete_terms' => 'manage_hris_medical',
                'assign_terms' => 'edit_hris_medical_reimburs_target',
            ),

            'rewrite' => false,

            /* Labels used when displaying taxonomy and terms. */ 
            'labels' => array(
                'name'                       => __( 'Reimbursement Target',                            'hris-medical' ),
                'singular_name'              => __( 'Reimbursement Target',                            'hris-medical' ),
                'menu_name'                  => __( 'Reimbursement Targets',                           'hris-medical' ),
                'name_admin_bar'             => __( 'Reimbursement Target',                            'hris-medical' ),
                'search_items'               => __( 'Search Reimbursement Targets',                    'hris-medical' ),
                'popular_items'              => __( 'Popular Reimbursement Targets',                   'hris-medical' ),
                'all_items'                  => __( 'All Reimbursement Targets',                       'hris-medical' ),
                'edit_item'                  => __( 'Edit Reimbursement Target',                       'hris-medical' ),
                'view_item'                  => __( 'View Reimbursement Target',                       'hris-medical' ),
                'update_item'                => __( 'Update Reimbursement Target',                     'hris-medical' ),
                'add_new_item'               => __( 'Add New Reimbursement Target',                    'hris-medical' ),
                'new_item_name'              => __( 'New Reimbursement Targets Name',                  'hris-medical' ),
                'separate_items_with_commas' => __( 'Separate Reimbursement Targets with commas',      'hris-medical' ),
                'add_or_remove_items'        => __( 'Add or remove Reimbursement Targets',             'hris-medical' ),
                'choose_from_most_used'      => __( 'Choose from the most used Reimbursement Targets', 'hris-medical' ),
            )
        );

        // Register the taxonomy.
        register_taxonomy( self::NAME, array( HRIS_Medical_Post_Type::NAME ), $args );
    }
}
