<?php

class HRIS_Medical_Approval implements HRIS_Medical_Component_Interface {

	const SLUG = 'hris-medical-approval';

	private $_list_table;

	public function load() {
		$hook = sprintf( 'load-%s_page_%s', HRIS_Medical_Post_Type::NAME, self::SLUG );
		add_action( $hook, array( $this, 'on_page_load' ) );
		add_action( 'admin_menu', array( $this, 'register_approval_menu' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'scripts_and_styles' ) );

	}

	public function on_page_load() {
		if ( ! isset( $_REQUEST['post_type'] ) )
			return;

		if ( HRIS_Medical_Post_Type::NAME !== $_REQUEST['post_type'] )
			return;

		if ( ! isset( $_REQUEST['page'] ) )
			return;

		if ( self::SLUG !== $_REQUEST['page'] )
			return;

		require_once HRIS_MEDICAL_INCLUDES . '/table.php';

		$this->_list_table = new HRIS_Medical_Table();

		//pr($this->_list_table->prepare_items());
		$this->_list_table->prepare_items();
	}

	/**
	 * Register approval menu.
	 *
	 * @since 0.1.0
	 * @action admin_menu
	 * @return void
	 */
	public function register_approval_menu() {
		add_submenu_page(
			'edit.php?post_type=' . HRIS_Medical_Post_Type::NAME,
			__( 'Approval', 'hris-medical' ),
			__( 'Approval', 'hris-medical' ),
			'approve_hris_medical_records',
			self::SLUG,
			array( $this, 'approval_page_callback' )
		);
	}

	/**
	 * Render approval page. Callback for 'add_submenu_page'.
	 *
	 * @since 0.1.0
	 * @return void
	 */
	public function approval_page_callback() {

		?>
		<div class="wrap">
			<?php screen_icon(); ?>
			<form id="posts-filter" action="" method="get">
			<h2><?php _e( 'Needs approval', 'hris-medical' ); ?></h2>
				<?php $this->_list_table->display(); ?>
			</form>
		</div>
		<?php
	}

	public function scripts_and_styles ($hook) {

	}

}
