<?php

/**
 * Any customizations need to be done when editing the post.
 *
 * @package HRIS Medical
 * @since 0.1.0
 * @author Fandy Fardian <shifty.power@gmail.com>
 */
class HRIS_Medical_Edit_Post implements HRIS_Medical_Component_Interface {

	/**
	 * Called by plugin's main-file in plugins_load action.
	 *
	 * @since 0.1.0
	 * @return void
	 */
	public function load() {
		add_filter( 'post_updated_messages', array( $this, 'alter_updated_messages' ) );

		add_filter( 'ef_custom_status_list', array( $this, 'alter_post_status_list' ), 10, 2 );

		add_action( 'load-post-new.php', array( $this, 'scripts_and_styles' ) );
		add_action( 'load-post.php', array( $this, 'scripts_and_styles' ) );

		// Redirect for hris_requestor, otherwise check for custom actions.
		add_action( 'load-post.php', array( $this, 'post_actions' ) );
		//hook this to make ajax handler
		add_action( 'wp_ajax_get_balance',  array( $this, 'get_balance_callback' ) );

	}

	/**
	 * Alter messages when a post updated.
	 *
	 * @since 0.1.0
	 * @filter post_updated_messages
	 * @param array $messages Post updated messages.
	 * @return array Altered messages
	 */
	public function alter_updated_messages( $messages ) {
		global $post_type;

		if ( HRIS_Medical_Post_Type::NAME !== $post_type ) {
			return $messages;
		}

		// Messages
		$updated_message   =  __( 'Medical entry updated.', 'hris-medical' );
		$approved_message  = __( 'Medical entry approved.', 'hris-medical' );
		$saved_message     = __( 'Medical entry saved.', 'hris-medical' );
		$submitted_message = __( 'Medical entry submitted.', 'hris-medical' );

		// Update 'Post updated. View post.' message.
		if ( isset( $messages['post'][1] ) ) {
			$messages['post'][1] = $updated_message;
		}

		// Update 'Post restored to revision from %s'.
		if ( isset( $messages['post'][5] ) ) {
			$messages['post'][5] = $updated_message;
		}

		// Update 'Post published. View post.' message.
		if ( isset( $messages['post'][6] ) ) {
			$messages['post'][6] = $approved_message;
		}

		// Update 'Post saved.' message.
		if ( isset( $messages['post'][7] ) ) {
			$messages['post'][7] = $saved_message;
		}

		// Update 'Post submitted.' message.
		if ( isset( $messages['post'][8] ) ) {
			$messages['post'][8] = $submitted_message;
		}

		// Update 'Post draft updated.' message.
		if ( isset( $messages['post'][10] ) ) {
			$messages['post'][10] = $updated_message;
		}

		return $messages;
	}

	public function alter_post_status_list( $custom_statuses, $post ) {

		if ( HRIS_Medical_Post_Type::NAME !== $post->post_type ) {
			return $custom_statuses;
		}

		if ( current_user_can( 'manage_hris_medical' ) ) {
			return $custom_statuses;
		}

		$removed_statuses = array( 'approved', 'director-review', 'supervisor-review' );
		foreach ( $custom_statuses as $key => $status ) {
			if ( in_array( $status->slug, $removed_statuses ) ) {
				unset( $custom_statuses[ $key ] );
			}
		}

		return $custom_statuses;
	}

	public function scripts_and_styles() {
		if ( ! isset( $_GET['post_type'] ) && ! isset( $_GET['post'] ) )
			return;

		if ( isset( $_GET['post_type'] ) && HRIS_Medical_Post_Type::NAME !== $_GET['post_type'] )
			return;

		if ( isset( $_GET['post'] ) ) {
			$post = get_post( $_GET['post'] );
			if ( ! $post )
				return;

			if ( HRIS_Medical_Post_Type::NAME !== $post->post_type )
				return;
		}


		if(is_page('create_hris_medical_record')) {

		}

		wp_enqueue_script( 'hris-medical-edit-post-js', HRIS_MEDICAL_URI . '/js/edit-post.js', array( 'jquery', ), HRIS_MEDICAL_VERSION );
		wp_enqueue_style( 'hris-medical-edit-post-css', HRIS_MEDICAL_URI . '/css/edit-post.css', array(), HRIS_MEDICAL_VERSION, 'all' );
		wp_localize_script(
			'hris-medical-edit-post-js',
			'HRIS_MEDICAL_EDIT_POST_JS',
				array(
					'ajaxurl'           =>  admin_url( 'admin-ajax.php' ),
				)
			);
		//if ( !current_user_can( 'approve_hris_medical_records' ) ) {

		//	wp_enqueue_script( 'hris-medical-approve-js', HRIS_MEDICAL_URI . '/js/approval.js', array( 'jquery', ), HRIS_MEDICAL_VERSION );
		//}

	}

	public function post_actions() {
		if ( ! isset( $_GET['post'] ) )
			return;

		
		$post = get_post( $_GET['post'] );

		if ( HRIS_Medical_Post_Type::NAME !== $post->post_type )
			return;
		
		if ( 'print' === $_GET['action'] ) {
			$this->_print( $post->ID );
			//exit();
		}


		$display = get_current_screen();

		//use to chek if user is don't has rule after change status
		
		$current_status  = get_post_status( $post->ID ); 
		$user            = wp_get_current_user();
		$user_status     = $this->_get_status_by_user( $post->ID, $post->post_author, $user->ID);
		


		if ( $user_status[$user->ID] !== $current_status &&
			! hris_check_user_role( 'administrator' ) && 
			! hris_check_user_role( 'hris_approver' ) ) {
			wp_redirect( admin_url( 'edit.php?post_type=' . HRIS_Medical_Post_Type::NAME ) );
		}


		if ($user_status[$user->ID] !== $current_status &&
			(hris_check_user_role( 'hris_approver' ) || 
			hris_check_user_role( 'hris_manager' ) ) ) {
			wp_redirect( admin_url( 'edit.php?post_type=' . HRIS_Medical_Post_Type::NAME .
				'&page='. HRIS_Medical_Approval::SLUG) );
		}

		if ( ! isset( $_GET['action'] ) )
			return;

		if ( 'approve' === $_GET['action'] ) {

			$this->_do_approve( $post->ID );
		} else if ( 'reject' === $_GET['action'] ) {
			$this->_do_reject( $post->ID );
		} 
	}

	protected function _do_approve( $post_id ) {
		check_admin_referer( 'approve-post_' . $post_id );

		// Gets post object
		$post_obj = get_post( $post_id );

		$current_status = get_post_status( $post_id );

		$next_status    = hris_get_next_post_status( HRIS_Medical_Post_Type::NAME, $post_obj->post_author, $current_status );
		if ( ! $next_status )
			return;

		wp_update_post( array(
			'ID'          => $post_id,
			'post_status' => $next_status,
		) );

		// Updates approver in post meta based on next status.
		$approver = hris_get_approver_by_status( HRIS_Medical_Post_Type::NAME, $post_obj->post_author, $next_status );
		if ( $approver ) {
			update_post_meta( $post_id, 'approver', $approver );
		} else {
			delete_post_meta( $post_id, 'approver' );
		}
		do_action( 'updated_approver', $approver, $current_status, $next_status, $post_id );

		wp_redirect( add_query_arg( 'approved', 1, wp_get_referer() ) );
		exit();
	}

	protected function _do_reject( $post_id ) {
		check_admin_referer( 'reject-post_' . $post_id );

		$old_status = get_post_status( $post_id );

		wp_update_post( array(
			'ID'          => $post_id,
			'post_status' => HRIS_Approval_Setting::REJECTED,
		) );

		$approver = get_post_meta( $post_id, 'approver', true );

		do_action( 'updated_approver', $approver, $old_status, HRIS_Approval_Setting::REJECTED, $post_id );

		delete_post_meta( $post_id, 'approver' );


		wp_redirect( add_query_arg( 'rejected', 1, wp_get_referer() ) );
		exit();
	}



	public function get_balance_callback() {
		// Don't let IE cache this request
		if ($_GET['action'] !=='get_balance') {
			return;
		}
    	header("Pragma: no-cache");
    	header("Cache-Control: no-cache, must-revalidate");
    	header("Expires: Thu, 01 Jan 1970 00:00:00 GMT");

    	header("Content-type: text/json");
		pr($_GET);
		//$user = wp_get_current_user();
		$setting = get_user_option( 'medical_limit_users', get_current_user_id() );
		pr($setting);
		die();
	}


	protected function _get_list_user_approver ( $page_id, $author, $user_id  ) {
		$setting   = hris_get_component_from_module( 'approval', 'Setting' );
		//$approver = $setting->get_user_approvers(HRIS_Purchase_Post_Type::NAME, $author );
		$list_statuses = $setting->get_post_statuses( HRIS_Medical_Post_Type::NAME );		
		$user_approve = array();		
		foreach ($list_statuses as $key => $val ) {			
			$user_approve[$key]['post_id'] = $page_id;
			$user_approve[$key]['author'] = $author;
			$user_approve[$key]['user_id'] = hris_get_approver_by_status( HRIS_Medical_Post_Type::NAME, $author, $val );
			$user_approve[$key]['status'] = $val;
		} 
		return $user_approve;

	}

	protected function _get_status_by_user ($page_id, $author, $user_id) {
		$setting   = hris_get_component_from_module( 'approval', 'Setting' );
		//$approver = $setting->get_user_approvers(HRIS_Purchase_Post_Type::NAME, $author );
		$list_statuses = $setting->get_post_statuses( HRIS_Medical_Post_Type::NAME );		
		$user_approve = array();
		foreach ($list_statuses as $key => $val ) {			
			$user_approve[hris_get_approver_by_status( HRIS_Medical_Post_Type::NAME, $author, $val )] = $val;
		} 
		return $user_approve;
	}


	protected function _print( $post_id ) {
		check_admin_referer( 'print-post_' . $post_id );

		$post = get_post( $post_id );
		$GLOBALS['post'] = $post;
		setup_postdata( $post );

		include HRIS_MEDICAL_TEMPLATES . 'print.php';
		exit();
	}
}
