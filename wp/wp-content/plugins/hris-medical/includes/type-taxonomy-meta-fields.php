<?php

class HRIS_Medical_Type_Taxonomy_Meta_Fields implements HRIS_Medical_Component_Interface {
	public function load() {
		add_action( HRIS_Medical_Reimburs_Target::NAME . '_add_form_fields', array( $this, 'on_add_new' ) );
		add_action( HRIS_Medical_Reimburs_Target::NAME . '_edit_form_fields', array( $this, 'on_edit' ) );

		add_action( 'edited_term', array( $this, 'on_save' ) );
		add_action( 'create_term', array( $this, 'on_save' ) );
	}

	public function on_add_new() {
		?>
		<div class="form-field">
			<label for="term_meta[is_planned]"><?php _e( 'Is planned?', 'hris-medical' ); ?>
			<input type="checkbox" name="term_meta[is_planned]" id="term_meta[is_planned]" value="1" checked="checked">
			</label>
			<p>
			<?php _e( 'Please uncheck if you want to add as unplanned medical type.', 'hris-medical' ); ?>
			</p>
		</div>
		<?php
	}

	public function on_edit( $term ) {
		$term_id = $term->term_id;
		$meta    = get_option( 'taxonomy_' . $term_id );
		?>
		<tr class="form-field">
			<th scope="row" valign="top"><label for="term_meta[custom_term_meta]">
				<?php _e( 'Is planned?', 'hris-medical' ); ?></label>
			</th>
			<td>
				<input
					type="checkbox"
					name="term_meta[is_planned]"
					id="term_meta[is_planned]"
					value="1"
					<?php checked( $meta['is_planned'] ); ?>>
				<p class="description"><?php _e( 'Please uncheck if you want to add as unplanned medical type.', 'hris-medical' ); ?></p>
			</td>
		</tr>
		<?php
	}

	public function on_save( $term_id ) {
		if ( isset( $_POST['term_meta'] ) ) {
			$meta = get_option( 'taxonomy_' . $term_id );
			foreach ( array_keys( $_POST['term_meta'] ) as $key ) {
				if ( isset( $_POST['term_meta'][ $key ] ) ) {
					$meta[ $key ] = $_POST['term_meta'][ $key ];
				}
			}
			update_option( 'taxonomy_' . $term_id, $meta );
		}
	}
}
