<?php
/**
 * Plugin Name: HRIS Medical
 * Plugin URI: http://gedex.web.id
 * Description: Plugin that adds employee medical request management for HRIS.
 * Version: 0.1.0
 * Author: Akeda Bagus
 * Author URI: http://gedex.web.id
 * License: MIT
 *
 * Plugin that adds employee medical request management for HRIS
 *
 * @package HRIS Medical
 * @version 0.1.0
 * @author Akeda Bagus <admin@gedex.web.id>
 * @license MIT License
 */

/**
 * Class that bootstrap this plugin.
 *
 * @since 0.1.0
 */
class HRIS_Medical {

	/**
	 * Components to load.
	 *
	 * @since 0.1.0
	 * @var array
	 * @access protected
	 */
	protected $_components_to_load;

	public function __construct() {
		// Initialization.
		$this->is_scripts_and_styles_enqueued = false;
		$this->_components_to_load            = array(
			'Post_Type',
			'Type_Taxonomy',
			'Type_Taxonomy_Meta_Fields',
			'Edit_Table',
			'Edit_Post',
			'Setting_Limit',
			'Approval',
			'Layout_Setting',
			// 'Auto_Employee_Name_And_Spouse',
			'Author_Family_Acf_Field_Register',
			//'Email_Notification',
			'Report',
		);

		// Autoloader registration
		spl_autoload_register( array( $this, 'autoload' ) );

		// Set the constants needed by the plugin.
		add_action( 'plugins_loaded', array( $this, 'define_constants' ), 1 );

		// Load components.
		add_action( 'plugins_loaded', array( $this, 'load_components' ), 2 );
	}

	/**
	 * Autoloader for this plugin.
	 *
	 * @since 0.1.0
	 */
	public function autoload( $class ) {
		if ( false === strpos( $class, __CLASS__ ) )
			return;

		$class = str_replace( array( '_', 'hris-medical-' ), array( '-', '' ), strtolower( $class ) ) . '.php';

		require_once HRIS_MEDICAL_INCLUDES . $class;
	}

	/**
	 * Defines constants used by the plugin.
	 *
	 * @since 0.1.0
	 * @action plugins_loaded
	 */
	public function define_constants() {
		// Set the version number of the plugin.
		define( 'HRIS_MEDICAL_VERSION', '0.1.0' );

		// Set constant path to this plugin directory.
		define( 'HRIS_MEDICAL_DIR', trailingslashit( plugin_dir_path( __FILE__ ) ) );

		// Set constant path to this plugin URL.
		define( 'HRIS_MEDICAL_URI', trailingslashit( plugin_dir_url( __FILE__ ) ) );

		// Set the constant path to this includes directory.
		define( 'HRIS_MEDICAL_INCLUDES', HRIS_MEDICAL_DIR . trailingslashit( 'includes' ) );

		// Set the constant path to this templates directory.
		define( 'HRIS_MEDICAL_TEMPLATES', HRIS_MEDICAL_DIR . trailingslashit( 'templates' ) );
	}

	/**
	 * Load components.
	 *
	 * @since 0.1.0
	 * @action plugins_loaded
	 */
	public function load_components() {
		$GLOBALS['hris_medical_components'] = array();

		foreach ( $this->_components_to_load as $component ) {
			$class = 'HRIS_Medical_' . $component;
			$GLOBALS['hris_medical_components'][ $component ] = new $class;
			$GLOBALS['hris_medical_components'][ $component ]->load();
		}
	}
}

$GLOBALS['hris_medical'] = new HRIS_Medical();
