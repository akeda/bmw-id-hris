<!doctype html>
<html>
<head>
	<title>Print Medical Form</title>
	<style>
	#medical-print-container header li {
		list-style: none;
	}
	#medical-print-rows table {
		border-spacing: 0;
		border-collapse: collapse;
	}
	#medical-print-rows table th,
	#medical-print-rows table td {
		border: 1px solid #333;
		padding: 5px;
	}
	.numeric {
		text-align: right;
	}

	.label-notice {
		width: 100%;
		background-color: #808080;
	}
	.main {
		background: 0;
		width: 890px;
		padding: 56px 23px 15px;
	}


	.approver{
		position: relative;
		display:block;
		vertical-align: text-bottom;
		height:100px;
	}
	.wrap {
		display: block;
		position: absolute; 
		bottom: 0; 
		left: 0;
		width : 890px;
	}
	.wrap span {
		display:inline-table;
	}
	.hr-review {
		margin-left: 365px;
	}

	.marg {
		display:block;
		height :15px;
	}

	.border-out {
		margin: 0 auto;
		width:800px;
		border: 1px solid #333;
	}

	.sub-head, .content, .attach-out, .footer {
		border: 1px solid #333;
		padding: 5px;
	}

	.table-frame {
		margin-top: 30px;
		margin-left:40px;
		margin-bottom: 25px;
	}

	.attach-inner {
		display:block;
		height:150px;
	}
	legend {
		background-color: #808080;
		margin-left: 20px;
	}
	fieldset{
		display:block;
		height:80%;
	} 
	.sub-footer{
		display: block;
		
	}
	.left-foot {
		display: inline-block;
		border: 1px solid #333;
		width: 430px;
	}
	.center-foot {
		display: inline-block;
		border: 1px solid #333;
	}
	.right-foot {
		display: inline-block;
		border: 1px solid #333;
		margin-right:-4px; 
	}
	.box {
		height:135px;
		width:175px;
	}
	.content-foot {
		border-top: 1px solid #333;
		text-align: center;	
	}


	</style>
</head>
<body>
	<div id="medical-print-container">
		<div class="marg"></div>
		<div class="border-out">
		<header>
			<div class="sub-head">
			<ul>
				<li><h2>PT. BMW Group Indonesia</h2></li>
				<li></li>
				<li><?php echo esc_html( get_the_title( $post ) ); ?></li>
				<li>Request date: <?php echo esc_html( get_the_date( 'd/m/Y' ) ); ?></li>

				<?php
				$user      = get_user_by( 'id', $post->post_author );
				$total     = 0;
				$setting   = hris_get_component_from_module( 'approval', 'Setting' );
				$approvers = $setting->get_user_approvers( HRIS_Medical_Post_Type::NAME, $post->post_author );
				?>
				<li>Employee Number: <?php echo esc_html( $user->user_login ); ?></li>
				<li>Approve Date : <?php echo esc_html( get_the_modified_date( 'd/m/Y' ) ); ?></li>
				<li>Employee Name : <?php echo esc_html( $user->display_name ); ?></li>
			</ul>
		</div>
		</header>
		<div class="content">
		<div id="medical-print-rows">
			<div class="table-frame">
			<?php if ( have_rows( 'medical_record' ) ): ?>
			<table>
				<thead>
					<th>No</th>
					<th>Name</th>
					<th>Medical Type</th>
					<th>Claims</th>
					<th>Approved Budget</th>
				</thead>

				<tbody>
				<?php $no = 1; ?>
				<?php while ( have_rows( 'medical_record' ) ) : the_row(); ?>
					<tr>
						<td><?php echo $no++; ?></td>
						<td><?php

						$name = get_sub_field( 'user' );
						
						if (is_array($name)){
							if(!is_numeric($name['target_id'])){
								echo $name['target_id'];
							}else {
								echo $name['target_name'];
							}									
								
							
						} else {
							echo $name;
						}


						?></td>
						<td>
						<?php
						$term_id = get_sub_field( 'medical_type' );
						$term    = get_term( $term_id, HRIS_Medical_Type_Taxonomy::NAME );

						if ( $term && ! is_wp_error( $term ) ) {
							echo esc_html( $term->name );
						}
						?>
						</td>
						<td class="numeric">
							<?php echo esc_html(  number_format( get_sub_field( 'amount' ), 2, ',', '.' ) ); ?>
						</td>
						<td class="numeric">
						<?php
						$review = get_sub_field( 'review' );
						
						if (!is_float($review)) {
							$review = floatval($review);
						}

						$review = is_double($review)?$review:0;
						$total += floatval($review);
						
						echo esc_html(  number_format( $review, 2, ',', '.' ) );
						?>
						</td>
					</tr>
				<?php endwhile; ?>
				</tbody>

				<tfoot>
					<th class="numeric" colspan="4">Total</th>
					<th class="numeric">
					<?php echo esc_html( number_format( $total, 2, ',', '.' ) ); ?>
					</th>
				</tfoot>

			</table>
		</div>
		<!--end class content -->
		</div>	
		</div>
		<!--attachment -->
		<div class="attach-out">
			<div class="attach-inner">
				<fieldset>
				<legend>Attachment</legend>
				</fieldset>	
			</div>	
		</div>
		<!--end attachment -->
		<div class="footer">
			<div class="sub-footer">
			<div class="left-foot">	
			<div class="label-notice">
					<legend>Authorization</legend>
			</div>
			<div>Approved For and on behalf of BMW Indonesia</div>
			<div class="approver">
				<div class="wrap">
				<?php $i=0;$du=array();?>	
				<?php foreach ( $approvers as $approver_id ) : ?>
					<?php $user = get_user_by( 'id', $approver_id ); ?>
					<?php if ( is_a( $user, 'WP_User' ) ) : ?>
					<?php $du[$i]=$user->display_name;$i++; ?>
				<!--		<span><?php echo esc_html( $user->display_name ); ?></span>-->
					<?php endif; ?>
				<?php endforeach; ?>
				<?php endif; // end have_rows( 'medical_record' ) ?>
				<?php if(count($du)==2):?>
				<span><?php echo esc_html( $du[1] );?></span>
					<?php endif; ?>
				<?php if(count($du) > 0):?>
				<span class="hr-review"><?php //echo esc_html( $du[0] );?></span>
					<?php endif; ?>	

				</div>
			</div>
				<div>This is a computer generated document. No signature is required.</div>
			</div>
			<div class="right-foot">
				<div class="box"></div>
				<div class="content-foot">Controler</div>
			</div>
			<div class="right-foot">
				<div class="box"></div>
				<div class="content-foot">Receiver</div>
			</div>	
			</div>	
		<!--footer end -->	
		</div>
		
		<!--end div border-->
	</div>
	</div>
</body>
</html>
