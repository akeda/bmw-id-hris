<?php

extract( $GLOBALS['report_data'] );
/**
 * @var WP_Query                    $query     Instance of WP_Query
 * @var mixed                       $term      Term row from database
 * @var HRIS_Medical_Report_Summary $self      Instance of HRIS_Medical_Report_Summary
 * @var string                      $date_from Date with format dd-mm-YYYY
 * @var string                      $date_to   Date with format dd-mm-YYYY
 */

$term_glasses_patient = get_term_by(
	'slug',
	'glasses',
	HRIS_Medical_Type_Taxonomy::NAME
);

$term_biofocus_lense = get_term_by(
	'slug',
	'biofocus-lense',
	HRIS_Medical_Type_Taxonomy::NAME
);

$options = get_option( HRIS_Medical_Setting_Limit::NAME );
$term_lense = get_term_by(
							'slug',
							'lense',
							HRIS_Medical_Type_Taxonomy::NAME
							);
$term_frame = get_term_by(
							'slug',
							'frame',
							HRIS_Medical_Type_Taxonomy::NAME
							);
$term_soft_lense = get_term_by(
							'slug',
							'soft-lense',
							HRIS_Medical_Type_Taxonomy::NAME
							);

$records = array();
if ( is_a( $query, 'WP_Query' ) && $query->have_posts() ) {
	while ( $query->have_posts() ) :
		$query->the_post();

		$author_id = $GLOBALS['post']->post_author;
		if ( ! isset( $records[ $author_id ] ) ) {
			$records[ $author_id ] = array(
				'author_name'     => get_the_author(),
				'total_request'  => array(
					'frame' => 0,
					'lense'   => 0,
					'soft-lense' => 0,

				),
				'total_approved'  => array(
					'frame' => 0,
					'lense'   => 0,
					'soft-lense' => 0,

				),

				'entitle'=> array(
					'frame' => isset($options['medical_limit_users'][$author_id][$term_frame->term_id]['value'])?
					$options['medical_limit_users'][$author_id][$term_frame->term_id]['value'] : 0,
					'lense'   => isset($options['medical_limit_users'][$author_id][$term_lense->term_id]['value'])?
					$options['medical_limit_users'][$author_id][$term_lense->term_id]['value'] : 0,
					'soft-lense' => isset($options['medical_limit_users'][$author_id][$term_soft_lense->term_id]['value'])?
					$options['medical_limit_users'][$author_id][$term_soft_lense->term_id]['value'] : 0,

				),
			);



		}

		while ( have_rows( 'medical_record' ) ) : the_row();
			$term_id = get_sub_field( 'medical_type' );
			$term    = get_term( $term_id, HRIS_Medical_Type_Taxonomy::NAME );
			//$records[ $author_id ]['medical_limit_users']['frame']

			// Skip terms that don't belong to glasses.
			if ( $term->parent !== $term_glasses_patient->term_id ) {

				continue;
			}


			// Dental term will be grouped alone
			if ( $term_id === $term_biofocus_lense->term_id ) {

				$records[ $author_id ][ 'total_approved' ][ 'lense' ] += (double) get_sub_field( 'review' );
				$records[ $author_id ][ 'total_request' ][ 'lense' ] += (double) get_sub_field( 'ammount' );
			} else {
				// This will be grouped based on 'name' field.

				$target     = ( $term_id === $term->term_id )  ? $term->slug : 'lense';

				if ( ! in_array( $target, array( 'lense', 'soft-lense', 'frame' ) ) ) {
					$target = 'lense';
				}

				$option = isset($options['medical_limit_users'][$author_id][$term_id])?
				$options['medical_limit_users'][$author_id][$term_id]['value']: 0;

				$records[ $author_id ][ 'total_approved' ][ $target ] += (double) get_sub_field( 'review' );
				$records[ $author_id ][ 'total_request' ][ $target ] += (double) get_sub_field( 'amount' );
				//$records[ $author_id ]['entitle'][$term->slug] = $option;

			}
		endwhile;

	endwhile;
}

?>
<h2>Medical Glasses Summary Report</h2>

<header class="report-header">
	<form action="" method="GET">
	<?php $self->the_hidden_fields(); ?>
	<table>
		<tbody>
			<tr>
				<td>Date from</td>
				<td>
					<input type="text" id="date_from" name="date_from" class="datepicker" value="<?php echo is_a( $date_from, 'DateTime' ) ? $date_from->format( 'd-m-Y' ) : '' ?>" data-datepicker-args='{"defaultDate": "+1w", "numberOfMonths": 2, "changeMonth": true, "changeYear": true, "from": true, "toRel": "#date_to"}'>
				</td>
			</tr>
			<tr>
				<td>Date to</td>
				<td>
					<input type="text" id="date_to" name="date_to" class="datepicker" value="<?php echo is_a( $date_to, 'DateTime' ) ? $date_to->format( 'd-m-Y' ) : '' ?>" data-datepicker-args='{"defaultDate": "+1w", "numberOfMonths": 2, "changeMonth": true, "changeYear": true, "to": true, "fromRel": "#date_from"}'>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>
					<input type="submit" class="button" value="Filter">
				</td>
		</tbody>
	</table>
	<form>
</header>

<div class="report-content">
	<table>
		<thead>
			<tr>
				<th rowspan="2">No</th>
				<th rowspan="2">Employee</th>
				<th colspan="3">Frame</th>
				<th colspan="3">Lense</th>
				<th colspan="3">Soft-Lense</th>
			</tr>
			<tr>
				<th>Entitlement</th>
				<th>Request</th>
				<th>Approve</th>
				<th>Entitlement</th>
				<th>Request</th>
				<th>Approve</th>
				<th>Entitlement</th>
				<th>Request</th>
				<th>Approve</th>
			</tr>
		</thead>
		<tbody>
		<?php if ( empty( $records ) ): ?>
			<tr>
				<td colspan="11">
					<p class="no-records">No records found. Please use filter above.</p>
				</td>
			</tr>
		<?php else: ?>
			<?php $no = 1; ?>
			<?php foreach ( $records as $user_id => $record ) : ?>
				<tr>
					<td><?php echo $no++; ?></td>
					<td><?php echo esc_html( $record['author_name'] ); ?></td>
					<td><?php echo esc_html( $record['entitle']['frame'] );; ?></td>
					<td><?php echo esc_html( $record['total_request']['frame'] ); ?></td>
					<td><?php echo esc_html( $record['total_approved']['frame'] ); ?></td>
					<td><?php echo esc_html( $record['entitle']['lense'] ); ?></td>
					<td><?php echo esc_html( $record['total_request']['lense'] ); ?></td>
					<td><?php echo esc_html( $record['total_approved']['lense'] ); ?></td>
					<td><?php echo esc_html( $record['entitle']['soft-lense'] ); ?></td>
					<td><?php echo esc_html( $record['total_request']['soft-lense'] ); ?></td>
					<td><?php echo esc_html( $record['total_approved']['soft-lense'] ); ?></td>

				</tr>
			<?php endforeach; ?>
		<?php endif; ?>
		</tbody>
</table>
</div>
