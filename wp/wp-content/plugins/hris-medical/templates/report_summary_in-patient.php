<?php

extract( $GLOBALS['report_data'] );
/**
 * @var WP_Query                    $query     Instance of WP_Query
 * @var mixed                       $term      Term row from database
 * @var HRIS_Medical_Report_Summary $self      Instance of HRIS_Medical_Report_Summary
 * @var string                      $date_from Date with format dd-mm-YYYY
 * @var string                      $date_to   Date with format dd-mm-YYYY
 */

$term_out_patient = get_term_by(
	'slug',
	'out-patient',
	HRIS_Medical_Type_Taxonomy::NAME
);

$term_inpatient = get_term_by(
	'slug',
	'in-patient',
	HRIS_Medical_Type_Taxonomy::NAME
);

$options = get_option( HRIS_Medical_Setting_Limit::NAME );

$records = array();
if ( is_a( $query, 'WP_Query' ) && $query->have_posts() ) {
	while ( $query->have_posts() ) :
		$query->the_post();

		$author_id = $GLOBALS['post']->post_author;
		if ( ! isset( $records[ $author_id ] ) ) {
			$records[ $author_id ] = array(
				'author_name'     => get_the_author(),
				'total_approved'  => array(
					'in-patient' => 0,
					'cesarean'   => 0,
				),
				'entitle'=> array(
					'in-patient' => isset($options['medical_limit_users'][$author_id][$term_inpatient->term_id]['value'])?
					$options['medical_limit_users'][$author_id][$term_inpatient->term_id]['value'] : 0,

				),
			);
		}

		while ( have_rows( 'medical_record' ) ) : the_row();
			$term_id = get_sub_field( 'medical_type' );
			$term    = get_term( $term_id, HRIS_Medical_Type_Taxonomy::NAME );

			// Skip terms that don't belong to out-patient.
			if ( $term->parent !== $term_inpatient->term_id && $term_id !== $term_inpatient->term_id  ) {
				continue;
			}


				$records[ $author_id ][ 'total_approved' ][ 'in-patient' ] += (double) get_sub_field( 'review' );

		endwhile;

	endwhile;
}

?>
<h2>Medical In Patient Summary Report</h2>

<header class="report-header">
	<form action="" method="GET">
	<?php $self->the_hidden_fields(); ?>
	<table>
		<tbody>
			<tr>
				<td>Date from</td>
				<td>
					<input type="text" id="date_from" name="date_from" class="datepicker" value="<?php echo is_a( $date_from, 'DateTime' ) ? $date_from->format( 'd-m-Y' ) : '' ?>" data-datepicker-args='{"defaultDate": "+1w", "numberOfMonths": 2, "changeMonth": true, "changeYear": true, "from": true, "toRel": "#date_to"}'>
				</td>
			</tr>
			<tr>
				<td>Date to</td>
				<td>
					<input type="text" id="date_to" name="date_to" class="datepicker" value="<?php echo is_a( $date_to, 'DateTime' ) ? $date_to->format( 'd-m-Y' ) : '' ?>" data-datepicker-args='{"defaultDate": "+1w", "numberOfMonths": 2, "changeMonth": true, "changeYear": true, "to": true, "fromRel": "#date_from"}'>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>
					<input type="submit" class="button" value="Filter">
				</td>
		</tbody>
	</table>
	<form>
</header>

<div class="report-content">
	<table>
		<thead>
			<tr>
				<th rowspan="2">No</th>
				<th rowspan="2">Employee</th>
				<th rowspan="2">Entitle  Claim</th>
				<th rowspan="2">Total Claim</th>
				<th rowspan="2">Remaining Balance</th>
			</tr>

		</thead>
		<tbody>
		<?php if ( empty( $records ) ): ?>
			<tr>
				<td colspan="5">
					<p class="no-records">No records found. Please use filter above.</p>
				</td>
			</tr>
		<?php else: ?>
			<?php $no = 1; ?>
			<?php foreach ( $records as $user_id => $record ) : ?>
				<tr>
					<td><?php echo $no++; ?></td>
					<td><?php echo esc_html( $record['author_name'] ); ?></td>
					<td><?php echo esc_html( number_format($record['entitle']['in-patient'], 2, ',', '.' ) ); ?></td>
					<td><?php echo esc_html( number_format($record['total_approved']['in-patient'], 2, ',', '.' ) ); ?></td>
					<td><?php
					$balance = $record['entitle']['in-patient'] - $record['total_approved']['in-patient'];
					echo number_format( $balance, 2, ',', '.' );
					?></td>

				</tr>
			<?php endforeach; ?>
		<?php endif; ?>
		</tbody>
</table>
</div>
