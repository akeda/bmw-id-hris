<?php extract( $GLOBALS['report_data'] ); ?>
<h2>Outpatient Detail Report</h2>

<header class="report-header">
	<form action="" method="GET">
	<?php $self->the_hidden_fields(); ?>
	<table>
		<tbody>
			<tr>
				<td>Date from</td>
				<td>
					<input type="text" id="date_from" name="date_from" class="datepicker" value="<?php echo is_a( $date_from, 'DateTime' ) ? $date_from->format( 'd-m-Y' ) : '' ?>" data-datepicker-args='{"defaultDate": "+1w", "numberOfMonths": 2, "changeMonth": true, "changeYear": true, "from": true, "toRel": "#date_to"}'>
				</td>
			</tr>
			<tr>
				<td>Date to</td>
				<td>
					<input type="text" id="date_to" name="date_to" class="datepicker" value="<?php echo is_a( $date_to, 'DateTime' ) ? $date_to->format( 'd-m-Y' ) : '' ?>" data-datepicker-args='{"defaultDate": "+1w", "numberOfMonths": 2, "changeMonth": true, "changeYear": true, "to": true, "fromRel": "#date_from"}'>
				</td>
			</tr>
			<tr>
				<td>Employee Name</td>
				<td>
					<strong><?php echo esc_html( $employee->display_name ); ?></strong>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>
					<input type="submit" class="button" value="Filter">
				</td>
		</tbody>
	</table>
	<form>
</header>

<div class="report-content">
	<table>
		<thead>
			<tr>
				<th rowspan="2">No</th>
				<th rowspan="2">Reimbursement For</th>
				<th rowspan="2">Outpatient type</th>
				<th colspan="2">Amount</th>
				<th colspan="2">Remarks</th>
				<th rowspan="2">Status</th>
			</tr>
			<tr>
				<th>Requested</th>
				<th>Approved</th>
				<th>Verification</th>
				<th>HRM</th>
			</tr>
		</thead>
		<tbody>
		<?php if ( is_a( $query, 'WP_Query' ) && $query->have_posts() ) : ?>
			<?php
			$no             = 1; // Incremental record number to be displayed.
			$total_approved = 0; // Total approved.
			?>
			<?php while( $query->have_posts() ) : $query->the_post(); ?>
				<?php if ( have_rows( 'medical_record' ) ) : // ACF repeater ?>
				<?php while ( have_rows( 'medical_record' ) ) : the_row(); ?>
				<tr>
					<td><?php echo $no++; ?></td>
					<td><?php 
					$medical_user = get_sub_field( 'user' );
					
					echo esc_html( $medical_user['target_name'] );
					?></td>
					<td>
					<?php
					$term_id = get_sub_field( 'medical_type' );
					$term    = get_term( $term_id, HRIS_Medical_Type_Taxonomy::NAME );

					if ( $term && ! is_wp_error( $term ) ) {
						echo esc_html( $term->name );
					}
					?>
					</td>
					<td class="numeric">
						<?php echo esc_html( number_format( get_sub_field( 'amount' ), 2, ',', '.' ) ); ?>
					</td>
					<td class="numeric">
					<?php
					$approved        = get_sub_field( 'review' );
					$filter_approved = !is_string($approved)? $approved : 0;
					
					$total_approved += floatval( $filter_approved );

					echo esc_html(  number_format( $filter_approved, 2, ',', '.' ) );
					?>
					</td>
					<td><?php the_sub_field( 'remarks' ); ?></td>
					<td><?php the_sub_field( 'reason' ); ?></td>
					<td>
					<?php
					
					?>
					</td>
				</tr>
				<?php endwhile; ?>
				<?php endif; ?>
			<?php endwhile; ?>
		<?php else : ?>
			<tr>
				<td colspan="9">
					<p class="no-records">No records found. Please use filter above.</p>
				</td>
			</tr>
		<?php endif; ?>
		</tbody>
	</table>
</div>
