<?php
/**
 * Plugin Name: HRIS Common Styles
 * Plugin URI: http://gedex.web.id
 * Description: Enqueue common styles used by HRIS.
 * Author: Akeda Bagus
 * Author URI: http://gedex.web.id/
 * Version: 0.1.0
 *
 * Enqueue common styles used by HRIS.
 *
 * @license MIT License.
 */

if ( ! defined( 'HRIS_COMMON_CSS_DIR' ) )
	define( 'HRIS_COMMON_CSS_DIR', trailingslashit( plugin_dir_path( __FILE__ ) ) );

if ( ! defined( 'HRIS_COMMON_CSS_URL' ) )
	define( 'HRIS_COMMON_CSS_URL', trailingslashit( plugin_dir_url( __FILE__ ) ) );

function _hris_common_css_enqueue_scripts_in_admin() {
	$styles_to_register = array(
		'hris-jquery-ui' => array(
			'src'       => HRIS_COMMON_CSS_URL . 'jquery-ui/jquery-ui.min.css',
			'deps'      => array(),
			'ver'       => '0.1.0',
			'media'     => 'all',
		),
		'hris-jquery-ui-theme' => array(
			'src'       => HRIS_COMMON_CSS_URL . 'jquery-ui/jquery.ui.theme.css',
			'deps'      => array( 'hris-jquery-ui' ),
			'ver'       => '0.1.0',
			'media'     => 'all',
		),
		'hris-common' => array(
			'src'       => HRIS_COMMON_CSS_URL . 'hris-common/hris-common.css',
			'deps'      => array(),
			'ver'       => '0.1.0',
			'media'     => 'all',
		),
	);
	foreach ($styles_to_register as $handle => $style) {
		/**
		 * @var string $src
		 * @var array  $deps
		 * @var string $ver
		 * @var string $media
		 */
		extract( $style );
		wp_register_style( $handle, $src, $deps, $ver, $media );
	}
}
add_action( 'admin_enqueue_scripts', '_hris_common_css_enqueue_scripts_in_admin' );
