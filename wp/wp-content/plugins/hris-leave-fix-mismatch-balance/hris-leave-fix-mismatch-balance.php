<?php
/**
 * Plugin Name: HRIS Leave Fix Mismatch Balance
 * Plugin URI: http://gedex.web.id
 * Description: One-time install plugin to fix mismatch leave balance.
 * Version: 0.1.0
 * Author: Akeda Bagus
 * Author URI: http://gedex.web.id
 * License: MIT
 *
 * 1. Fixes user meta `leave_balance`
 */
class HRIS_Leave_Fix_Mismatch_Balance {

	public function run() {
		$users = $this->get_users();

		$year_start = apply_filters( 'hris_leave_fix_mismatch_balance_year_start', 2013 );
		$year_end   = apply_filters( 'hris_leave_fix_mismatch_balance_year_end',   date( 'Y' ) );

		$params = compact( 'year_start', 'year_end' );
		foreach ($users as $user_id => $display_name) {
			$params['user_id']      = $user_id;
			$params['display_name'] = $display_name;

			$this->fix_balance( $params );
		}
	}

	public function get_users() {
		$setting = hris_get_component_from_module( 'approval', 'Setting' );
		$users   = $setting->get_users();

		return $users;
	}

	public function fix_balance( array $params ) {
		extract($params);
		/**
		 * @var string|int $year_start
		 * @var string|int $year_end
		 * @var int        $user_id
		 * @var string     $display_name
		 */

		// @todo get all leave records on current year.
		$meta_leave = get_user_meta( $user_id, HRIS_Leave_Balance::LEAVE_BALANCE_USER_META, true );

		printf( 'Fixing balance for %s', $display_name );
		for ($y = $year_start; $y <= $year_end; $y++) {
			$query = new WP_Query( array(
				'post_type'   => HRIS_Leave_Post_Type::NAME,
				'post_status' => HRIS_Approval_Setting::APPROVED,
				'meta_query'  => array(
					''
				),
			) );

			// @fix balance on user meta on current year
		}
	}
}

add_action( 'plugins_loaded', function() {
	// $fixer = new HRIS_Leave_Fix_Mismatch_Balance();
	// $fixer->run();
} );
