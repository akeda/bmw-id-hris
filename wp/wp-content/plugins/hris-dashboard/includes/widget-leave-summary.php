<?php

/**
 * Class that adds leave summary widget into WordPress dashboard.
 *
 * @since 0.1.0
 */
class HRIS_Dashboard_Widget_Leave_Summary extends HRIS_Dashboard_Widget implements HRIS_Dashboard_Component_Interface {

	public function __construct() {
		parent::__construct( 'dashboard_hris_leave_summary', array(
			'title'   => __( 'Your Leave Summary', 'hris-dashboard' ),
			'context' => 'side',
		) );
	}

	/**
	 * Callback to render the widget in WordPress dashboard.
	 *
	 * @since 0.1.0
	 */
	public function callback() {
		$user    = wp_get_current_user();
		$year    = intval( date( 'Y' ) );
		$balance = hris_get_component_from_module( 'leave', 'Balance' );
		$user_b  = $balance->get_leave_balance( $user->ID, $year );
		?>
		<h3><?php printf( __( 'Year %d', 'hris-dashboard' ), $year ); ?></h3>
		<table align="center">
			<tbody>
				<tr>
					<th><strong><?php _e( 'Number of Vacation', 'hris-dashboard' ); ?></strong></th>
					<th><strong><?php _e( 'Taken', 'hris-dashboard' ); ?></strong></th>
					<th><strong><?php _e( 'Remaining Balance', 'hris-dashboard' ); ?></strong></th>
				</tr>
				<tr>
					<td valign="top">
						<?php echo esc_html( $user_b['balance'] ); ?></p>
					</td>
					<td valign="top">
						<?php echo esc_html( $balance->get_total_approved( $user->ID, $year ) ); ?>
					</td>
					<td valign="top">
						<?php echo esc_html( $balance->get_remaining_leave( $user->ID, $year ) ); ?>
					</td>
				</tr>
			</tbody>
		</table>
		<?php
	}

	public function control_callback() {}
}
