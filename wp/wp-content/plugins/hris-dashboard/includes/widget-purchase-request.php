<?php

/**
 * Class that adds purchase request widget into WordPress dashboard.
 *
 * @since 0.1.0
 */
class HRIS_Dashboard_Widget_Purchase_Request extends HRIS_Dashboard_Widget implements HRIS_Dashboard_Component_Interface {

	public function __construct() {
		parent::__construct( 'dashboard_hris_purchase_request', array(
			'title'   => __( 'Your Purchase Request', 'hris-dashboard' ),
			'context' => 'side',
		) );
	}

	/**
	 * Callback to render the widget in WordPress dashboard.
	 *
	 * @since 0.1.0
	 */
	public function callback() {
		$user = wp_get_current_user();
		?>
		<?php
	}

	public function control_callback() {}
}
