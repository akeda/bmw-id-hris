<?php

/**
 * Class that adds excerpt profile widget into WordPress dashboard.
 *
 * @since 0.1.0
 */
class HRIS_Dashboard_Widget_Excerpt_Profile extends HRIS_Dashboard_Widget implements HRIS_Dashboard_Component_Interface {

	public function __construct() {
		parent::__construct( 'dashboard_hris_excerpt_profile', array(
			'title'   => __( 'Your Profile', 'hris-dashboard' ),
			'context' => 'normal',
		) );
	}

	/**
	 * Callback to render the widget in WordPress dashboard.
	 *
	 * @since 0.1.0
	 */
	public function callback() {
		$user = wp_get_current_user();
		?>
		<div class="table table_profile">
			<?php echo get_avatar( $user->ID, 50, 'mystery' ); ?>
			<table>
				<tr class="first">
					<td class="first"><?php esc_html_e( 'Name', 'hris-dashboard' ) ?></td>
					<td class="b"><?php echo esc_html( $user->user_nicename ) ?></td>
				</tr>
				<tr>
					<td class="first"><?php esc_html_e( 'Email', 'hris-dashboard' ) ?></td>
					<td class="b"><?php echo esc_html( $user->user_email ) ?></td>
				</tr>
			</table>
		</div>
		<p class="submit">
			<a href="<?php echo esc_url( admin_url( 'profile.php' ) ) ?>" class="button-primary"><?php esc_html_e( 'Edit profile', 'hris-dashboard' ) ?></a>
			<br class="clear" />
		</p>
		<?php
	}

	/**
	 * Callback that handles submission of widget options.
	 */
	public function control_callback() {}
}
