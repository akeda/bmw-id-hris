<?php

/**
 * Class that removes unwanted meta boxes from WordPress dashboard.
 *
 * @since 0.1.0
 */
class HRIS_Dashboard_Removes_Unwanted_Meta_Boxes implements HRIS_Dashboard_Component_Interface {

	/**
	 * List of unwanted meta boxes that needs to be removed from dashboard.
	 *
	 * @var array
	 * @access protected
	 */
	protected $_removed_meta_boxes = array(
		'dashboard_primary' => array(
			'screen'  => 'dashboard',
			'context' => 'side',
		),
		'dashboard_secondary' => array(
			'screen'  => 'dashboard',
			'context' => 'side',
		),
		'dashboard_recent_comments' => array(
			'screen'  => 'dashboard',
			'context' => 'normal',
		),
		'dashboard_incoming_links' => array(
			'screen'  => 'dashboard',
			'context' => 'normal',
		),
		'dashboard_quick_press' => array(
			'screen'  => 'dashboard',
			'context' => 'side',
		),
		'dashboard_recent_drafts' => array(
			'screen'  => 'dashboard',
			'context' => 'side',
		),
		'dashboard_plugins' => array(
			'screen'  => 'dashboard',
			'context' => 'normal',
		),
		'dashboard_right_now' => array(
			'screen'  => 'dashboard',
			'context' => 'normal',
		),
		'dashboard_activity' => array(
			'screen'  => 'dashboard',
			'context' => 'normal',
		),
	);

	/**
	 * Actions performed during component load.
	 *
	 * @since 0.1.0
	 */
	public function load() {
		add_action( 'wp_dashboard_setup', array( $this, 'remove_meta_boxes' ) );
	}

	/**
	 * Remove meta boxes defined in `$_removed_meta_boxes` property.
	 *
	 * @since 0.1.0
	 * @action wp_dashboard_setup
	 */
	public function remove_meta_boxes() {
		foreach ( $this->_removed_meta_boxes as $id => $param ) {
			if ( ! isset( $param['screen'] ) || ! isset( $param['context'] ) ) {
				continue;
			}

			if ( is_array( $param['context'] ) ) {
				foreach ( $param['context'] as $context ) {
					remove_meta_box( $id, $param['screen'], $context );
				}
			} else {
				remove_meta_box( $id, $param['screen'], $param['context'] );
			}
		}
	}
}
