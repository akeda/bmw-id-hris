<?php

/**
 * Class that adds travel request summary widget into WordPress dashboard.
 *
 * @since 0.1.0
 */
class HRIS_Dashboard_Widget_Travel_Request extends HRIS_Dashboard_Widget implements HRIS_Dashboard_Component_Interface {

	public function __construct() {
		parent::__construct( 'dashboard_hris_travel_request', array(
			'title'   => __( 'Your Travel Request', 'hris-dashboard' ),
			'context' => 'side',
		) );
	}

	/**
	 * Callback to render the widget in WordPress dashboard.
	 *
	 * @since 0.1.0
	 */
	public function callback() {
		$user = wp_get_current_user();
		?>
		<?php
	}

	public function control_callback() {}
}
