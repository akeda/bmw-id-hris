<?php

/**
 * Interface that plugin-component's class MUST implement.
 *
 * @package HRIS Dashboard
 * @since 0.1.0
 * @author Akeda Bagus <akeda@x-team.com>
 */
interface HRIS_Dashboard_Component_Interface {
	public function load();
}
