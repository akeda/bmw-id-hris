<?php

abstract class HRIS_Dashboard_Widget {
	/**
	 * Widget ID.
	 *
	 * @since 0.1.0
	 * @var string
	 * @access private
	 */
	protected $_id;

	/**
	 * Widget title.
	 *
	 * @since 0.1.0
	 * @var string
	 * @access private
	 */
	protected $_title;

	/**
	 * Widget context.
	 *
	 * @since 0.1.0
	 * @var string
	 * @access private
	 */
	protected $_context;

	/**
	 * Constructor.
	 *
	 * @since 0.1.0
	 * @param string $id Widget ID
	 * @param array $params Widget parameters
	 * @access public
	 * @return void
	 */
	public function __construct( $id, $params = array() ) {
		$this->_id = $id;

		// Required params.
		foreach ( array( 'title', 'context' ) as $required ) {
			if ( ! isset( $params[ $required ] ) ) {
				throw new Exception( sprintf( 'Missing required param %s when instantiating widget of %s', $required, __CLASS__ ) );
			}
		}

		$this->_title   = $params['title'];
		$this->_context = $params['context'];

	}

	/**
	 * Actions performed during component load.
	 *
	 * @since 0.1.0
	 */
	public function load() {
		// Adds the widget then moves the widget to 'side'.
		add_action( 'wp_dashboard_setup', array( $this, 'dashboard_setup' ) );
	}

	/**
	 * Register the widget and set the context.
	 *
	 * @since 0.1.0
	 * @action wp_dashboard_setup
	 */
	public function dashboard_setup() {
		global $wp_meta_boxes;

		wp_add_dashboard_widget( $this->_id, $this->_title, array( $this, 'callback' ), array( $this, 'control_callback' ) );

		foreach ( array( 'normal', 'side', 'advanced', ) as $context ) {
			if ( $context === $this->_context ) {
				continue;
			}

			$is_widget_can_be_moved = (
				isset( $wp_meta_boxes['dashboard'][ $context ][ 'core' ][ $this->_id ] )
				&&
				is_array( $wp_meta_boxes['dashboard'][ $context ][ 'core' ][ $this->_id ] )
			);

			if ( $is_widget_can_be_moved ) {
				$widget = $wp_meta_boxes['dashboard'][ $context ][ 'core' ][ $this->_id ];
				unset( $wp_meta_boxes['dashboard'][ $context ][ 'core' ][ $this->_id ] );
				$wp_meta_boxes['dashboard'][ $this->_context ][ 'core' ][ $this->_id ] = $widget;
			}
		}
	}

	abstract public function callback();

	abstract public function control_callback();
}
