<?php

/**
 * Class that removes unwanted meta boxes from WordPress dashboard.
 *
 * @since 0.1.0
 */
class HRIS_Dashboard_Removes_Welcome_Panel implements HRIS_Dashboard_Component_Interface {

	/**
	 * Actions performed during component load.
	 *
	 * @since 0.1.0
	 */
	public function load() {
		remove_action( 'welcome_panel', 'wp_welcome_panel' );
	}
}
