<?php
/**
 * Plugin Name: HRIS Dashboard
 * Plugin URI: http://gedex.web.id
 * Description: Plugin that customizes WordPress dashboard for HRIS.
 * Version: 0.1.0
 * Author: Akeda Bagus
 * Author URI: http://gedex.web.id
 * License: MIT
 *
 * Plugin that customizes WordPress dashboard for HRIS.
 *
 * @package HRIS Dashboard
 * @version 0.1.0
 * @author Akeda Bagus <admin@gedex.web.id>
 * @license MIT License
 */

/**
 * Class that bootstrap this plugin.
 *
 * @since 0.1.0
 */
class HRIS_Dashboard {

	/**
	 * Whether all scripts and styles need by this plugin were enqueued already.
	 *
	 * @since 0.1.0
	 * @var bool
	 * @access private
	 */
	private $_is_scripts_and_styles_enqueued;

	protected $_components_to_load;

	public function __construct() {
		// Initialization.
		$this->is_scripts_and_styles_enqueued = false;
		$this->_components_to_load            = array(
			'Widget_Excerpt_Profile',
			'Widget_Leave_Summary',
			'Widget_Travel_Request',
			'Widget_Purchase_Request',
			'Removes_Unwanted_Meta_Boxes',
			'Removes_Welcome_Panel',
		);

		// Autoloader registration
		spl_autoload_register( array( $this, 'autoload' ) );

		// Set the constants needed by the plugin.
		add_action( 'plugins_loaded', array( $this, 'define_constants' ), 1 );

		// Load components.
		add_action( 'plugins_loaded', array( $this, 'load_components' ), 2 );

		// Enqueue scripts and styles
		add_action( 'wp_dashboard_setup', array( $this, 'load_scripts_and_styles' ) );
	}

	/**
	 * Autoloader for this plugin.
	 *
	 * @since 0.1.0
	 */
	public function autoload( $class ) {
		if ( false === strpos( $class, __CLASS__ ) )
			return;

		$class = str_replace( array( '_', 'hris-dashboard-' ), array( '-', '' ), strtolower( $class ) ) . '.php';

		require_once HRIS_DASHBOARD_INCLUDES . $class;
	}

	/**
	 * Defines constants used by the plugin.
	 *
	 * @since 0.1.0
	 * @action plugins_loaded
	 */
	public function define_constants() {
		// Set the version number of the plugin.
		define( 'HRIS_DASHBOARD_VERSION', '0.1.0' );

		// Set constant path to this plugin directory.
		define( 'HRIS_DASHBOARD_DIR', trailingslashit( plugin_dir_path( __FILE__ ) ) );

		// Set constant path to this plugin URL.
		define( 'HRIS_DASHBOARD_URI', trailingslashit( plugin_dir_url( __FILE__ ) ) );

		// Set the constant path to this includes directory.
		define( 'HRIS_DASHBOARD_INCLUDES', HRIS_DASHBOARD_DIR . trailingslashit( 'includes' ) );
	}

	/**
	 * Load components.
	 *
	 * @since 0.1.0
	 * @action plugins_loaded
	 */
	public function load_components() {
		$GLOBALS['hris_dashboard_components'] = array();

		foreach ( $this->_components_to_load as $component ) {
			$class = 'HRIS_Dashboard_' . $component;
			$GLOBALS['hris_dashboard_components'][ $component ] = new $class;
			$GLOBALS['hris_dashboard_components'][ $component ]->load();
		}
	}

	/**
	 * Enqueue scripts and styles.
	 *
	 * @since 0.1.0
	 * @action wp_dashboard_setup
	 */
	public function load_scripts_and_styles() {

		if ( $this->_is_scripts_and_styles_enqueued ) {
			return;
		}

		wp_enqueue_style( 'hris-dashboard-widgets', plugins_url( '/css/hris-dashboard-widgets.css', __FILE__ ) );

		$this->_is_scripts_and_styles_enqueued = true;
	}
}

$GLOBALS['hris_dashboard'] = new HRIS_Dashboard();
