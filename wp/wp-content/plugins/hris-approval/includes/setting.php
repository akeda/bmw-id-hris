<?php

class HRIS_Approval_Setting {

	const NAME = 'hris_approval_settings';

	const APPROVED = 'approved';

	const REJECTED = 'rejected';

	/**
	 * Slug of setting page.
	 *
	 * @since 0.1.0
	 * @var string
	 * @access public
	 */
	public $setting_page;

	/**
	 * Available post types.
	 *
	 * @since 0.1.9
	 * @var array
	 * @access protected
	 */
	protected $_avail_post_types;

	/**
	 * One request cache so that we don't need to call get_users multiple times.
	 *
	 * @since 0.1.0
	 * @var array
	 * @access protected
	 */
	protected $_cached_users;

	/**
	 * One request cache so that we don't call get_settings method multiple times.
	 *
	 * @since 0.1.0
	 * @var array
	 * @access protected
	 */
	protected $_cached_user_approvers = array();

	/**
	 * One request cache so that we don't call get_settings method multiple times.
	 *
	 * @since 0.1.0
	 * @var array
	 * @access protected
	 */
	protected $_cached_post_statuses = array();

	/**
	 * One request cache so that we don't call get_settings method multiple times.
	 *
	 * @since 0.1.0
	 * @var array
	 * @access protected
	 */
	protected $_cached_enabled_post_types = array();

	/**
	 * Method that's called by plugin's bootstrapper.
	 *
	 * @since 0.1.0
	 * @return void
	 */
	public function load() {
		add_action( 'admin_menu', array( $this, 'settings_page_setup' ) );

		add_action( 'admin_enqueue_scripts', array( $this, 'load_scripts_and_styles' ) );
	}

	/**
	 * Page setup for this setting.
	 *
	 * @since 0.1.0
	 * @action admin_menu
	 * @return void
	 */
	public function settings_page_setup() {
		add_action( 'admin_init', array( $this, 'register_settings' ) );

		$this->setting_page = add_submenu_page(
			'options-general.php',
			__( 'Approval Settings', 'hris-approval' ),
			__( 'Approval', 'hris-approval' ),
			'manage_options',
			self::NAME,
			array( $this, 'render_page' )
		);
	}

	public function load_scripts_and_styles( $hook ) {
		if ( 'settings_page_' . self::NAME !== $hook )
			return;

		wp_enqueue_style( 'hris_approval_setting_style', HRIS_APPROVAL_URI . '/css/hris-approval-setting.css' );
	}

	/**
	 * Register settings.
	 *
	 * @since 0.1.0
	 * @action admin_init
	 * @return void
	 */
	public function register_settings() {
		add_settings_section( 'default', '', '__return_empty_string', self::NAME );

		register_setting( self::NAME, self::NAME, array( $this, 'validate_settings' ) );

		add_settings_field(
			'enabled_post_types',
			__( 'Enabled post type', 'hris-approval' ),
			array( $this, 'render_enabled_post_types' ),
			self::NAME
		);

		$settings  = $this->_get_settings();
		foreach ( $settings as $post_type => $setting ) {
			if ( ! isset( $setting['enabled'] ) )
				continue;

			if ( ! $setting['enabled'] )
				continue;

			add_settings_section( 'default', '', '__return_empty_string', self::NAME );

			add_settings_field(
				'approval_matrix_' . $post_type,
				sprintf( __( 'Approval matrix %s', 'hris-approval' ), $post_type ),
				array( $this, 'render_approval_matrix' ),
				self::NAME,
				'default',
				array(
					'post_type' => $post_type,
					'setting'   => $setting,
				)
			);
		}
	}

	/**
	 * Callback for add_settings_field.
	 *
	 * @since 0.1.0
	 * @return void
	 */
	public function render_enabled_post_types() {
		$post_types = $this->_get_post_types();
		$settings   = $this->_get_settings();
		?>
		<table class="form-table">
			<tbody>
				<?php foreach ( $post_types as $post_type ) : ?>
				<?php
				$post_type_setting = isset( $settings[ $post_type->name ] ) ? $settings[ $post_type->name ] : array();
				?>
				<tr>
					<th>
						<label><?php esc_html_e( $post_type->labels->name ); ?></label>
					</th>
					<td valign="top">
						<?php
						$field = sprintf( '%s[%s][enabled]', self::NAME, $post_type->name );
						$value = (
							isset( $post_type_setting['enabled'] )
							&&
							$post_type_setting['enabled']
						)
						? true : false;
						?>
						<label>
							<input
								name="<?php echo esc_attr( $field ); ?>"
								id="<?php echo esc_attr( $field ); ?>"
								type="checkbox"
								value="1"
								<?php checked( $value ); ?>>
								<?php _e( 'Active', 'hris-approval' ) ?>
						</label>
						<br>
						<?php
						$field = sprintf( '%s[%s][num_levels]', self::NAME, $post_type->name );
						$value = (
							isset( $post_type_setting['num_levels'] )
							&&
							$post_type_setting['num_levels']
						)
						? $post_type_setting['num_levels'] : 2;
						?>
						<input
							name="<?php echo esc_attr( $field ); ?>"
							id="<?php echo esc_attr( $field ); ?>"
							type="number"
							value="<?php echo esc_attr( $value ); ?>">
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
		<?php
	}

	public function validate_settings( $new ) {
		// @TODO: Adds validation/sanitization
		return $new;
	}

	public function validate_approval_matrix( $new ) {
		// @TODO: Adds validation/sanitization
		return $new;
	}

	public function render_page() {
		// Automatically called in options-head.php
		if ( get_current_screen()->parent_base !== 'options-general' ) {
			settings_errors();
		}

		?>
		<div class="wrap">
			<?php screen_icon( 'options-general' ); ?>
			<h2><?php _e( 'Approval Settings', 'hris-approval' ); ?></h2>
			<form method="post" action="options.php">
				<?php settings_fields( self::NAME ); ?>
				<?php do_settings_sections( self::NAME ); ?>
				<?php submit_button( esc_attr__( 'Update Settings', 'hris-approval' ) ); ?>
			</form>
		</div>
		<?php
	}

	public function render_approval_matrix( $args ) {
		global $wp_post_statuses;

		// $setting, $post_type
		extract( $args );

		if ( ! isset( $setting['enabled'] ) )
			return;

		if ( ! $setting['enabled'] )
			return;

		if ( ! isset( $setting['num_levels'] ) )
			return;

		$num_levels = absint( $setting['num_levels'] );
		if ( ! $num_levels )
			return;

		$setting      = $this->_get_setting( $post_type );
		$users        = $this->get_users();
		$field_prefix = sprintf( '%s[%s][approval]', self::NAME, $post_type );

		// Removes unused post statuses
		$excluded_statuses = array( 'private', 'publish', 'future', 'trash', 'auto-draft', 'inherit', 'draft', 'rejected', 'approved' );
		$post_statuses     = array_diff_key( $wp_post_statuses, array_flip( $excluded_statuses ) );
		?>
		<table class="widefat fixed hris-approval-matrix">
			<thead>
				<tr>
					<th class="manage-column"><span class="label"><?php _e( 'Employee', 'hris-approval' ); ?></span></th>
					<?php for ( $i = 1; $i <= $num_levels; $i++ ) : ?>
					<th class="manage-column">
						<span class="label"><?php echo esc_html( sprintf( 'Level %d', $i ) ) ?></span>
						<br>
						<select name="<?php echo esc_attr( sprintf( '%s[%s][status][%d]', self::NAME, $post_type, $i ) ) ?>">
							<option value=""></option>
							<?php foreach ( $post_statuses as $name => $obj ) : ?>
							<option value="<?php echo esc_attr( $name ) ?>" <?php selected( ( isset( $setting['status'][ $i ] ) ? $setting['status'][ $i ] : -1 ), $name )?>><?php echo esc_html( $obj->label ) ?></option>
							<?php endforeach; ?>
						</select>
					</th>
					<?php endfor; ?>
				</tr>
			</thead>
			<tbody>
				<?php $counter = 0; ?>
				<?php foreach ( $users as $user_id => $user_display ) : ?>
				<tr<?php echo ( ++$counter % 2 === 0 ) ? ' class="alternate"' : ''; ?>>
					<td class="user">
					<?php
					$user_obj = new WP_User( $user_id );
					echo get_avatar( $user_id, 32 );
					echo esc_html( $user_display );
					?>
					<span class="username"><?php echo esc_html( $user_obj->user_login ); ?></span>
					<?php if ( ! empty( $user_obj->roles ) && is_array( $user_obj->roles ) ) : ?>
						<br><span class="roles">
						<?php foreach ( $user_obj->roles as $role ) : ?>
							<?php echo esc_html( $role ); ?>
						<?php endforeach; ?>
						</span>
					<?php endif; ?>
					</td>
					<?php for ( $i = 1; $i <= $num_levels; $i++ ) : ?>
					<td>
						<select name="<?php echo esc_attr( sprintf( '%s[%d][%d]', $field_prefix, $user_id, $i ) ) ?>">
						<?php
						$args = array(
							'exclude' => $user_id,
						);
						if ( isset( $setting['approval'][ $user_id ][ $i ] ) && $setting['approval'][ $user_id ][ $i ] ) {
							$args['selected'] = $setting['approval'][ $user_id ][ $i ];
						}
						echo $this->get_users_as_options( $args ); // xss ok
						?>
						</select>
					</td>
					<?php endfor; ?>

				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
		<?php
	}

	/**
	 * Gets registered post types which is public and not a builtin from the core.
	 *
	 * @return array
	 */
	protected function _get_post_types() {
		if ( ! $this->_avail_post_types ) {
			$this->_avail_post_types = get_post_types( array(
				'public'   => true,
				'_builtin' => false,
			), 'objects' );
		}

		return $this->_avail_post_types;
	}

	/**
	 * Gets settings.
	 *
	 * @since 0.1.0
	 * @return array Setting values
	 */
	protected function _get_settings() {
		return get_option( self::NAME, array() );
	}

	/**
	 * Updates settings.
	 *
	 * @since 0.1.0
	 * @param mixed $value Value for the settings.
	 * @return void
	 */
	protected function _update_settings( $value ) {
		update_option( self::NAME, $value );
	}

	/**
	 * Gets a single setting of a given $key. Returns $default if setting
	 * $key doesn't exist.
	 *
	 * @param string $key Should be a string of post_type
	 * @param mixed $default Default value to return if setting key doesn't exist
	 * @return mixed
	 */
	protected function _get_setting( $key, $default = array() ) {
		$settings = $this->_get_settings();
		return isset( $settings[ $key ] ) ? $settings[ $key ] : $default;
	}

	/**
	 * Returns boolean whether a given $post_type is enabled or not.
	 *
	 * @since 0.1.0
	 * @param string $post_type Post type
	 * @return bool True is $post_type is enabled.
	 */
	protected function _is_post_type_enabled( $post_type ) {
		$setting = $this->_get_setting( $post_type );
		if ( ! $setting )
			return false;

		if ( ! isset( $setting['enabled'] ) )
			return false;

		if ( ! $setting['enabled'] )
			return false;

		return true;
	}

	/**
	 * Gets approval-enabled post types.
	 *
	 * @since 0.1.0
	 * @return array Expected format
	 *         ```
	 *         array(
	 *           'post_type' => 'post_type',
	 *           ...
	 *         )
	 *         ```
	 */
	public function get_enabled_post_types() {
		if ( ! empty( $this->_cached_enabled_post_types ) )
			return $this->_cached_enabled_post_types;

		$settings = $this->_get_settings();
		$enabled  = array();
		foreach ( $settings as $post_type => $setting ) {
			if ( isset( $setting['enabled'] ) && $setting['enabled'] ) {
				$enabled[ $post_type ] = $post_type;
			}
		}

		$this->_cached_enabled_post_types = $enabled;
		return $this->_cached_enabled_post_types;
	}

	/**
	 * Get list of users (administrator excluded).
	 *
	 * @return array Expected array structure to return:
	 *         ```
	 *         array(
	 *           'user_id' => 'display_name',
	 *           ...
	 *         )
	 *         ```
	 */
	public function get_users() {
		if ( ! empty( $this->_cached_users ) )
			return $this->_cached_users;

		$users   = get_users();
		$options = array();
		foreach ( $users as $user ) {
			if ( $user->has_cap( 'administrator' ) )
				continue;

			$options[ $user->ID ] = $user->display_name;
		}
		$this->_cached_users = $options;

		return $this->_cached_users;
	}

	/**
	 * Gets list of users as select's options.
	 *
	 * @param array $args Expected arguments:
	 *              ```
	 *              array(
	 *              )
	 * @return string
	 */
	public function get_users_as_options( $args = array() ) {
		$users   = $this->get_users();
		$options = '<option value=""></option>';

		if ( isset( $args['exclude'] ) && ! is_array( $args['exclude'] ) )
			$args['exclude'] = array( $args['exclude'] );
		else
			$args['exclude'] = array();

		foreach ( $users as $id => $name ) {
			if ( in_array( $id, $args['exclude'] ) )
				continue;

			$selected = '';
			if ( isset( $args['selected'] ) && absint( $args['selected'] ) === $id ) {
				$selected = ' selected="selected"';
			}
			$options .= sprintf( '<option value="%d"%s>%s</option>', $id, $selected, $name );
		}

		return $options;
	}

	/**
	 * Returns list of approvers from a given $user_id and $post_type.
	 *
	 * @since 0.1.0
	 * @param string $post_type Post type
	 * @param int $user_id User ID
	 * @return array List of user approvers
	 */
	public function get_user_approvers( $post_type, $user_id ) {
		$cache_available = (
			isset( $this->_cached_user_approvers[ $post_type ][ $user_id ] )
			&&
			$this->_cached_user_approvers[ $post_type ][ $user_id ]
		);
		if ( $cache_available )
			return $this->_cached_user_approvers[ $post_type ][ $user_id ];

		$setting = $this->_get_setting( $post_type );
		if ( ! $setting ) {
			return array();
		}

		if ( ! isset( $setting['approval'][ $user_id ] ) )
			return array();

		if ( ! $setting['approval'][ $user_id ] )
			return array();

		$this->_cached_user_approvers[ $post_type ][ $user_id ] = $setting['approval'][ $user_id ];

		return $this->_cached_user_approvers[ $post_type ][ $user_id ];
	}

	/**
	 * Gets approver in current status.
	 *
	 * @since 0.1.0
	 * @param string $post_type Post type
	 * @param int $user_id User ID
	 * @param string $post_status Post status
	 * @return null|int Null if no approver in current status, otherwise returns the ID of approver
	 */
	public function get_approver_in_current_status( $post_type, $user_id, $post_status ) {
		$approvers = $this->get_user_approvers( $post_type, $user_id );
		$statuses  = $this->get_post_statuses( $post_type );

		// Indices always starts from 1 and step-by 1, same with $approvers.
		$index_of_current_status = 0;
		foreach ( $statuses as $index => $status ) {
			if ( $post_status === $status ) {
				$index_of_current_status = $index;
				break;
			}
		}

		if ( ! $index_of_current_status )
			return null;

		if ( isset( $approvers[ $index_of_current_status ] ) && $approvers[ $index_of_current_status ] )
			return $approvers[ $index_of_current_status ];

		return null;
	}

	/**
	 * Returns true if a given $approver_id is the approver for a given $user_id
	 * in the $post_type with status $post_status. This method can be used to
	 * filter row in post table so that approver is shown only records that he/she
	 * needs to approve (based on approval setting).
	 *
	 * @since 0.1.0
	 * @param string $post_type Post type
	 * @param int $user_id User ID
	 * @param string $post_status Post status
	 * @param int $approver_id Approver ID
	 * @return bool
	 */
	public function is_approver_in_current_status( $post_type, $user_id, $post_status, $approver_id ) {

	}

	/**
	 * Gets post statuses of a $post_type from the approval setting.
	 *
	 * @since 0.1.0
	 * @param string $post_type Post type
	 * @return array
	 */
	public function get_post_statuses( $post_type ) {
		$cache_available = (
			isset( $this->_cached_post_statuses[ $post_type ] )
			&&
			$this->_cached_post_statuses[ $post_type ]
		);

		if ( $cache_available )
			return $this->_cached_post_statuses[ $post_type ];

		$setting = $this->_get_setting( $post_type );
		if ( ! $setting ) {
			return array();
		}

		if ( ! isset( $setting['status'] ) )
			return array();

		$this->_cached_post_statuses[ $post_type ] = $setting['status'];

		return $this->_cached_post_statuses[ $post_type ];
	}

	/**
	 * Gets first post status of a $post_type from the approval setting.
	 *
	 * @since 0.1.0
	 * @param string $post_type Post type
	 * @return string First post status
	 */
	public function get_first_post_status( $post_type ) {
		$statuses = $this->get_post_statuses( $post_type );
		if ( ! $statuses )
			return self::APPROVED;

		return array_shift( $statuses );
	}

	/**
	 * Gets a next post status given a $post_type with current status $post_status.
	 *
	 * @since 0.1.0
	 * @param string $post_type Post type
	 * @param int $user_id
	 * @param string $post_status Post status
	 * @return string Next post status
	 */
	public function get_next_post_status( $post_type, $user_id, $post_status ) {
		$statuses = $this->get_post_statuses( $post_type );

		// Indices always starts from 1 and step-by 1
		$current_status_index = 0;

		if ( $post_status !== 'draft' ) {
			foreach ( $statuses as $index => $status ) {
				$current_status_index = $index;
				if ( $post_status === $status ) {
					break;
				}
			}
		}

		// In most cases this happens between 'draft' to any point before the last
		// post status.
		if ( isset( $statuses[ $current_status_index + 1 ] ) && $statuses[ $current_status_index + 1 ] ) {
			$status = $statuses[ $current_status_index + 1 ];

			// Check approver in this status. If no approver it means the leave gets approved.
			$approver = $this->get_approver_in_current_status( $post_type, $user_id, $status );

			if ( $approver )
				return $status;
		}

		// Last status, means the next is approved.
		return self::APPROVED;
	}

	/**
	 * Gets a previous post status given a $post_type with current status $post_status.
	 *
	 * @since 0.1.0
	 * @param string $post_type Post type
	 * @param int $user_id
	 * @param string $post_status Post status
	 * @return string Previous post status
	 */
	public function get_previous_post_status( $post_type, $user_id, $post_status ) {
		$statuses = $this->get_post_statuses( $post_type );

		// Indices always starts from 1 and step-by 1
		$current_status_index = 0;

		if ( $post_status !== 'draft' ) {
			foreach ( $statuses as $index => $status ) {
				$current_status_index = $index;
				if ( $post_status === $status ) {
					break;
				}
			}
		}

		// In most cases this happens between 'draft' to any point before the last
		// post status.
		if ( isset( $statuses[ $current_status_index - 1 ] ) && $statuses[ $current_status_index - 1 ] ) {
			$status = $statuses[ $current_status_index - 1 ];

			// Check approver in this status. If no approver it means the leave gets approved.
			$approver = $this->get_approver_in_current_status( $post_type, $user_id, $status );

			if ( $approver )
				return $status;
		}

		// Last status, means the next is approved.
		return self::APPROVED;
	}
}
