<?php

class HRIS_Approval_Post_Hooks {

	protected $_enabled_post_types = array();

	public function load() {
		$setting    = $GLOBALS['hris_approval_components']['Setting'];
		$post_types = $setting->get_enabled_post_types();

		// Caches it.
		$this->_enabled_post_types = $post_types;

		foreach ( $post_types as $post_type ) {
			// Customizes columns header.
			add_filter( 'manage_edit-' . $post_type . '_columns', array( $this, 'custom_columns_header' ), 99 );

			// Fills customized columns.
			add_action( 'manage_' . $post_type . '_posts_custom_column', array( $this, 'custom_columns_row' ), 99, 2 );

			// Custom bulk action on table nav.
			add_filter( 'bulk_actions-edit-' . $post_type, array( $this, 'bulk_actions' ) );

		}

		// Enqueues scripts and styles.
		add_action( 'load-edit.php', array( $this, 'scripts_and_styles' ) );

		// Wrap the nice status in column so that it can be colorized with CSS.
		add_filter( 'manage_posts_custom_column_status', array( $this, 'colorize_column_status' ), 99, 2 );
	}

	/**
	 * Customizes columns header of post type edit table.
	 *
	 * @since 0.1.0
	 * @filter manage_edit-{post_type}_columns
	 * @param array $columns Default columns header
	 * @return array Customized columns header
	 */
	public function custom_columns_header( $columns ) {
		// Not hris_requestor. Assuming other roles have more capabilities.
		if ( ! hris_check_user_role( 'hris_requestor' ) ) {
			$columns['actions'] = __( 'Actions', 'hris-approval' );
		}

		return $columns;
	}

	/**
	 * Customizes row of post type edit table.
	 *
	 * @since 0.1.0
	 * @action manage_{post_type}_posts_custom_column
	 * @param string $column Column being passed
	 * @param int $post_id Post ID
	 * @return void
	 */
	public function custom_columns_row( $column, $post_id ) {
		$post_status = get_post_status( $post_id );

		// Not hris_requestor. Assuming other roles have more capabilities.
		$show_actions = (
			! hris_check_user_role( 'hris_requestor' )
			&&
			'actions' === $column
			&&
			HRIS_Approval_Setting::APPROVED !== $post_status
			&&
			HRIS_Approval_Setting::REJECTED !== $post_status
		);
		if ( $show_actions ) {
			$tpl = '<a href="%s" class="button">%s</a>';
			echo sprintf( $tpl, hris_get_approve_post_link( $post_id ), __( 'Approve', 'hris-leave' ) );
			echo '<br>';
			echo sprintf( $tpl, hris_get_reject_post_link( $post_id ), __( 'Reject', 'hris-leave' ) );
		}
	}

	/**
	 * If user role is not adminstrator don't show bulkactions.
	 *
	 * @since 0.1.0
	 * @action bulk_actions-{screen_id}
	 * @param array $actions
	 * @return array Actions
	 */
	public function bulk_actions( $actions ) {
		if ( ! hris_check_user_role( 'administrator' ) )
			return array();

		return $actions;
	}

	/**
	 * Enqueues scripts and styles on edit.php for this post type.
	 *
	 * @since 0.1.0
	 * @action
	 */
	public function scripts_and_styles() {
		if ( ! isset( $_GET['post_type'] ) )
			return;

		if ( ! in_array( $_GET['post_type'], $this->_enabled_post_types ) )
			return;

		wp_enqueue_style( 'hris_approval_edit_table', HRIS_APPROVAL_URI . '/css/hris-approval-edit-table.css' );
	}

	/**
	 * Wrap the column status so that it can be colorized with CSS.
	 *
	 * @since 0.1.0
	 * @param string $nice_status Text status inside the wrapper
	 * @param object $post Post object
	 * @return string
	 */
	public function colorize_column_status( $nice_status, $post ) {
		$status_slug = get_post_status( $post->ID );
		if ( HRIS_Approval_Setting::APPROVED === $status_slug )
			return '<span class="approved">' . $nice_status . '</span>';
		else if ( HRIS_Approval_Setting::REJECTED === $status_slug )
			return '<span class="rejected">' . $nice_status . '</span>';

		return '<span class="pending">' . $nice_status . '</span>';
	}
}
