<?php extract( $GLOBALS['report_data'] ); ?>

<h2>Detail User Purchase Report</h2>
<?php if ( is_a( $query, 'WP_Query' ) && $query->have_posts() ) : ?>
<?php $query->the_post(); ?>	
<header class="report-header">
	<form action="" method="GET">
	<?php $self->the_hidden_fields(); ?>
	<table>
		<tbody>
			<tr>
				<td>Employee Number</td>
				<td>
					<strong><?php echo esc_html( $employee->user_login ); ?></strong>
				</td>
			</tr>
			<tr>
				<td>Employee Name</td>
				<td>
					<strong><?php echo esc_html( $employee->display_name ); ?></strong>
				</td>
			</tr>
			<tr>
				<td>Limit</td>
				<td>
					<strong>
					<?php 
					echo esc_html( number_format( $this->setting , 2, ',', '.' ) ); 
					?>
				</strong>
				</td>
			</tr>
			<tr>
				<td>Total</td>
				<td>
					<strong><?php echo number_format( $GLOBALS['total'], 2, ',', '.' ); ?></strong>
				</td>
			</tr>
			<tr>
				<td>Balance Remaining</td>
				<td>
					<strong><?php echo number_format( $GLOBALS['balance'], 2, ',', '.' ); ?></strong>
				</td>
			</tr>
			<tr>
				<td>Order No</td>
				<td>
					<strong><?php echo esc_html( the_title() ); ?></strong>
				</td>
			</tr>
			<tr>
				<td>Request Date</td>
				<td>
					<strong><?php echo esc_html( date( 'd/m/Y', strtotime($query->post->post_date) ) ); ?></strong>
				</td>
			</tr>
		</tbody>
	</table>
	<form>
</header>
<div class="report-content">
	<table>
		<thead>
			<tr>
				<th rowspan="2">No</th>
				<th rowspan="2">Part No / Description</th>
				<th rowspan="2">Quantity</th>
				<th rowspan="2">Unit Price</th>
				<th rowspan="2">HR Status</th>
				<th rowspan="2">Ctrl Status</th>
			</tr>
			
		</thead>
		<tbody>
			<?php if ( have_rows( 'purchase' ) ) : // ACF repeater ?>
			<?php
			$no             = 1; // Incremental record number to be displayed.
			$total_price = 0; // Total price.
			?>
				<?php while ( have_rows( 'purchase' ) ) : the_row(); ?>
			<?php if (get_sub_field( 'hr_review_status' ) !== 'reject' && 
			get_sub_field( 'ctrl_review_status' ) !== 'reject') : ?>
				<tr>
					<td><?php echo $no++; ?></td>
					<td><?php the_sub_field( 'car_type' ); ?></td>
					<td><?php the_sub_field( 'quantity' ); ?></td>
					<td>
					<?php
					$price_unit        = get_sub_field( 'unit_price_include_vat' );
					$total_price += floatval( $price_unit );

					echo esc_html(  number_format( floatval($price_unit), 2, ',', '.' ) );
					?>
					</td>
					<td><?php the_sub_field( 'hr_review_status' ); ?></td>
					<td><?php the_sub_field( 'ctrl_review_status' ); ?></td>
				</tr>
			<?php endif; ?>							
			<?php endwhile; ?>
		<?php else : ?>
			<tr>
				<td colspan="9">
					<p class="no-records">No records found. Please use filter above.</p>
				</td>
			</tr>
		<?php endif; ?>
		</tbody>
		<tfoot>
			<tr>
				<th colspan="3">
					Total : 
				</th>
				<th>
				<?php echo esc_html(  number_format( $total_price , 2, ',', '.' ) ); ?>	
				</th>
				<th colspan="2">
				&nbsp;	
				</th>
			</tr>	
		</tfoot>
</table>
</div>
<?php endif; ?>
