<!doctype html>
<html>
<head>
	<title>Print Purchase Form</title>
	<style>
	#purchase-print-container {
		display: block;
	}
	#purchase-print-container header li {
		list-style: none;
	}
	#purchase-print-rows table {
		border-spacing: 0;
		border-collapse: collapse;

	}
	#purchase-print-rows table th,
	#purchase-print-rows table td {
		border: 1px solid #333;
		padding: 5px;
	}
	.numeric {
		text-align: right;
	}
	.label-notice {
		width: 100%;
		background-color: #808080;
		margin-top: 30px;
	}


	.table-out {
		margin-left: 40px;
	}

	.border-out {
		margin: 0 auto;
		width:800px;
		border: 1px solid #333;
		padding: 20px;
	}

	.approver{
		display:block;
		margin-top: 90px;
		height:30px;
	}

	.list-approver {
		display: inline-block;
		width:170px;
		
		
	}

	.list-approver span {
		vertical-align: bottom;
	}

	</style>
</head>
<body>
<div class="main">	
<div id="purchase-print-container">
	<div class="border-out">
		<header>
			<ul>
				<?php
				$user      = get_user_by( 'id', $post->post_author );
				$total     = 0;
				$setting   = hris_get_component_from_module( 'approval', 'Setting' );
				$approvers = $setting->get_user_approvers( HRIS_Purchase_Post_Type::NAME, $post->post_author );

				?>
				<li>
					<span>Employee Number: </span>
					<span><?php echo esc_html( $user->user_login ); ?></span></li>
				<li>Name: <?php echo esc_html( $user->display_name ); ?></li>
				<li><?php echo esc_html( get_the_title( $post ) ); ?></li>
				<li>Request date: <?php echo esc_html( get_the_date( 'd/m/Y' ) ); ?></li>

				
				
			</ul>
		</header>
		<div class="table-out">
		<div id="purchase-print-rows">
			<?php if ( have_rows( 'purchase' ) ): ?>
			<table>
				<thead>
					<th>No</th>
					<th>Part No /Description  </th>
					<th>Order Qty</th>
					<th>Employee Price /Unit</th>
					
				</thead>

				<tbody>
				<?php $no = 1; ?>
				<?php while ( have_rows( 'purchase' ) ) : the_row(); ?>
					<tr>
						<td><?php echo $no++; ?></td>
						<td><?php the_sub_field( 'car_type' ); ?></td>
						<td class="numeric">
						<?php
							echo esc_html(  get_sub_field( 'quantity' ) );
						?>
						</td>
						<td class="numeric">
						<?php
						$review = get_sub_field( 'unit_price_include_vat' );
						$total += floatval($review);

						echo esc_html(  number_format( $review, 2, ',', '.' ) );
						?>
						</td>
					</tr>
				<?php endwhile; ?>
				</tbody>

				<tfoot>
					<th class="numeric" colspan="3">Total</th>
					<th class="numeric">
					<?php echo esc_html( number_format( $total, 2, ',', '.' ) ); ?>
					</th>
				</tfoot>

			</table>
		</div>
		<!-- end table out -->
		</div>
		<div class="footer">
		<div class="label-notice">
				<legend>Authorization</legend>
		</div>
		<div>Approved For and on behalf of BMW Indonesia</div>
		<div class="approver">	
			<?php foreach ( $approvers as $key => $approver_id ) : ?>
				<?php $user = get_user_by( 'id', $approver_id ); ?>
				<?php if($key < 3 ) : ?>
					<?php if ( is_a( $user, 'WP_User' ) ) : ?>
					<div class="list-approver">
						<span><?php echo esc_html( $user->display_name ); ?><br></span>
					</div>
					<?php endif; ?>		
				<?php endif; ?>
			<?php endforeach; ?>
			<?php endif; // end have_rows( 'medical_record' ) ?>
			<?php if ($total >= 1000000 && $total < 5000000 ) :?>
			<div class="list-approver">
					<span>Clerence Hua<br></span>
				</div>	
			<?php endif; // end have_rows( 'medical_record' ) ?>
			<?php if ($total >= 5000000 ) :?>
			<div class="list-approver">
					<span>Clarence Chua<br></span>
				</div>
				<div class="list-approver">
					<span>Ramesh Divyanathan<br></span>
				</div>	
			<?php endif; // end have_rows( 'medical_record' ) ?>

			</div>
			<div>This is a computer generated document. No signature is required.</div>
		</div>
	</div>	
	</div>	
	
</div>	
</body>
</html>
