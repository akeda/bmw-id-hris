<?php extract( $GLOBALS['report_data'] ); ?>

<h2>User Purchase Report</h2>
<header class="report-header">
	<form action="" method="GET">
	<?php $self->the_hidden_fields(); ?>
	<table>
		<tbody>
			<tr>
				<td>Date from</td>
				<td>
					<input type="text" id="date_from" name="date_from" class="datepicker" value="<?php echo is_a( $date_from, 'DateTime' ) ? $date_from->format( 'd-m-Y' ) : '' ?>" data-datepicker-args='{"defaultDate": "+1w", "numberOfMonths": 2, "changeMonth": true, "changeYear": true, "from": true, "toRel": "#date_to"}'>
				</td>
			</tr>
			<tr>
				<td>Date to</td>
				<td>
					<input type="text" id="date_to" name="date_to" class="datepicker" value="<?php echo is_a( $date_to, 'DateTime' ) ? $date_to->format( 'd-m-Y' ) : '' ?>" data-datepicker-args='{"defaultDate": "+1w", "numberOfMonths": 2, "changeMonth": true, "changeYear": true, "to": true, "fromRel": "#date_from"}'>
				</td>
			</tr>
			<tr>
				<td>Employee Name</td>
				<td>
					<strong><?php echo esc_html( $employee->display_name ); ?></strong>
				</td>
			</tr>
			<tr>
				<td>Limit</td>
				<td>
					<strong>
					<?php 
					echo esc_html( number_format( $this->setting , 2, ',', '.' ) ); 
					?>
				</strong>
				</td>
			</tr>
			<tr>
				<td>Total</td>
				<td>
					<strong><?php echo number_format( $GLOBALS['total'], 2, ',', '.' ); ?></strong>
				</td>
			</tr>
			<tr>
				<td>Balance Remaining</td>
				<td>
					<strong><?php echo number_format( $GLOBALS['balance'], 2, ',', '.' ); ?></strong>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>
					<input type="submit" class="button" value="Filter">
				</td>
		</tbody>
	</table>
	<form>
</header>
<?php $fields = get_field_objects();
 
//pr($fields);
 
?>
<div class="report-content">
	<table>
		<thead>
			<tr>
				<th>No</th>
				<th>Order NO</th>
				<th>Total</th>				
				<th>Status</th>
			</tr>
			<!--
			<tr>
				<th>Requested</th>
				<th>Approved</th>
				<th>Verification</th>
				<th>HRM</th>
			</tr>
		-->
		</thead>
		<tbody>
			<?php if ( is_a( $query, 'WP_Query' ) && $query->have_posts() ) : ?>
			<?php
			$no             = 1; // Incremental record number to be displayed.
			$total_approved = 0; // Total approved.
			$total_price_incl_vat = 0;
			//pr($query->get_query_var('post_status'));
			?>
			<?php while( $query->have_posts() ) : $query->the_post(); ?>
			<tr>
				<td><?php echo $no++; ?></td>				
				<td class="title"> <a href="
				<?php echo esc_url(admin_url('edit.php?post_type=' . HRIS_Purchase_Post_Type::NAME .
				'&page='. HRIS_Purchase_Report::SLUG .'-'.$this->setting.'&user_id='.$employee->ID. '
				&post_id='.$query->post->ID.'&date_from='.$date_from->format( 'd-m-Y' ).
				'&date_to='.$date_to->format( 'd-m-Y' ) )  ); ?> ">
					<?php echo  the_title();?></a></td>
				<td>
					
					<?php
					$field_tot = get_field('sum_total_price_inc_vat', $query->post->ID, 'double');
					if (empty($field_tot)) {
						$field_tot = 0;
					}
					echo number_format( $field_tot, 2, ',', '.' );
					$total_price_incl_vat += $field_tot;
					?> 
				</td>				
				<td class="status"><?php echo esc_html( strtoupper($query->post->post_status) );?></td>	
			</tr>	
			<?php endwhile; ?>
			</tbody>
			<tfoot>
			<tr>
				<th colspan="2">
					Grand Total
				</th>
				<th>
				<?php echo number_format( $total_price_incl_vat, 2, ',', '.' );	?>
				</th>
				<th></th>	
			</tr>
		</tfoot>
			<?php wp_reset_postdata(); ?>
			<?php else : ?>
		<tbody>	
			<tr>
				<td colspan="5">
					<p class="no-records">No records found. Please use filter above.</p>
				</td>
			</tr>
			<?php endif; ?>	
		</tbody>
	</table>
</div>
