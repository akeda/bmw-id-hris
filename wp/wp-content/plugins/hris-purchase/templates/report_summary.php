<?php
extract( $GLOBALS['report_data'] );

$options = get_option( HRIS_Purchase_Setting::NAME );

$records = array();
if ( is_a( $query, 'WP_Query' ) && $query->have_posts() ) {
	while ( $query->have_posts() ) :
		$query->the_post();

		$author_id = $GLOBALS['post']->post_author;
		if ( ! isset( $records[ $author_id ] ) ) {
			$records[ $author_id ] = array(
				'author_name'     => get_the_author(),
				'total_approved'  => 0,
				'balance' 		  => (is_array($options)&&isset($options['purchase_balance_users'][$author_id]))? 
					$options['purchase_balance_users'][$author_id] : $self->setting,
			);
		}
	while ( have_rows( 'purchase' ) ) : the_row();
			
				$records[ $author_id ][ 'total_approved' ] += (double) get_sub_field( 'total_price_inc_vat' );
			
		endwhile;

	endwhile;

}	
?>
<h2>Purchase Summary Report</h2>

<header class="report-header">
	<form action="" method="GET">
	<?php $self->the_hidden_fields(); ?>
	<table>
		<tbody>
			<tr>
				<td>Date from</td>
				<td>
					<input type="text" id="date_from" name="date_from" class="datepicker" value="<?php echo is_a( $date_from, 'DateTime' ) ? $date_from->format( 'd-m-Y' ) : '' ?>" data-datepicker-args='{"defaultDate": "+1w", "numberOfMonths": 2, "changeMonth": true, "changeYear": true, "from": true, "toRel": "#date_to"}'>
				</td>
			</tr>
			<tr>
				<td>Date to</td>
				<td>
					<input type="text" id="date_to" name="date_to" class="datepicker" value="<?php echo is_a( $date_to, 'DateTime' ) ? $date_to->format( 'd-m-Y' ) : '' ?>" data-datepicker-args='{"defaultDate": "+1w", "numberOfMonths": 2, "changeMonth": true, "changeYear": true, "to": true, "fromRel": "#date_from"}'>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>
					<input type="submit" class="button" value="Filter">
				</td>
		</tbody>
	</table>
	<form>
</header>

<div class="report-content">
	<table>
		<thead>
			<tr>
				<th rowspan="2">No</th>
				<th rowspan="2">Employee</th>
				<th rowspan="2">Balance</th>
				<th rowspan="2">Total </th>
				<th rowspan="2">Remaining Balance</th>				
			</tr>

		</thead>
		<tbody>
		<?php if( count($records) == 0 ): ?>
		<tr>
				<td colspan="9">
					<p class="no-records">No records found. Please use filter above.</p>
				</td>
			</tr>
		<?php else : ?>	
		<?php $no = 1; ?>
		<?php $gtotal = 0; ?>
		<?php foreach ( $records as $user_id => $record ) : ?>
			<tr>
				<td><?php echo $no++; ?></td>
				<td>
					<a href="
				<?php echo esc_url(admin_url('edit.php?post_type=' . HRIS_Purchase_Post_Type::NAME .
				'&page='. HRIS_Purchase_Report::SLUG .'-'.$this->setting.'&user_id='.$user_id. '
				&date_from='.$date_from->format( 'd-m-Y' ).
				'&date_to='.$date_to->format( 'd-m-Y' ) )  ); ?> ">
					<?php echo esc_html( $record['author_name'] ); ?>
					</a>
				</td>
				<td><?php echo esc_html( number_format($record['balance'], 2, ',', '.' ) ); ?></td>
				<td><?php echo esc_html( number_format($record['total_approved'], 2, ',', '.' ) ); ?></td>				
				<td>
				<?php
				$gtotal += $record['total_approved'];
				$total_approved = $record['balance'] - $record['total_approved'];
				echo number_format( $total_approved, 2, ',', '.' );
				?>
				</td>
				
			</tr>
		<?php endforeach; ?>

		<?php endif;?>
		</tbody>
		<?php if(isset($gtotal) ) : ?> 
		<tfoot>
			<tr>
				<th colspan="3">Grand Total</th>
				<th>
					<?php echo esc_html( number_format($gtotal, 2, ',', '.' ) ); ?>
				</th>
				<th >&nbsp;</th>
			</tr>	
		</tfoot>
		<?php endif; ?>	
</table>
</div>
