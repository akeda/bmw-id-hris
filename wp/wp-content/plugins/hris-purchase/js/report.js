(function($) {

	var self = {
		'fireUntilReady': function() {
			$(function(){
				self.init();
			});
		}
	};

	self.init = function() {
		self.bindDatePicker();
	};

	self.bindDatePicker = function() {
		var defaultArgs = {
			dateFormat: 'dd-mm-yy'
		};

		$('.datepicker').each(function() {
			var $el = $(this),
					args = $el.data( 'datepickerArgs' ) || {};

			if ( args.from && args.toRel ) {
				var $to = $(args.toRel);
				args.onClose = function( selectedDate ) {
					$to.datepicker( 'option', 'minDate', selectedDate );

					var fromDate = $el.datepicker( 'getDate' );
					var newDate = new Date( fromDate.getFullYear(), 12, 31 );
					$to.datepicker( 'option', 'maxDate', newDate );

					// Changes the year
					var toDate = $to.datepicker( 'getDate' );
					toDate.setFullYear( fromDate.getFullYear() );
					$to.datepicker( 'setDate', toDate );
				};

				delete args.from;
				delete args.toRel;
			}
			if ( args.to && args.fromRel ) {
				var $from = $(args.fromRel);
				args.onClose = function( selectedDate ) {
					$from.datepicker( 'option', 'maxDate', selectedDate );
				};
				delete args.to;
				delete args.fromRel;
			}
			$.extend( args, defaultArgs );

			$el.datepicker(args);
		});
	};

 self.fireUntilReady();

}(jQuery));
