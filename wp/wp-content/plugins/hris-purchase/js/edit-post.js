(function($){
$(function() {
 
 var template = '<div><input type="submit" class="button primary" value="Submit"></div>';
	$('.acf_postbox .inside').append(template);

	$('#postbox-container-1').remove();
 var __fieldHide = ['efp_euro','lc','xrate','available_stock','stock_price','margin',
 'employee_price','unit_price_include_vat','total_price_inc_vat','attachment','hr_review_status'
 ,'hr_review_reason','ctrl_review_status','ctrl_review_reason']; 

 $.each(__fieldHide,function(x,y){
 	$('.acf-th-'+y).hide();
 	$('[data-field_name="'+y+'"]').hide();
 	//$()
 });

 $('#acf-sum_unit_price_include_vat').hide();
 $('#acf-sum_total_price_inc_vat').hide();
 $('#acf-director_approval').hide();
 $('#acf-director_reason').hide();
 $('#acf-finance_reason').hide();
 $('#acf-finnace_approval').hide();
 
 
 $('[data-field_name="quantity"] input').filter(':visible').css('width',10);
if ( typeof HRIS_PURCHASE_EDIT_POST_JS !== 'undefined') { 

 if (HRIS_PURCHASE_EDIT_POST_JS.pal_manager) {
 	$('#acf-sum_unit_price_include_vat input').attr('readonly','readonly');
 	$('#acf-sum_total_price_inc_vat input').attr('readonly','readonly');
 	$('#acf-attachment').show();
 	$('#acf-sum_unit_price_include_vat').show();
    $('#acf-sum_total_price_inc_vat').show();
    //console.log(HRIS_PURCHASE_EDIT_POST_JS.pal_manager);
 
 	var _doDisplay = ['efp_euro','lc','xrate','available_stock','stock_price','margin',
 	'employee_price','unit_price_include_vat','total_price_inc_vat',];

 	var total = ['sum_unit_price_include_vat', 'sum_total_price_inc_vat','attachment',];
	    
	    
		var field = $('#acf-sum_unit_price_include_vat'),
			balance = $('#acf-sum_unit_price_include_vat'),
			balance_field = $('#acf-sum_unit_price_include_vat input'),
			total = $('#acf-sum_total_price_inc_vat'),
			total_field = $('#acf-sum_total_price_inc_vat input');
			
			console.log(field);
			// Move total field inside field's input wrapper
			
			if ( field.length && balance_field.length ) {
				console.log(balance_field);
				field.append(balance_field);
				field.append(total);

				//$('.acf-input-wrap', '#acf-field-sum_total_price_inc_vat').remove();
				field.css('width', 1030);
				field.css('text-align', 'right');
				total.css('display','inline-block');
				total.css('width',20);
				total.css('vertical-align','bottom');
				total.css('margin-left',15);
				balance_field.css('margin-right', 20);
				total_field.css('margin-right', 10);
				balance_field.attr('readonly','readonly');
				total_field.attr('readonly','readonly');
			}
			
			
	

 	$.each ( _doDisplay , function(i,j) {
 		//console.log($('.acf-th-'+j));
 		$('.acf-th-'+j).show();
 		$('[data-field_name="'+j+'"]').show();
 			
 	});
 	$('[data-field_name="efp_euro"] input').filter(':visible').attr('style','width:80px !important');
 	$('[data-field_name="lc"] input').filter(':visible').attr('style','width:80px !important');
 	$('[data-field_name="xrate"] input').filter(':visible').attr('style','width:80px !important');
 	$('[data-field_name="margin"] input').filter(':visible').attr('style','width:50px !important');
 	$('[data-field_name="available_stock"] input').filter(':visible').attr('style','width:50px !important');
 	$('[data-field_name="stock_price"] input').filter(':visible').attr('style','width:100px !important');
 	$('[data-field_name="quantity"] input').filter(':visible').attr('style','width:50px !important');
 	$('[data-field_name="employee_price"] input').filter(':visible').attr('style','width:100px !important');
 	$('[data-field_name="unit_price_include_vat"] input').filter(':visible').attr('style','width:100px !important');
 	$('[data-field_name="total_price_inc_vat"] input').filter(':visible').attr('style','width:100px !important');
 	$('#acf-sum_unit_price_include_vat input').filter(':visible').attr('style','width:100px !important');
 	$('[data-field_name="margin"] input').filter(':visible').blur(function(){
 		
 		var _margin = $(this).val();
 		if(_margin == 0) {
 			alert("margin Can't be zero");
 			$(_tr).find('[data-field_name="stock_price"] input').focus();
 			return false;
 		}
 		var _tr = $(this).parent().parent().parent().parent();
 		var __availStock = $(_tr).find('[data-field_name="available_stock"] input').val();
 	if (parseInt(__availStock) > 0 ) {
 		var __stockPrice = $(_tr).find('[data-field_name="stock_price"] input').val();
 		//if(__stockPrice <  1) {
 		//	alert("Please fill Stock price field");
 		//	$(_tr).find('[data-field_name="stock_price"] input').focus();
 		//	return false;
 		//}

 		var _employeePrice = (parseFloat(_margin))*(__stockPrice*1);
 		
 		
 	} else {
 		
 		var _employeePrice = calCemptyStock(_tr,_margin); 

 	}
 		$(_tr).find('[data-field_name="employee_price"] input').val( _employeePrice );
 		var unit_price_include_vat = _employeePrice * 1.1;
 		$(_tr).find('[data-field_name="unit_price_include_vat"] input').val( unit_price_include_vat.toFixed(2) );
 		var _quantity =  $(_tr).find('[data-field_name="quantity"] input').val();
 		var _total_price_inc_vat = (_quantity * 1) * unit_price_include_vat ;
 		$(_tr).find('[data-field_name="total_price_inc_vat"] input').val( _total_price_inc_vat.toFixed(2) );
 	//do calculating total price for Rows		
 	sumRow();
 		
 	});

 }	
 if (HRIS_PURCHASE_EDIT_POST_JS.hr_review) {
 	$('#acf-attachment').show();
 	$('#acf-sum_unit_price_include_vat').show();
    $('#acf-sum_total_price_inc_vat').show();
    var _doDisplay = ['hr_review_status','hr_review_reason',]
    var _doReadonly = ['efp_euro','lc','xrate','available_stock','stock_price','margin',
 	'employee_price','unit_price_include_vat','total_price_inc_vat','attachment',];
    $.each ( _doReadonly , function(i,j) {
 		//console.log($('.acf-th-'+j));
 		$('.acf-th-'+j).show();
 		$('[data-field_name="'+j+'"]').show();
 		$('[data-field_name="'+j+'"] input').filter(':visible').attr('readonly','readonly');
 			
 	});
 	$.each ( _doDisplay , function(k,l) {
 		//console.log($('.acf-th-'+j));
 		$('.acf-th-'+l).show();
 		$('[data-field_name="'+l+'"]').show();
 			
 	});

 	adjustDisplay();

 	var field = $('#acf-sum_unit_price_include_vat'),
			balance = $('#acf-sum_unit_price_include_vat'),
			balance_field = $('#acf-sum_unit_price_include_vat input'),
			total = $('#acf-sum_total_price_inc_vat'),
			total_field = $('#acf-sum_total_price_inc_vat input');
			
			console.log(field);
			// Move total field inside field's input wrapper
			
			if ( field.length && balance_field.length ) {
				console.log(balance_field);
				field.append(balance_field);
				field.append(total);

				//$('.acf-input-wrap', '#acf-field-sum_total_price_inc_vat').remove();
				field.css('width', 1030);
				field.css('text-align', 'right');
				total.css('display','inline-block');
				total.css('width',20);
				total.css('vertical-align','bottom');
				balance_field.css('margin-right', 20);
				total_field.css('margin-right', 10);
				balance_field.attr('readonly','readonly');
				total_field.attr('readonly','readonly');
			}
 	

 }

 if (HRIS_PURCHASE_EDIT_POST_JS.supervisor_review) {
 	$('#acf-attachment').show();
 	$('#acf-sum_unit_price_include_vat').show();
    $('#acf-sum_total_price_inc_vat').show();
    var _doDisplay = ['ctrl_review_status','ctrl_review_reason']
    var _doReadonly = ['efp_euro','lc','xrate','available_stock','stock_price','margin',
 	'employee_price','unit_price_include_vat','total_price_inc_vat','attachment',
 	'hr_review_status','hr_review_reason',];
    $.each ( _doReadonly , function(i,j) {
 		//console.log($('.acf-th-'+j));
 		$('.acf-th-'+j).show();
 		$('[data-field_name="'+j+'"]').show();
 		$('[data-field_name="'+j+'"] input').filter(':visible').attr('readonly','readonly');
 		$('[data-field_name="'+j+'"] option').not(':selected').attr('disabled','disabled');
 			
 	});
 	$.each ( _doDisplay , function(k,l) {
 		//console.log($('.acf-th-'+j));
 		$('.acf-th-'+l).show();
 		$('[data-field_name="'+l+'"]').show();
 			
 	});

 	var field = $('#acf-sum_unit_price_include_vat'),
			balance = $('#acf-sum_unit_price_include_vat'),
			balance_field = $('#acf-sum_unit_price_include_vat input'),
			total = $('#acf-sum_total_price_inc_vat'),
			total_field = $('#acf-sum_total_price_inc_vat input');
			
			console.log(field);
			// Move total field inside field's input wrapper
			
			if ( field.length && balance_field.length ) {
				console.log(balance_field);
				field.append(balance_field);
				field.append(total);

				//$('.acf-input-wrap', '#acf-field-sum_total_price_inc_vat').remove();
				field.css('width', 1885);
				field.css('text-align', 'right');
				total.css('display','inline-block');
				total.css('width',20);
				total.css('vertical-align','bottom');
				balance_field.css('margin-right', 20);
				total_field.css('margin-right', 10);
				balance_field.attr('readonly','readonly');
				total_field.attr('readonly','readonly');
			}

 	

 }

 if (HRIS_PURCHASE_EDIT_POST_JS.finance_review) {
 	$('#acf-sum_unit_price_include_vat').hide();
    $('#acf-sum_total_price_inc_vat').hide();
    $('#acf-finnace_approval').show();
    $('#acf-finance_reason').show();
    var __fieldDisplay = ['car_type','quantity','available_stock','unit_price_include_vat',
    'total_price_inc_vat',];
    var iterasi = 0;
    var __textToAppend = "<tfoot><tr>";
    $.each ( __fieldDisplay , function(i,j) {
 		//console.log($('.acf-th-'+j));
 		$('.acf-th-'+j).show();
 		$('[data-field_name="'+j+'"]').show();
 		$('[data-field_name="'+j+'"] input').filter(':visible').attr('readonly','readonly');
 		var iterasi = 0;
 		
 		if(j!=='car_type') {
 			$('[data-field_name="'+j+'"] input').filter(':visible').each(function(o,p){
 				iterasi += $(p).val()*1;
 			});
 			__textToAppend +="<th>";
 			__textToAppend +=iterasi;
 		} else {
 			__textToAppend +="<th colspan='2'>";
 			__textToAppend += "Total";
 		}
 			__textToAppend +="</th>";
 	});
 	__textToAppend +="</tr></tfoot>";

 	$('.acf-input-table').append(__textToAppend);
    $('submit').click(function(){
 		var __appVal = $('acf-field-finnace_approval').val();
 		var __reasVal = $('acf-field-finance_reason').val();
 		if(__appVal=='reject' && __reasVal.length == 0 ) {
 			alert("Must Fiil the reason if your choice to reject");
 			$('acf-field-finance_reason').focus();
 			return false;
 		}  
 	});
 }

 if (HRIS_PURCHASE_EDIT_POST_JS.director_review) {
 	
	$('#acf-sum_unit_price_include_vat').hide();
	$('#acf-sum_total_price_inc_vat').hide();
	$('#acf-director_approval').show();
    $('#acf-director_reason').show();
    var __fieldDisplay = ['car_type','quantity','available_stock','unit_price_include_vat',
    'total_price_inc_vat',];
    var iterasi = 0;
    var __textToAppend = "<tfoot><tr>";
    $.each ( __fieldDisplay , function(i,j) {
 		//console.log($('.acf-th-'+j));
 		$('.acf-th-'+j).show();
 		$('[data-field_name="'+j+'"]').show();
 		$('[data-field_name="'+j+'"] input').filter(':visible').attr('readonly','readonly');
 		var iterasi = 0;
 		
 		if(j!=='car_type') {
 			$('[data-field_name="'+j+'"] input').filter(':visible').each(function(o,p){
 				iterasi += $(p).val()*1;
 			});
 			__textToAppend +="<th>";
 			__textToAppend +=iterasi;
 		} else {
 			__textToAppend +="<th colspan='2'>";
 			__textToAppend += "Total";
 		}
 			__textToAppend +="</th>";
 	});
 	__textToAppend +="</tr></tfoot>";

 	$('.acf-input-table').append(__textToAppend);
	$('submit').click(function(){
	 		var __appVal = $('acf-field-director_approval').val();
	 		var __reasVal = $('acf-field-director_reason').val();
	 		if(__appVal=='reject' && __reasVal.length == 0 ) {
	 			alert("Must Fiil the reason if your choice to reject");
	 			$('acf-field-director_reason').focus();
	 			return false;
	 		}  
	 });
 } 
 // if edit this post and user login is not author 
 if (HRIS_PURCHASE_EDIT_POST_JS.login_not_author) {
 	$('.remove').hide();
	$('.acf-button').hide();
	$('.acf-button-add').hide();
	$('.acf-button-remove').hide();
	$('.label').css('width',0);
	$('[data-field_name="car_type"] input').filter(':visible').attr('readonly','readonly');
	$('[data-field_name="quantity"] input').filter(':visible').attr('readonly','readonly');
	$('[data-field_name="employee_price"] input').filter(':visible').attr('readonly','readonly');
	$('[data-field_name="unit_price_include_vat"] input').filter(':visible').attr('readonly','readonly');
	$('[data-field_name="total_price_inc_vat"] input').filter(':visible').attr('readonly','readonly');
	//console.log($('[data-field_name="employee_price"] input').filter(':visible'));
 }

} else {
	
	$(document).on('click', '.primary', function() {

		var eachEl = $('[data-field_name="quantity"] input').filter(':visible');
		var x = 0;		
		$.each( eachEl, function(m, n) {
			if($(n).val().length < 1 || $(n).val() == 0 ) {
				x++;
				
			}
		} );
		if (x > 0 ) {
			alert("Field Quantity Can't Empty or must greather than zero !!");
			return false;
		}
	});
}

function adjustDisplay() {
	$('[data-field_name="efp_euro"] input').filter(':visible').attr('style','width:80px !important');
 	$('[data-field_name="lc"] input').filter(':visible').attr('style','width:80px !important');
 	$('[data-field_name="xrate"] input').filter(':visible').attr('style','width:80px !important');
 	$('[data-field_name="margin"] input').filter(':visible').attr('style','width:50px !important');
 	$('[data-field_name="available_stock"] input').filter(':visible').attr('style','width:50px !important');
 	//$('[data-field_name="stock_price"] input').filter(':visible').attr('style','width:100px !important');
 	$('[data-field_name="quantity"] input').filter(':visible').attr('style','width:50px !important');
 	//$('[data-field_name="employee_price"] input').filter(':visible').attr('style','width:100px !important');
 	//$('[data-field_name="unit_price_include_vat"] input').filter(':visible').attr('style','width:100px !important');
 	//$('[data-field_name="total_price_inc_vat"] input').filter(':visible').attr('style','width:100px !important');
 	$('#acf-sum_unit_price_include_vat input').filter(':visible').attr('style','width:100px !important');
 	var __fieldModif = ['quantity','efp_euro','lc','xrate','available_stock','stock_price',
 						'margin','employee_price','unit_price_include_vat',
 						'total_price_inc_vat','attachment','hr_review_status' ,
 						'hr_review_reason','ctrl_review_status','ctrl_review_reason'];
 	$.each (__fieldModif,function(q,z){
 		if(z.match(/price/g) ) {
 			$('[data-field_name="'+z+'"] input').filter(':visible').attr('style','width:100px !important');
 		} else if (z.match(/review_status/g)){
 			$('[data-field_name="'+z+'"] select').filter(':visible').attr('style','width:50px !important');
 		}
 	});					

}//end function adjustDisplay

//for calculating sum of row unit_price_inc_vat and total_price_inc_vat
function sumRow(){
		var sum_unit_price = 0;
 		var _sum_unit_price = $('[data-field_name="unit_price_include_vat"] input').filter(':visible');
 		$.each(_sum_unit_price,function(x,y){
 			sum_unit_price += ($(y).val() * 1 )
 		});
 		var sum_total_unit_price = 0;
 		var _sum_total_unit_price = $('[data-field_name="total_price_inc_vat"] input').filter(':visible');
 		$.each(_sum_total_unit_price,function(i,j){
 			sum_total_unit_price += ($(j).val() * 1 );
 		});

 		console.log(sum_unit_price+"****"+sum_total_unit_price);
 		$('#acf-sum_unit_price_include_vat input').val(sum_unit_price.toFixed(2));

 		$('#acf-sum_total_price_inc_vat input').val(sum_total_unit_price.toFixed(2));
 		//$('#acf-sum_unit_price_include_vat input').bindMoneyFormatting();
}

function calCemptyStock(el,_margin) {
		if(typeof(el)=='nan') {
			console.log('transfer element paramater failed');
			return false;
		}
		var efp_euro 	= $(el).find('[data-field_name="efp_euro"] input').val();
 		var lc			= $(el).find('[data-field_name="lc"] input').val();
 		var xrate		= $(el).find('[data-field_name="xrate"] input').val();
 		if (efp_euro==0 || lc == 0 || xrate ==0) {
 			alert("Please efp_euro, lc, xrate field must greather than 0 !!");
 			$(efp_euro).focus();
 			return false;
 		}

 		return (parseFloat(_margin))*(efp_euro*1)*(lc*1)*(xrate*1);
}

});    
}(jQuery));