(function($){

$(function() {
	// Hacky.. hacky way to removes the feature

	$('.row-actions').remove();
	if ( HRIS_PURCHASE_EDIT_TABLE.remove_title_anchor ) {
		$('.row-title').each(function() {
			var txt = $(this).text();

			$(this).parent().parent().html(txt);
		});
	}

	if ( HRIS_PURCHASE_EDIT_TABLE.remove_checkbox ) {
		$('.column-cb, .check-column').remove();
		$('.tablenav .bulkactions').remove();
	}
});

}(jQuery));