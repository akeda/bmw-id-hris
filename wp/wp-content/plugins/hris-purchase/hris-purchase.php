<?php
/**
 * Plugin Name: HRIS Purchase
 * Plugin URI: http://gedex.web.id
 * Description: Plugin that adds employee purchase request management for HRIS.
 * Version: 0.1.0
 * Author: Fandy Fardian
 * Author URI: http://gedex.web.id
 * License: MIT
 *
 * Plugin that adds employee purchase request management for HRIS
 *
 * @package HRIS Purchase
 * @version 0.1.0
 * @author Fandy Fardian <shifty.power@gmail.com>
 * @license MIT License
 */

/**
 * Class that bootstrap this plugin.
 *
 * @since 0.1.0
 */
class HRIS_Purchase {

	/**
	 * Components to load.
	 *
	 * @since 0.1.0
	 * @var array
	 * @access protected
	 */
	protected $_components_to_load;

	public function __construct() {
		// Initialization.
		$this->is_scripts_and_styles_enqueued = false;
		$this->_components_to_load            = array(
			'Post_Type',
			//'Type_Taxonomy_Meta_Fields',
			'Edit_Table',
			'Edit_Post',
			'Layout_Setting',
			'Setting',
			'Report',
			'Approval',
			//'Email_Notification',
		);

		// Autoloader registration
		spl_autoload_register( array( $this, 'autoload' ) );

		// Set the constants needed by the plugin.
		add_action( 'plugins_loaded', array( $this, 'define_constants' ), 1 );

		// Load components.
		add_action( 'plugins_loaded', array( $this, 'load_components' ), 2 );
	}

	/**
	 * Autoloader for this plugin.
	 *
	 * @since 0.1.0
	 */
	public function autoload( $class ) {
		if ( false === strpos( $class, __CLASS__ ) )
			return;

		$class = str_replace( array( '_', 'hris-purchase-' ), array( '-', '' ), strtolower( $class ) ) . '.php';

		require_once HRIS_PURCHASE_INCLUDES . $class;
	}

	/**
	 * Defines constants used by the plugin.
	 *
	 * @since 0.1.0
	 * @action plugins_loaded
	 */
	public function define_constants() {
		// Set the version number of the plugin.
		define( 'HRIS_PURCHASE_VERSION', '0.1.0' );

		// Set constant path to this plugin directory.
		define( 'HRIS_PURCHASE_DIR', trailingslashit( plugin_dir_path( __FILE__ ) ) );

		// Set constant path to this plugin URL.
		define( 'HRIS_PURCHASE_URI', trailingslashit( plugin_dir_url( __FILE__ ) ) );

		// Set the constant path to this includes directory.
		define( 'HRIS_PURCHASE_INCLUDES', HRIS_PURCHASE_DIR . trailingslashit( 'includes' ) );

		// Set the constant path to this templates directory.
		define( 'HRIS_PURCHASE_TEMPLATES', HRIS_PURCHASE_DIR . trailingslashit( 'templates' ) );
	}

	/**
	 * Load components.
	 *
	 * @since 0.1.0
	 * @action plugins_loaded
	 */
	public function load_components() {
		$GLOBALS['hris_purchase_components'] = array();

		foreach ( $this->_components_to_load as $component ) {
			$class = 'HRIS_Purchase_' . $component;
			$GLOBALS['hris_purchase_components'][ $component ] = new $class;
			$GLOBALS['hris_purchase_components'][ $component ]->load();
		}
	}
}

$GLOBALS['hris_purchase'] = new HRIS_Purchase();
