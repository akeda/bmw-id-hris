<?php

class HRIS_Purchase_Post_Type implements HRIS_Purchase_Component_Interface {

	const NAME = 'hris_purchase_record';

	public function load() {
		// Register custom post type on the 'init' hook.
		add_action( 'init', array( $this, 'register_post_type' ) );
		//add_action( HRIS_Purchase_Setting_Limit::NAME , array( $this, 'on_add_new' ) );
		// When saving_post, do the followings:
		// - Generate unique title that serves as purchase code.
		add_action( 'save_post', array( $this, 'save_post' ) );

		//add_action( 'add_meta_boxes_' . self::NAME, array( $this, 'remove_meta_boxes' ) );
	}

	/**
	 * Registers post type needed by this plugin.
	 *
	 * @since 0.1.0
	 * @access public
	 * @return void
	 */
	public function register_post_type() {
		// Set up the arguments for the portfolio item post type.
		$args = array(
			'description'         => '',
			'public'              => true,
			'publicly_queryable'  => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'exclude_from_search' => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'menu_position'       => 5,
			// 'menu_icon'           => HRIS_LEAVE_URI . 'images/menu-icon.png',
			'can_export'          => true,
			'delete_with_user'    => true,
			'hierarchical'        => false,
			'has_archive'         => false,
			'query_var'           => false,
			'capability_type'     => self::NAME,
			'map_meta_cap'        => true,

			'capabilities' => array(

				// meta caps (don't assign these to roles)
				'edit_post'              => 'edit_'   . self::NAME,
				'read_post'              => 'read_'   . self::NAME,
				'delete_post'            => 'delete_' . self::NAME,

				// primitive/meta caps
				'create_posts'           => 'create_hris_purchase_records',

				// primitive caps used outside of map_meta_cap()
				'edit_posts'             => 'edit_hris_purchase_records',
				'edit_others_posts'      => 'approve_hris_purchase_records',
				'publish_posts'          => 'approve_hris_purchase_records',
				'read_private_posts'     => 'read',

				// primitive caps used inside of map_meta_cap()
				'read'                   => 'read',
				'delete_posts'           => 'manage_hris_purchase',
				'delete_private_posts'   => 'manage_hris_purchase',
				'delete_published_posts' => 'manage_hris_purchase',
				'delete_others_posts'    => 'manage_hris_purchase',
				'edit_private_posts'     => 'approve_hris_purchase_records',
				'edit_published_posts'   => 'approve_hris_purchase_records'
			),

			'rewrite' => false,

			// What features the post type supports.
			'supports' => array(
				'author',
			),

			// Labels used when displaying the posts.
			'labels' => array(
				'name'               => __( 'Purchase',                 'hris-purchase' ),
				'singular_name'      => __( 'Purchase',                 'hris-purchase' ),
				'menu_name'          => __( 'Purchase',                 'hris-purchase' ),
				'name_admin_bar'     => __( 'Purchase',                 'hris-purchase' ),
				'add_new'            => __( 'Add New',                 'hris-purchase' ),
				'add_new_item'       => __( 'Add New Purchase',         'hris-purchase' ),
				'edit_item'          => __( 'Edit purchase',            'hris-purchase' ),
				'new_item'           => __( 'New Purchase',             'hris-purchase' ),
				'view_item'          => __( 'View Purchase',            'hris-purchase' ),
				'search_items'       => __( 'Search Purchase',          'hris-purchase' ),
				'not_found'          => __( 'No purchase found',          'hris-purchase' ),
				'not_found_in_trash' => __( 'No purchaseS found in trash', 'hris-purchase' ),
				'all_items'          => __( 'Purchase',                 'hris-purchase' ),
			)
		);
		
		// Register the post type.
		register_post_type( 'hris_purchase_record', $args );
	}

	public function save_post( $post_id ) {
		if ( ! isset( $_POST['post_type'] ) ) {
			//planing is too hook into limit setting to get balance
			//print_r($this->_get_setting( 'purchase_limit_users' ));
			return;
		}

		// If this isn't 'hris_purchase_record', don't update it.
		if ( self::NAME !== $_POST['post_type'] ) {
			return;
		}

		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
			return;

		// If this is a revision, get real post ID.
		if ( $parent_id = wp_is_post_revision( $post_id ) ) {
			$post_id = $parent_id;
		}

		

		// Unhook this method so it doesn't loop infinitely.
		remove_action( 'save_post', array( $this, 'save_post' ) );

		$post_args = array(
			'ID' => $post_id,
		);
		// Check if post_title is empty
		if ( empty( $_POST['post_title'] ) ) {
			
			$code = $this->_generate_code( $post_id );
			$post_args['post_title'] = $code;
		}

		$old_status = get_post_status( $post_id );

		// Gets post_status to update;
		$post_args['post_status'] = $this->_get_post_status_to_update( $post_id );
		
		$new_status = $post_args['post_status'];

			// Updates the post status and/or title
		wp_update_post( $post_args );

			// Gets post object
		$post_obj = get_post( $post_id );

		// Updates approver in post meta based on next status.
		$approver = hris_get_approver_by_status( self::NAME, $post_obj->post_author, $post_args['post_status'] );
		
		
		if ( $approver ) {
			update_post_meta( $post_id, 'approver', $approver );
		} else {
			delete_post_meta( $post_id, 'approver' );
		}
		do_action( 'updated_approver', $approver, $old_status, $new_status, $post_id );


			// Re-hook this method.
			add_action( 'save_post', array( $this, 'save_post' ) );
		
	}

	protected function _generate_code( $post_id ) {
		$current_date = date( 'Ymd', current_time( 'timestamp', 0 ) );

		return 'SP' . $current_date . str_pad( $post_id, 10, '0', STR_PAD_LEFT );
	}

	protected function _get_post_status_to_update( $post_id ) {
		$post_obj    = get_post( $post_id );
		$post_type   = get_post_type( $post_id );
		$post_status = get_post_status( $post_id );
		$next_status = 'draft';
		$field = get_fields($post_id);
		
		$first_status = 0;
		$supervisor_reject = 0;
		$ctrl_reject = 0;
		$unit_price_incl = 0; 
		
		foreach ($field['purchase'] as $key => $value) {
			# code...
			if( empty($value['quantity'])) {
				$first_status++;
			}

			if ( empty($value['unit_price_include_vat'])){
				//pal-manager-stats cek
				$unit_price_incl++;
			}
			if($value['hr_review_status'] == 'reject') {
				$hr_reject++;
			}

			if($value['ctrl_review_status'] == 'reject') {
				$ctrl_reject++;
			}
		}
		
		//if($first_status === 0 && $unit_price_incl > 0 ) {
			//pr("di sini ");die;
		if ($post_status =='draft') {
			$next_status = hris_get_next_post_status( $post_type, $post_obj->post_author, $post_status );	
		} else if ( $post_status =='pal-manager-review' ) {
			
			if($field['sum_unit_price_include_vat'] > 0 ) {
				$next_status = hris_get_next_post_status( $post_type, $post_obj->post_author, $post_status );
			}else {
				$next_status = $post_status;
				//$next_status = HRIS_Approval_Setting::REJECTED;
			}
		} else if ($post_status=='hr-review') {
			/* waitng cnfirmation from mr big bos */
			if( $hr_reject > 0 ) {
				//wp_update();
				$next_status = HRIS_Approval_Setting::REJECTED;
			} else {
				$next_status = hris_get_next_post_status( $post_type, $post_obj->post_author, $post_status );
			}
		}  else if ($post_status=='supervisor-review') {
			
			if( $ctrl_reject > 0 ) {
				//next will be step back twice using function
				$next_status = 'pal-manager-review';	
			} else {
				
				if (intval($field['sum_total_price_inc_vat']) > 1000000 ) {				
					
					$next_status = hris_get_next_post_status( $post_type, $post_obj->post_author, $post_status );
				} else {
					$next_status = HRIS_Approval_Setting::APPROVED;
				}
			}
		} else if ($post_status=='finance-review') {
			
			if($field['finnace_approval'] == 'approve') {
				
				if ( intval($field['sum_total_price_inc_vat']) > 5000000 ) {
					$next_status = hris_get_next_post_status( $post_type, $post_obj->post_author, $post_status );					
				}else {
					$next_status = HRIS_Approval_Setting::APPROVED;
				}

			} else {
				$next_status = HRIS_Approval_Setting::REJECTED;
			}

		} else if ($post_status=='director-review') {
			if ($field['finnace_approval'] == 'approve') {
				$next_status = hris_get_next_post_status( $post_type, $post_obj->post_author, $post_status );
			} else {
				$next_status = HRIS_Approval_Setting::REJECTED;
			}

		} else {
			$next_status = $post_status;
		}

		// Get the next post status and updates it.
		//$next_status = hris_get_next_post_status( $post_type, $post_obj->post_author, $post_status );
		if ( ! $next_status )
			$next_status = 'draft';

		return $next_status;
	}

	public function remove_meta_boxes() {
		global $post_type;

		if ( HRIS_Purchase_Post_Type::NAME !== $post_type )
			return;

		$removed_meta_boxes = array(
			HRIS_Purchase_Taxanomy_Type::NAME . 'div' => array(
				'screen'  => self::NAME,
				'context' => 'side',
			),
			'submitdiv' => array(
				'screen'  => self::NAME,
				'context' => 'side',
			),
		);

		foreach ( $removed_meta_boxes as $id => $param ) {
			if ( ! isset( $param['screen'] ) || ! isset( $param['context'] ) ) {
				continue;
			}

			if ( is_array( $param['context'] ) ) {
				foreach ( $param['context'] as $context ) {
					remove_meta_box( $id, $param['screen'], $context );
				}
			} else {
				remove_meta_box( $id, $param['screen'], $param['context'] );
			}
		}
	}

	public function on_add_new($params) {
		//print_r($params);
	}
}
