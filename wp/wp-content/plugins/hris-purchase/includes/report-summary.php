<?php

class HRIS_Purchase_Report_Summary {
	/**
	 * @var object
	 */
	private $user;

	public function render() {
		
		if ( ! $this->setting ) {
			wp_die( __( 'Setting MUST BE SET before render with <code>set_setting( $setting )</code> method', 'hris-medical' ) );
			exit();
		}

		
		$data = $this->get_data();
		$this->user = wp_get_current_user();
		$GLOBALS['report_data'] = $data;
		$GLOBALS['total'] = $this->getCount($data);	
		$GLOBALS['balance'] = (int)$this->setting - (int)$GLOBALS['total'];
		

		$template = HRIS_PURCHASE_TEMPLATES . 'report_summary.php';

		include $template;
		
	}

	public function get_data() {
		$employee = wp_get_current_user();
		if ( isset( $_REQUEST['user_id'] ) && ($user = get_user_by( 'id', $_REQUEST['user_id'] )) ) {
			$employee = $user;
		}

		$query     = null;
		$date_from = '';
		$date_to   = '';

		// Check requested date to fill the query.
		if ( isset( $_REQUEST['date_from'] ) && isset( $_REQUEST['date_to'] ) ) {
			$date_from = new DateTime( $_REQUEST['date_from'] );
			$date_to   = new DateTime( $_REQUEST['date_to'] );

			// Makes sure $date_from and $date_to within the same year
			// and $date_from is less than $date_to. If no $date_to will
			// be set to $date_from.
			if ( ( $date_from->format('Y') !== $date_to->format('Y') ) ||
			       $date_from > $date_to )
			{
				$date_to->setDate(
					intval( $date_from->format('Y') ),
					intval( $date_from->format('m') ),
					intval( $date_from->format('d') )
				);
			}

			$query = new WP_Query( array(
				'post_type'  => HRIS_Purchase_Post_Type::NAME,
				'date_query' => array(
					'after' => array(
						'year'  => $date_from->format('Y'),
						'month' => $date_from->format('n'),
						'day'   => $date_from->format('j'),
					),
					'before' => array(
						'year'  => $date_to->format('Y'),
						'month' => $date_to->format('n'),
						'day'   => $date_to->format('j'),
					),
					'inclusive' => true,
				),
				
			) );
		}
		
		$user = $this->user;
		$self = $this;

		return compact(  'query', 'user', 'self', 'date_from', 'date_to' );
	}

	public function get_hidden_fields() {
		return str_replace(
			array(
				'{post_type}',
				'{page}',
				//'{user_id}'
			),
			array(
				esc_attr( HRIS_Purchase_Post_Type::NAME ),
				esc_attr( HRIS_Purchase_Report::SLUG ),
				//isset( $_REQUEST['user_id'] ) ? esc_attr( $_REQUEST['user_id'] ) : $this->user->ID,
			),
			'
			<input type="hidden" name="post_type" value="{post_type}">
			<input type="hidden" name="page" value="{page}">
						'
		);
	}

	public function the_hidden_fields() {
		echo $this->get_hidden_fields();
	}

	public function getCount($data) {
		$count = 0;
		$val = array();
		extract($data);

		if ( is_a( $query, 'WP_Query' ) && $query->have_posts() ) {
			while( $query->have_posts() ) { 
				$query->the_post();  
				$val = get_field_object( 'sum_unit_price_include_vat' );
				$count += $val['value'];	
				
			}
		}
		return $count;
	}

	public function set_setting( $setting ) {
   		$this->setting = $setting;
    }

    public function getDetail( $post_id ) {
    	$employee = wp_get_current_user();
		if ( isset( $_REQUEST['user_id'] ) && ($user = get_user_by( 'id', $_REQUEST['user_id'] )) ) {
			$employee = $user;
		}
		$query = new WP_Query( array(
				'post_type'  				=> HRIS_Purchase_Post_Type::NAME,
				'author'     				=> $employee->ID,
				HRIS_Purchase_Setting::NAME => $this->setting,
				'p'							=> $post_id,
				)
		);

		$user = $this->user;
		$self = $this;

		return compact( 'employee', 'query', 'user', 'self' );

    }

}