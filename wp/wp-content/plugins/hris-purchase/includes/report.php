<?php

class HRIS_Purchase_Report implements HRIS_Purchase_Component_Interface {
	const SLUG = 'hris-purchase-report';

	/**
	 * Used to cache terms in reporting.
	 *
	 * @var array
	 */
	protected $setting;

	/**
	 * Setting options.
	 *
	 * @since 0.1.0
	 * @var array
	 */
	private $options;

	public function __construct() {
		$this->options = array(
			'fields'        => array(
				'default_annual_purchase_balance' => array(
					'title'   => __( 'Default annual purchase balance', 'hris-purchase' ),
					'type'    => 'number',
					'default' => 5500000,
				),
				'purchase_balance_users' => array(
					'title'   => __( 'Purchase balance per employee', 'hris-purchase' ),
					'type'    => 'purchase_balance_users_matrix',
				),
			),
		);

	}

	public function load() {
		add_action( 'admin_menu', array( $this, 'register_report_menu' ) );

		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_statics' ) );
	}

	public function register_report_menu() {


		$user = wp_get_current_user();
		$setting = $this->get_options('purchase_balance_users',$user->ID);
		// Summary only for hris-manager and administrator.
		if ( hris_check_user_role( 'hris_manager' ) || hris_check_user_role( 'administrator' ) ) {
			require_once HRIS_PURCHASE_INCLUDES . 'report-summary.php';
			$report_summary = new HRIS_Purchase_Report_Summary();
			$report_summary->set_setting( $setting );

			add_submenu_page(
				'edit.php?post_type=' . HRIS_Purchase_Post_Type::NAME,
				__( 'Report Summary', 'hris-purchase' ),
				__( 'Report Summary', 'hris-purchase' ),
				'edit_hris_purchase_records',
				self::SLUG,
				array( $report_summary, 'render' )
			);

		}
			
			
			$report_by_user = new HRIS_Purchase_Report_By_User();
			$report_by_user->set_setting( $setting );
			add_submenu_page(
				'edit.php?post_type=' . HRIS_Purchase_Post_Type::NAME,
				sprintf( __( 'Report %s', 'hris-purchase' ), $user->display_name ),
				sprintf( __( 'Report %s', 'hris-purchase' ), $user->display_name ),
				'edit_hris_purchase_records',
				self::SLUG . '-' .$setting ,
				array( $report_by_user, 'render' )
			);


		//$terms = $this->get_terms();

		// Child reports based on HRIS_Purchase_Type_Taxonomy.
		//require_once HRIS_PURCHASE_INCLUDES . 'report-by-term.php';
		/*
		foreach ( $terms as $term ) {
			$report_by_type = new HRIS_Purchase_Report_By_Term();
			$report_by_type->set_term( $term );

			add_submenu_page(
				'edit.php?post_type=' . HRIS_Purchase_Post_Type::NAME,
				sprintf( __( 'Report %s', 'hris-purchase' ), $term->name ),
				sprintf( __( 'Report %s', 'hris-purchase' ), $term->name ),
				'edit_hris_purchase_records',
				self::SLUG . '-' . $term->slug,
				array( $report_by_type, 'render' )
			);
		}
		*/
	}

	public function enqueue_statics( $hook ) {
		$expected_hooks = array(
			HRIS_Purchase_Post_Type::NAME . '_page_' . self::SLUG,
		);
/*
		$terms = $this->get_terms();
		foreach ( $terms as $term ) {
			$expected_hooks[] = HRIS_Purchase_Post_Type::NAME . '_page_' . self::SLUG . '-' . $term->slug;
		}

		if ( ! in_array( $hook, $expected_hooks ) ) {
			return;
		}
*/
		wp_enqueue_style( 'hris_purchase_report', HRIS_PURCHASE_URI . '/css/report.css' );
		wp_enqueue_style( 'hris-jquery-ui-theme' );
		wp_enqueue_style( 'hris-common' );

		wp_enqueue_script( 'hris_purchase_report', HRIS_PURCHASE_URI . '/js/report.js', array( 'jquery', 'jquery-ui-datepicker' ) );
	}
	/*
	protected function get_terms() {
		if ( ! $this->terms ) {
			$this->terms = get_terms( HRIS_Purchase_Type_Taxonomy::NAME, array(
				'parent'     => 0,
				'hide_empty' => false,
			) );

			if ( ! $this->terms || is_wp_error( $this->terms ) ) {
				$this->terms = array();
			}
		}

		return $this->terms;
	}
	*/

	private function get_options($key, $user_id) {
		if(!$user_id) {
			return $this->_get_setting( $key );	
		} else {
			return $this->_get_setting( $key, $user_id );	
		}
		
	}

	protected function _get_setting( $key, $user_id=null ) {
		$settings = $this->_get_settings();
		
		if ( is_null($user_id) || !isset( $settings[ $key ][$user_id] ) ) {
			return $settings['default_annual_purchase_balance'];
		} else {
			return $settings[ $key ][$user_id];	
		}	 
	}



	/**
	 * Gets settings.
	 *
	 * @since 0.1.0
	 * @return array Setting values
	 */
	protected function _get_settings() {
		$defaults = array_combine(
			array_keys( $this->options['fields'] ),
			array_map( function( $arr ) { return isset( $arr['default'] ) ? $arr['default'] : ''; }, $this->options['fields'] )
		);
		return array_merge( $defaults, get_option( HRIS_Purchase_Setting::NAME, array() ) );
	}
}
