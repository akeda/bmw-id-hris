<?php

/**
 * Interface that plugin-component's class MUST implement.
 *
 * @package HRIS Purchase
 * @since 0.1.0
 * @author Fandy Fardian <shifty.power@gmail.com>
 */
interface HRIS_Purchase_Component_Interface {
	public function load();
}
