<?php
/**
 * Set layout for add, edit or approve and spouse fields on post-new or edit screen.
 *
 * @package HRIS Purchase
 * @since 0.1.0
 * @author Fandy Fardian <shifty.power@gmail.com>
 */
class HRIS_Purchase_Layout_Setting implements HRIS_Purchase_Component_Interface {

	/**
	 * Called by plugin's main-file in plugins_load action.
	 *
	 * @since 0.1.0
	 * @return void
	 */
	public function load() {
		//add_filter('acf/load_field', array( $this, 'lay_acf_load_field'));
		add_action('admin_enqueue_scripts', array( $this, 'scripts_and_styles' ) );
	}

	/**
	 * Set display field is display on post-new screen or in edit and approve.
	 *
	 * @action acf/load_value
	 * @param string $value
	 * @param int $post_id
	 * @param array $field
	 * @return string
	 */

	public function lay_acf_load_field ( $field ) {
		return $field;
	}

	public function scripts_and_styles () {
		if ( ! isset( $_GET['post_type'] ) && ! isset( $_GET['post'] ) )
			return;

		if ( isset( $_GET['post_type'] ) && HRIS_Purchase_Post_Type::NAME !== $_GET['post_type'] )
			return;

		if ( isset( $_GET['post'] ) ) {
			$post = get_post( $_GET['post'] );
			if ( ! $post )
				return;

			if ( HRIS_Purchase_Post_Type::NAME !== $post->post_type )
				return;
		}
		$rows = get_field_objects( 'purchase' );
		
		wp_enqueue_script( 'hris-purchase-layout', HRIS_PURCHASE_URI . '/js/layout-setting.js', array( 'jquery', ), HRIS_MEDICAL_VERSION );

		

	}


	private function _is_edit_page($new_edit = null){
	    global $pagenow;
	    //make sure we are on the backend
	    if (!is_admin()) return false;


	    if($new_edit == "edit")
	        return in_array( $pagenow, array( 'post.php',  ) );
	    else if($new_edit == "new") //check for new post page
	        return in_array( $pagenow, array( 'post-new.php' ) );
	    else //check for either new or edit
	        return in_array( $pagenow, array( 'post.php', 'post-new.php' ) );
	}

}