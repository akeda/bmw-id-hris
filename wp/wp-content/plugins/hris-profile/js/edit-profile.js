(function($) {

$(function() {

	$('#hris-profile').tabs();

	$('#hris-profile-family .add-children').on('click', function(e) {
		e.preventDefault();

		var first_el = $('#hris-profile-family ul.children li:last');
		var template = first_el.clone();

		$('input, select', template).each(function(){
			var el = $(this),
					start = el.attr('name').indexOf('['),
					end = el.attr('name').indexOf(']'),
					index = el.attr('name').substr( start+1, (end-start)-1 ),
					new_index = parseInt(index,10) + 1,
					new_name = el.attr('name').replace('['+index+']', '['+new_index+']');

			el.attr('name', new_name);
			el.attr('id', new_name);
		});
		$('input', template).val('');

		template.css('margin-top', '10px');
		template.css('padding-top', '10px');
		template.css('border-top', '1px solid #ccc');
		template.append('<br><a href="#" class="remove">Remove</a>');

		$('#hris-profile-family .children').append(template);

	});

	$('#hris-profile-family').on('click', '.remove', function(e) {
		e.preventDefault();

		$(this).parent().remove();
	});

});

}(jQuery));
