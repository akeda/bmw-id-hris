<?php
/**
 * Plugin Name: HRIS Profile
 * Plugin URI: http://gedex.web.id
 * Description: Plugin that customizes WordPress user profile for HRIS.
 * Version: 0.1.0
 * Author: Akeda Bagus
 * Author URI: http://gedex.web.id
 * License: MIT
 *
 * Plugin that customizes WordPress user profile for HRIS.
 *
 * @package HRIS Profile
 * @version 0.1.0
 * @author Akeda Bagus <admin@gedex.web.id>
 * @license MIT License
 */

/**
 * Class that bootstrap this plugin.
 *
 * @since 0.1.0
 */
class HRIS_Profile {

	/**
	 * Components to load.
	 *
	 * @since 0.1.0
	 * @var array
	 * @access protected
	 */
	protected $_components_to_load;

	public function __construct() {
		// Initialization.
		$this->is_scripts_and_styles_enqueued = false;
		$this->_components_to_load            = array(
			'Edit_Profile',
		);

		// Autoloader registration
		spl_autoload_register( array( $this, 'autoload' ) );

		// Set the constants needed by the plugin.
		add_action( 'plugins_loaded', array( $this, 'define_constants' ), 1 );

		// Load components.
		add_action( 'plugins_loaded', array( $this, 'load_components' ), 2 );
	}

	/**
	 * Autoloader for this plugin.
	 *
	 * @since 0.1.0
	 */
	public function autoload( $class ) {
		if ( false === strpos( $class, __CLASS__ ) )
			return;

		$class = str_replace( array( '_', 'hris-profile-' ), array( '-', '' ), strtolower( $class ) ) . '.php';

		require_once HRIS_PROFILE_INCLUDES . $class;
	}

	/**
	 * Defines constants used by the plugin.
	 *
	 * @since 0.1.0
	 * @action plugins_loaded
	 */
	public function define_constants() {
		// Set the version number of the plugin.
		define( 'HRIS_PROFILE_VERSION', '0.1.0' );

		// Set constant path to this plugin directory.
		define( 'HRIS_PROFILE_DIR', trailingslashit( plugin_dir_path( __FILE__ ) ) );

		// Set constant path to this plugin URL.
		define( 'HRIS_PROFILE_URI', trailingslashit( plugin_dir_url( __FILE__ ) ) );

		// Set the constant path to this includes directory.
		define( 'HRIS_PROFILE_INCLUDES', HRIS_PROFILE_DIR . trailingslashit( 'includes' ) );
	}

	/**
	 * Load components.
	 *
	 * @since 0.1.0
	 * @action plugins_loaded
	 */
	public function load_components() {
		$GLOBALS['hris_profile_components'] = array();

		foreach ( $this->_components_to_load as $component ) {
			$class = 'HRIS_Profile_' . $component;
			$GLOBALS['hris_profile_components'][ $component ] = new $class;
			$GLOBALS['hris_profile_components'][ $component ]->load();
		}
	}
}

$GLOBALS['hris_profile'] = new HRIS_Profile();
