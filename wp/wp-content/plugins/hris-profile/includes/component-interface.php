<?php

/**
 * Interface that plugin-component's class MUST implement.
 *
 * @package HRIS Profile
 * @since 0.1.0
 * @author Akeda Bagus <akeda@x-team.com>
 */
interface HRIS_Profile_Component_Interface {
	public function load();
}
