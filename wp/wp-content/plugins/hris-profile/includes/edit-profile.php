<?php

class HRIS_Profile_Edit_Profile implements HRIS_Profile_Component_Interface {

	/**
	 * Whether all scripts and styles need by this plugin were enqueued already.
	 *
	 * @since 0.1.0
	 * @var bool
	 * @access private
	 */
	private $_is_scripts_and_styles_enqueued;

	private $sections;

	private $fields;

	public function __construct() {
		$this->_is_scripts_and_styles_enqueued = false;

		$this->sections = array(
			'detail' => array(
				'label' => __( 'Detail', 'hris-profile' ),
			),
			'cv' => array(
				'label' => __( 'CV', 'hris-profile' ),
			),
			'family' => array(
				'label' => __( 'Family', 'hris-profile' ),
			),
		);

		$this->fields = array(
			'detail' => array(
				'birth_place' => array(
					'label' => __( 'Birth place', 'hris-profile' ),
					'type'  => 'text',
				),
				'birth_date' => array(
					'label'    => __( 'Birth date', 'hris-profile' ),
					'type'     => 'select_date',
					'min_year' => 1940,
					'max_year' => date( 'Y' ) - 17,
				),
				'gender' => array(
					'label'   => __( 'Gender', 'hris-profile' ),
					'type'    => 'select',
					'options' => array(
						'female' => __( 'Female', 'hris-profile' ),
						'male'   => __( 'Male', 'hris-profile' ),
					)
				),
				'religion' => array(
					'label'   => __( 'Religion', 'hris-profile' ),
					'type'    => 'select',
					'options' => array(
						'islam'     => __( 'Islam', 'hris-profile' ),
						'katolik'   => __( 'Katolik', 'hris-profile' ),
						'protestan' => __( 'Protestan', 'hris-profile' ),
					),
				),
				'address' => array(
					'label'   => __( 'Address', 'hris-profile' ),
					'type'    => 'textarea',
				),
				'zip_code' => array(
					'label'   => __( 'Zip Code', 'hris-profile' ),
					'type'    => 'text',
				),
				'identity_number' => array(
					'label'   => __( 'Identity Number', 'hris-profile' ),
					'type'    => 'text',
				),
				'mobile_phone' => array(
					'label'   => __( 'Mobile Phone', 'hris-profile' ),
					'type'    => 'text',
				),
				'join_date' => array(
					'label'    => __( 'Join Date', 'hris-profile' ),
					'type'     => 'select_date',
					'min_year' => 1990,
					'max_year' => date( 'Y' ) + 1,
				),
				'end_contract_date' => array(
					'label'    => __( 'End Contract Date', 'hris-profile' ),
					'type'     => 'select_date',
					'min_year' => 1990,
					'max_year' => date( 'Y' ) + 10,
				),
				'resign_date' => array(
					'label'    => __( 'Resign Date', 'hris-profile' ),
					'type'     => 'select_date',
					'min_year' => 1990,
					'max_year' => date( 'Y' ) + 1,
				),
			),
			'cv' => array(
			),
			'family' => array(
				'spouse' => array(
					'label'    => __( 'Spouse', 'hris-profile' ),
					'type'     => 'family_member',
				),
				'children' => array(
					'label'    => __( 'Children', 'hris-profile' ),
					'type'     => 'multi_family_member',
				),
			),
		);
	}

	public function load() {

		// Disable color scheme picker. 'admin_color_scheme_picker' is defined before
		// 'admin_enqueue_scripts' where we can hook into to remove the action.
		// See wp-includes/default-filters.php
		add_action( 'admin_enqueue_scripts', function() {
			remove_all_actions( 'admin_color_scheme_picker' );
		} );

		// Enqueue styles and scripts
		add_action( 'load-profile.php', array( $this, 'on_profile_load' ) );
		add_action( 'load-user-edit.php', array( $this, 'on_profile_load' ) );

		// Add custom fields for user.
		add_action( 'show_user_profile', array( $this, 'render_custom_fields' ), 99 );
		add_action( 'edit_user_profile', array( $this, 'render_custom_fields' ), 99 );

		// Saves custom fields.
		add_action( 'personal_options_update',  array( $this, 'save_custom_fields' ) );
		add_action( 'edit_user_profile_update', array( $this, 'save_custom_fields' ) );
	}

	public function on_profile_load() {
		if ( $this->_is_scripts_and_styles_enqueued ) {
			return;
		}

		wp_enqueue_style( 'hris-profile-widgets', HRIS_PROFILE_URI . '/css/edit-profile.css' );

		wp_enqueue_script( 'hris-profile-widgets', HRIS_PROFILE_URI . '/js/edit-profile.js', array( 'jquery', 'jquery-ui-tabs' ), HRIS_PROFILE_VERSION );

		$this->_is_scripts_and_styles_enqueued = true;
	}

	public function render_custom_fields( $user ) {
		?>
		<h3><?php _e( 'Employee Information', 'hris-profile' ) ?></h3>
		<div id="hris-profile">
			<ul class="tabs">
				<?php foreach ( $this->sections as $section => $section_prop ) : ?>
				<?php $section_ref = sprintf( 'hris-profile-%s', $section ); ?>
				<li>
					<a href="#<?php echo esc_attr( $section_ref ); ?>" data-type="<?php echo esc_attr( $section_ref ); ?>" class="nav-tab-link"><?php echo esc_html( $section_prop['label'] ); ?></a>
				</li>
				<?php endforeach; ?>
			</ul>

			<?php foreach ( $this->fields as $section => $fields ) : ?>
			<?php $section_ref = sprintf( 'hris-profile-%s', $section ); ?>
			<div id="<?php echo esc_attr( $section_ref ) ?>">
				<table class="form-table">
				<tbody>
				<?php foreach ( $fields as $field => $field_prop ) : ?>
					<tr>
						<th><?php echo esc_html( $field_prop['label'] ); ?></th>
						<td valign="top">
						<?php
						$field_args = wp_parse_args( $field_prop, array(
							'name'  => $field,
							'id'    => $field,
							'value' => get_user_meta( $user->ID, $field, true ),
							'desc'  => '',
						) );
						$renderer = 'render_field_' . $field_prop['type'];
						$this->{$renderer}( $field_args );
						?>
						</td>
					</tr>
				<?php endforeach; ?>
				</tbody>
				</table>
			</div>
			<?php endforeach; ?>
		</div>
		<!-- / hris-profile -->
		<?php
	}

	public function save_custom_fields( $user_id ) {
		foreach ( $this->fields as $fields ) {
			foreach ( $fields as $field => $__ ) {
				if ( isset( $_POST[ $field ] ) ) {
					// @todo sanitizer
					update_user_meta( $user_id, $field, $_POST[ $field ] );
				}
			}
		}
	}

	/**
	 * Callback for `add_settings_field` where field's type is text.
	 *
	 * @param  array $args Field args passed to `add_settings_field` call
	 * @return void
	 */
	public static function render_field_text( $args ) {

		extract( wp_parse_args( $args, array(
			'type'    => 'text',
			'pattern' => '',
		) ) );
		/**
		 * @var string $type
		 * @var string $pattern
		 * @var string $name
		 * @var string $id
		 * @var string $value
		 * @var string $desc
		 */
		?>
		<input
			type="<?php echo esc_attr( $type ) ?>"
			name="<?php echo esc_attr( $name ) ?>"
			id="<?php echo esc_attr( $id ) ?>"
			class="regular-text"
			<?php if ( $pattern ): ?>
				pattern="<?php echo esc_attr( $pattern ) ?>"
			<?php endif; ?>
			value="<?php echo esc_attr( $value ); ?>">
		<?php if ( $desc ): ?>
		<p class="description"><?php echo esc_html( $desc ); ?></p>
		<?php endif;
	}

	public static function render_field_textarea( $args ) {

		extract( wp_parse_args( $args, array(
			'rows'    => 5,
			'pattern' => '',
		) ) );
		/**
		 * @var string $type
		 * @var string $pattern
		 * @var string $name
		 * @var string $id
		 * @var string $value
		 * @var string $desc
		 */
		?>
		<textarea
			rows="<?php echo esc_attr( $rows ) ?>"
			name="<?php echo esc_attr( $name ) ?>"
			id="<?php echo esc_attr( $id ) ?>"
			class="textarea"
			<?php if ( $pattern ): ?>
				pattern="<?php echo esc_attr( $pattern ) ?>"
			<?php endif; ?>><?php echo esc_html( $value ); ?></textarea>
		<?php if ( $desc ): ?>
		<p class="description"><?php echo esc_html( $desc ); ?></p>
		<?php endif;
	}

	/**
	 * Callback for `add_settings_field` where field's type is number.
	 *
	 * @param  array $args Field args passed to `add_settings_field` call
	 * @return void
	 */
	public static function render_field_number( $args ) {
		$args['type'] = 'number';
		self::render_field_text( $args );
	}

	/**
	 * Callback for `add_settings_field` where field's type is select.
	 *
	 * @param array $args Field args passed to `add_settings_field` call
	 */
	public static function render_field_select( $args ) {
		extract( wp_parse_args( $args, array(
			'options' => array(),
		) ) );
		/**
		 * @var array  $options
		 * @var string $id
		 * @var string $name
		 * @var string $value
		 * @var string $desc
		 */
		extract( $args );
		if ( ! isset( $args['options'] ) )
			$options = array();
		?>
		<select id="<?php echo esc_attr( $id ); ?>" name="<?php echo esc_attr( $name ); ?>">
			<?php foreach ( $options as $option_val => $option_label ): ?>
				<option value="<?php echo esc_attr( $option_val ); ?>" <?php selected( $option_val, $value ); ?>>
					<?php echo esc_html( $option_label ); ?>
				</option>
			<?php endforeach; ?>
		</select>
		<?php if ( $desc ): ?>
		<p class="description"><?php echo esc_html( $desc ); ?></p>
		<?php endif;
	}

	public function render_field_select_date( $args ) {
		extract( wp_parse_args( $args, array(
			'days'     => array_combine( range(1, 31), range(1, 31) ),
			'months'   => array_combine( range(1, 12), range(1, 12) ),
			'min_year' => 1940,
			'max_year' => date( 'Y' ) - 17,
		) ) );

		$args['years'] = array_combine( range( $args['min_year'], $args['max_year'] ), range( $args['min_year'], $args['max_year'] ) );
		/**
		 * @var string $id
		 * @var string $name
		 * @var string $value
		 * @var string $desc
		 */
		extract( $args );

		if ( ! isset( $value ) || ( isset( $value ) && ! $value ) )
			$value = array(
				'day'   => 1,
				'month' => 1,
				'year'  => $min_year,
			);
		?>
		<select id="<?php echo esc_attr( $id . '[day]' ); ?>" name="<?php echo esc_attr( $name . '[day]' ); ?>">
			<?php foreach ( $days as $option_val => $option_label ): ?>
				<option value="<?php echo esc_attr( $option_val ); ?>" <?php selected( $option_val, $value['day'] ); ?>>
					<?php echo esc_html( $option_label ); ?>
				</option>
			<?php endforeach; ?>
		</select>
		-
		<select id="<?php echo esc_attr( $id . '[month]' ); ?>" name="<?php echo esc_attr( $name . '[month]' ); ?>">
			<?php foreach ( $months as $option_val => $option_label ): ?>
				<option value="<?php echo esc_attr( $option_val ); ?>" <?php selected( $option_val, $value['month'] ); ?>>
					<?php echo esc_html( $option_label ); ?>
				</option>
			<?php endforeach; ?>
		</select>
		-
		<select id="<?php echo esc_attr( $id . '[year]' ); ?>" name="<?php echo esc_attr( $name . '[year]' ); ?>">
			<?php foreach ( $years as $option_val => $option_label ): ?>
				<option value="<?php echo esc_attr( $option_val ); ?>" <?php selected( $option_val, $value['year'] ); ?>>
					<?php echo esc_html( $option_label ); ?>
				</option>
			<?php endforeach; ?>
		</select>
		<?php if ( $desc ): ?>
		<p class="description"><?php echo esc_html( $desc ); ?></p>
		<?php endif;
	}

	public function render_field_family_member( $args ) {
		if ( ! isset( $args['value'] ) || ( isset( $args['value'] ) && empty( $args['value'] ) ) ) {
			$args['value'] = array(
				'name' => '',
				'birth_date' => array(
					'day' => 1,
					'month' => 1,
					'year' => 1940,
				),
				'birth_place' => '',
			);
		}

		$name_args = $args;
		$name_args['type'] = 'text';
		$name_args['value'] = $args['value']['name'];
		$name_args['name'] = $args['name'] . '[name]';
		$name_args['id']   = $args['name'] . '[name]';
		$name_args['desc'] = __( 'Name', 'hris-profile' );
		$this->render_field_text( $name_args );

		echo '<br>';

		$birth_date_args = $args;
		$birth_date_args['type'] = 'select_date';
		$birth_date_args['value'] = $args['value']['birth_date'];
		$birth_date_args['min_year'] = 1940;
		$birth_date_args['max_year'] = date( 'Y' ) ;
		$birth_date_args['name'] = $args['name'] . '[birth_date]';
		$birth_date_args['id']   = $args['name'] . '[birth_date]';
		$birth_date_args['desc'] = __( 'Birth Date', 'hris-profile' );
		$this->render_field_select_date( $birth_date_args );

		echo '<br>';

		$birth_place_args = $args;
		$birth_place_args['type'] = 'text';
		$birth_place_args['value'] = $args['value']['birth_place'];
		$birth_place_args['name'] = $args['name'] . '[birth_place]';
		$birth_place_args['id']   = $args['name'] . '[birth_place]';
		$birth_place_args['desc'] = __( 'Birth Place', 'hris-profile' );
		$this->render_field_text( $birth_place_args );
	}

	public function render_field_multi_family_member( $args ) {
		$show_delete_link = true;
		if ( ! isset( $args['value'] ) || ( isset( $args['value'] ) && ! $args['value'] ) ) {
			$args['value'] = array(
				array(
					'name' => '',
					'birth_date' => array(
						'day' => 1,
						'month' => 1,
						'year' => 1940,
					),
					'birth_place' => '',
				),
			);
			$show_delete_link = false;
		}
		?>
		<ul class="children">
		<?php foreach ( $args['value'] as $index => $member ) : ?>
			<li>
			<?php
			$arg = array(
				'type'  => 'family_member',
				'id'    => $args['name'] . '[' . $index . ']',
				'name'  => $args['name'] . '[' . $index . ']',
				'value' => $member,
			);
			$this->render_field_family_member( $arg );
			if ( $show_delete_link ) {
				echo '<br><a href="#" class="remove">Remove</a>';
			}
			?>
			</li>
		<?php endforeach; ?>
		</ul>
		<br>
		<a href="#" class="button add-children">Add Children</a>
		<?php
	}
}
