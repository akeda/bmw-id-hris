<?php
/**
 * Plugin Name: HRIS Common JS
 * Plugin URI: http://gedex.web.id
 * Description: Enqueue common scripts used by HRIS.
 * Author: Akeda Bagus
 * Author URI: http://gedex.web.id/
 * Version: 0.1.0
 *
 * Enqueue common scripts used by HRIS.
 *
 * @license MIT License.
 */

if ( ! defined( 'HRIS_COMMON_JS_DIR' ) )
	define( 'HRIS_COMMON_JS_DIR', trailingslashit( plugin_dir_path( __FILE__ ) ) );

if ( ! defined( 'HRIS_COMMON_JS_URL' ) )
	define( 'HRIS_COMMON_JS_URL', trailingslashit( plugin_dir_url( __FILE__ ) ) );

/**
 * Enqueue common scripts in admin page.
 *
 * @action admin_enqueue_scripts
 */
function _hris_common_js_enqueue_scripts_in_admin() {
	$scripts_to_enqueue = array(
		// FILL ME IF YOU NEED JS AVAILABLE IN ADMIN
	);

	foreach ($scripts_to_enqueue as $handle => $script) {
		/**
		 * @var string $src
		 * @var array  $deps
		 * @var string $ver
		 * @var bool   $in_footer
		 */
		extract( $script );
		wp_enqueue_script( $handle, $src, $deps, $ver, $in_footer );
	}
}
add_action( 'admin_enqueue_scripts', '_hris_common_js_enqueue_scripts_in_admin' );
