<?php
/**
 * Plugin Name: HRIS ACF Money
 * Plugin URI: http://gedex.web.id
 * Description: Money field for ACF.
 * Author: Akeda Bagus
 * Author URI: http://gedex.web.id/
 * Version: 0.1.0
 *
 * Money field for ACF.
 *
 * @license MIT License.
 */
function hris_acf_money_register_fields() {
	define( 'HRIS_ACF_MONEY_VERSION', '0.1.0' );

	require_once plugin_dir_path( __FILE__ ) . '/money-v4.php';
}
add_action( 'acf/register_fields', 'hris_acf_money_register_fields' );

