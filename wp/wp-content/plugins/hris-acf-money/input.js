(function($) {
	'use strict';

	acf = acf || {};
	acf.fields = acf.fields || {};

	acf.fields.money = {
		init: function() {
			acf.fields.money.bindMoneyFormatting(document);

			// When new row is added via repeater.
			$(document).on('acf/setup_fields', acf.fields.money.bindMoneyFormattingOnRepeater);
		},

		bindMoneyFormatting: function(container) {
			$('.acf-money', container).each( function() {
				var $input = $('.input', this),
						thousand_separator = $(this).data('thousand_separator'),
						decimal_separator = $(this).data('decimal_separator'),
						decimal_places = $(this).data('decimal_places');

				$input.number(true, decimal_places, decimal_separator, thousand_separator);
				$input.on('blur', acf.fields.money.blurHandler);
			} );
		},

		bindMoneyFormattingOnRepeater: function(e, container) {
			var $c = $(container);

			if ($c.hasClass('row')) {
				acf.fields.money.bindMoneyFormatting($c);
			}
		},

		blurHandler: function(e) {
			var $el = $(e.target),
					$alt = $el.parent().find('.input-alt');

			$alt.val( $el.val() );
		},

	};

	// Init!
	$(acf.fields.money.init);

}(jQuery));
