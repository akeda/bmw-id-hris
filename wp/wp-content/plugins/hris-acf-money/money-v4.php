<?php

class acf_field_money extends acf_field
{

	public $settings; // will hold info such as dir / path
	public $defaults; // will hold default field options


	/**
	 *  __construct
	 *
	 *  Set name / label needed for actions / filters
	 *
	 *  @since	3.6
	 *  @date	23/01/13
	 */
	public function __construct() {
		// vars
		$this->name     = 'money';
		$this->label    = __( 'Money' );
		$this->category = __( 'Basic', 'acf'); // Basic, Content, Choice, etc
		$this->defaults = array(
			'thousand_separator' => '.',
			'decimal_separator'  => ',',
			'decimal_places'     => 2,
		);

		// do not delete!
    parent::__construct();

    // settings
		$this->settings = array(
			'path'    => apply_filters( 'acf/helpers/get_path', __FILE__ ),
			'dir'     => apply_filters( 'acf/helpers/get_dir',  __FILE__ ),
			'version' => '1.0.0'
		);
	}

	/**
	 *  create_options()
	 *
	 *  Create extra options for your field. This is rendered when editing a field.
	 *  The value of $field['name'] can be used (like bellow) to save extra data to the $field
	 *
	 *  @type	action
	 *  @since	3.6
	 *  @date	23/01/13
	 *
	 *  @param	$field	- an array holding all the field's data
	 */
	public function create_options( $field ) {
		// defaults?
		/*
		$field = array_merge($this->defaults, $field);
		*/

		// key is needed in the field names to correctly save the data
		$key = $field['name'];


		// Create Field Options HTML
		?>
		<tr class="field_option field_option_<?php echo $this->name; ?>">
			<td class="label">
				<label><?php _e( 'Thousand separator', 'acf'); ?></label>
				<p class="description"><?php _e( 'Thousand separator', 'acf'); ?></p>
			</td>
			<td>
				<?php
				do_action('acf/create_field', array(
					'type'  => 'text',
					'name'  => 'fields[' . $key . '][thousand_separator]',
					'value' => $field['thousand_separator'],
				));
				?>
			</td>
		</tr>
		<tr class="field_option field_option_<?php echo $this->name; ?>">
			<td class="label">
				<label><?php _e( 'Decimal separator', 'acf'); ?></label>
				<p class="description"><?php _e( 'Decimal separator', 'acf'); ?></p>
			</td>
			<td>
				<?php
				do_action('acf/create_field', array(
					'type'  => 'text',
					'name'  => 'fields[' . $key . '][decimal_separator]',
					'value' => $field['decimal_separator'],
				));
				?>
			</td>
		</tr>
		<tr class="field_option field_option_<?php echo $this->name; ?>">
			<td class="label">
				<label><?php _e( 'decimal_places', 'acf'); ?></label>
				<p class="description"><?php _e( 'Decimal places', 'acf'); ?></p>
			</td>
			<td>
				<?php
				do_action('acf/create_field', array(
					'type'  => 'number',
					'name'  => 'fields[' . $key . '][decimal_places]',
					'value' => $field['decimal_places'],
				));
				?>
			</td>
		</tr>
		<?php
	}


	/**
	 *  create_field()
	 *
	 *  Create the HTML interface for your field
	 *
	 *  @param	$field - an array holding all the field's data
	 *
	 *  @type	action
	 *  @since	3.6
	 *  @date	23/01/13
	 */
	public function create_field( $field ) {
		// defaults?
		/*
		$field = array_merge($this->defaults, $field);
		*/

		// perhaps use $field['preview_size'] to alter the markup?


		// create Field HTML
		?>
		<div class="acf-money" data-thousand_separator="<?php echo esc_attr( $field['thousand_separator'] ); ?>" data-decimal_separator="<?php echo esc_attr( $field['decimal_separator'] ); ?>" data-decimal_places="<?php echo esc_attr( $field['decimal_places'] ); ?>">
			<input type="hidden" value="<?php echo esc_attr( $field['value'] ); ?>" name="<?php echo esc_attr( $field['name'] ); ?>" class="input-alt" />
			<input type="text" value="<?php echo esc_attr( $field['value'] ); ?>" class="input" />
		</div>
		<?php
	}


	/**
	 *  input_admin_enqueue_scripts()
	 *
	 *  This action is called in the admin_enqueue_scripts action on the edit screen where your field is created.
	 *  Use this action to add css + javascript to assist your create_field() action.
	 *
	 *  $info	http://codex.wordpress.org/Plugin_API/Action_Reference/admin_enqueue_scripts
	 *  @type	action
	 *  @since	3.6
	 *  @date	23/01/13
	 */
	public function input_admin_enqueue_scripts() {
		extract( array(
			'handle'    => 'acf-money-jquery-number',
			'src'       => plugin_dir_url( __FILE__ ) . 'jquery.number.min.js',
			'deps'      => array( 'jquery' ),
			'ver'       => HRIS_ACF_MONEY_VERSION,
			'in_footer' => false,
		) );
		wp_enqueue_script( $handle, $src, $deps, $ver, $in_footer );

		extract( array(
			'handle'    => 'acf-money-input',
			'src'       => plugin_dir_url( __FILE__ ) . 'input.js',
			'deps'      => array( 'acf-money-jquery-number' ),
			'ver'       => HRIS_ACF_MONEY_VERSION,
			'in_footer' => false,
		) );
		wp_enqueue_script( $handle, $src, $deps, $ver, $in_footer );

	}

}


// create field
new acf_field_money();

