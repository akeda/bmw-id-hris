<?php
/**
 * Plugin Name: HRIS ACF Style
 * Plugin URI: http://gedex.web.id
 * Description: ACF style for HRIS.
 * Author: Akeda Bagus
 * Author URI: http://gedex.web.id/
 * Version: 0.1.0
 *
 * ACF style for HRIS.
 *
 * @license MIT License.
 */

/**
 * Enqueue admin scripts to override default ACF style.
 *
 * @since 0.1.0
 * @action admin_enqueue_scripts
 * @return void
 */
function hris_acf_style_admin_scripts() {
	// If ACF is not activated don't enqueue the scripts
	if ( ! function_exists( 'the_field' ) ) {
		return;
	}

	wp_enqueue_style( 'hris_acf_style', plugin_dir_url( __FILE__ ) . '/hris-acf-style.css' );
}
add_action( 'admin_enqueue_scripts', 'hris_acf_style_admin_scripts' );
