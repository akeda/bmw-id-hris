(function($){

$(function() {
	var get_number_of_days = function(e) {
		console.log('change fired');

		var $start_date = $('input[type="hidden"]:first', '#acf-start_date');
		var $end_date = $('input[type="hidden"]:first', '#acf-end_date');

		if ( ! $start_date.val() || ! $end_date.val() )
			return;

		var tpl_loading = '<span id="calculating_number_of_days" style="font-size: 9px; font-style: italic; color: #999">&nbsp; Calculating number of days...</span>';
		var $target     = $('#acf-field-number_of_days');

		$target.val('').parent().append( tpl_loading );

		var xhr = $.ajax({
			url: wp.ajax.settings.url,
			type: 'POST',
			data: {
				'action': 'check_number_of_days',
				'start_date': $start_date.val(),
				'end_date': $end_date.val(),
			}
		});

		xhr.done( function( r ) {
			if ( 'data' in r && 'number_of_days' in r['data']) {
				$target.val( r['data']['number_of_days'] );
			}
			$('#calculating_number_of_days').remove();
		} );
		xhr.fail( function( xhr, textStatus ) {
			console.log(textStatus);
			$('#calculating_number_of_days').remove();
		} );

	};

	$(document).on('change', '#acf-start_date input[type="text"]', get_number_of_days );
	$(document).on('change', '#acf-end_date input[type="text"]', get_number_of_days );

});

}(jQuery));
