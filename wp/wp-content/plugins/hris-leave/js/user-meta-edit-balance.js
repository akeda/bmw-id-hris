/* globals jQuery, _, USER_META_EDIT_BALANCE_VARS */
(function($, _) {
	'use strict';

	var self = $.extend(
		{},
		USER_META_EDIT_BALANCE_VARS
	);

	/**
	 * Init.
	 */
	self.init = function() {
		_.templateSettings.variable = 'vars';
		self.render_header();

		self.on_change_user();
	};

	/**
	 * Render header part.
	 */
	self.render_header = function() {
		var $header = $( '#user-meta-edit-balance > header' );

		self.tpl_users = _.template( $('script#select-users').html() );
		$header.append( self.tpl_users( self.users ) );
		setTimeout( function() {
			$( '#' + self.users.select ).trigger( 'change' );
		} );
	};

	/**
	 * Render the body, which is table of balance meta for selected user.
	 */
	self.render_body = function( data ) {
		console.log(data);
	};

	/**
	 * Handler when selected user is changed.
	 */
	self.on_change_user = function() {
		$( '#' + self.users.select ).on( 'change', function() {
			$('.current-user').html( $('option:selected', this).text() );
			self.fetch_user_data();
		} );
	};

	/**
	 * Given a user id fetch his/her data.
	 */
	self.fetch_user_data = function( user_id ) {
		var ajax = $.ajax( {
			url: ajaxurl,
			type: 'POST',
			data: {
				action: 'hris_leave_get_user_meta_balance',
				user_id: $( '#' + self.users.select ).val()
			}
		} );

		ajax.done( function( result ) {
			if ( result.success ) {
				self.render_body( result.data );
			}
		} );
	};

	/**
	 * Fires the init.
	 */
	$( function() {
		self.init();
	} );

}(jQuery, _));
