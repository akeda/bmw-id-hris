<?php
/**
 * Allows administrator / hris_manager to edit user meta of leave balance.
 */
class HRIS_Leave_User_Meta_Edit_Balance implements HRIS_Leave_Component_Interface {
	const SLUG = 'hris-leave-user-meta-edit-balance';

	public function load() {
		add_action( 'admin_menu', array( $this, 'register_menu' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'scripts_and_styles' ) );

		add_action( 'wp_ajax_hris_leave_get_user_meta_balance', array( $this, 'ajax_get_user_meta_balance' ) );
	}

	public function register_menu() {
		if ( ! hris_check_user_role( 'hris_manager' ) && ! hris_check_user_role( 'administrator' ) )
			return;

		add_submenu_page(
			'edit.php?post_type=' . HRIS_Leave_Post_Type::NAME,
			__( 'User Balance', 'hris-leave' ),
			__( 'User Balance', 'hris-leave' ),
			'edit_hris_leave_records',
			self::SLUG,
			array( $this, 'render_page_callback' )
		);
	}

	public function render_page_callback() {
		require HRIS_LEAVE_TEMPLATES . 'user-meta-edit-balance.php';
	}

	public function ajax_get_user_meta_balance() {
		$user_id = $_REQUEST['user_id'];
		$data    = get_user_meta( $user_id, HRIS_Leave_Balance::LEAVE_BALANCE_USER_META, true );

		wp_send_json_success( $data );
	}

	public function scripts_and_styles( $hook ) {
		$expected_hook = HRIS_Leave_Post_Type::NAME . '_page_' . self::SLUG;
		if ( $hook !== $expected_hook ) {
			return;
		}

		wp_enqueue_script( 'underscore' );
		wp_enqueue_script( self::SLUG, HRIS_LEAVE_URI . '/js/user-meta-edit-balance.js', array( 'jquery', 'underscore' ), HRIS_LEAVE_VERSION );
		wp_enqueue_style(  self::SLUG, HRIS_LEAVE_URI . '/css/user-meta-edit-balance.css', array(), HRIS_LEAVE_VERSION, 'all' );

		wp_localize_script(
			self::SLUG,
			'USER_META_EDIT_BALANCE_VARS',
			$this->get_exported_js()
		);
	}

	public function get_exported_js() {
		$setting = hris_get_component_from_module( 'approval', 'Setting' );
		$users   = array(
			'data'   => $setting->get_users(),
			'select' => 'hris-users',
		);

		return compact( 'users' );
	}
}
