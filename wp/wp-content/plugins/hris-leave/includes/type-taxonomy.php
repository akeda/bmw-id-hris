<?php

class HRIS_Leave_Type_Taxonomy implements HRIS_Leave_Component_Interface {
	const NAME = 'hris_leave_type';

	public function load() {
		// Register custom post type on the 'init' hook.
		add_action( 'init', array( $this, 'register_taxonomy' ) );
	}

	public function register_taxonomy() {
		$args = array(
			'public'            => true,
			'show_ui'           => true,
			'show_in_nav_menus' => true,
			'show_tagcloud'     => false,
			'show_admin_column' => true,
			'hierarchical'      => true,
			'query_var'         => false,

			'capabilities' => array(
				'manage_terms' => 'manage_hris_leave',
				'edit_terms'   => 'edit_hris_leave_types',
				'delete_terms' => 'manage_hris_leave',
				'assign_terms' => 'edit_hris_leave_types',
			),

			'rewrite' => false,

			/* Labels used when displaying taxonomy and terms. */
			'labels' => array(
				'name'                       => __( 'Leave Types',                           'hris-leave' ),
				'singular_name'              => __( 'Leave Type',                            'hris-leave' ),
				'menu_name'                  => __( 'Leave Types',                           'hris-leave' ),
				'name_admin_bar'             => __( 'Leave Type',                            'hris-leave' ),
				'search_items'               => __( 'Search Leave Types',                    'hris-leave' ),
				'popular_items'              => __( 'Popular Leave Types',                   'hris-leave' ),
				'all_items'                  => __( 'All Leave Types',                       'hris-leave' ),
				'edit_item'                  => __( 'Edit Leave Type',                       'hris-leave' ),
				'view_item'                  => __( 'View Leave Type',                       'hris-leave' ),
				'update_item'                => __( 'Update Leave Type',                     'hris-leave' ),
				'add_new_item'               => __( 'Add New Leave Type',                    'hris-leave' ),
				'new_item_name'              => __( 'New Leave Types Name',                  'hris-leave' ),
				'separate_items_with_commas' => __( 'Separate leave types with commas',      'hris-leave' ),
				'add_or_remove_items'        => __( 'Add or remove leave types',             'hris-leave' ),
				'choose_from_most_used'      => __( 'Choose from the most used leave types', 'hris-leave' ),
			)
		);

		// Register the taxonomy.
		register_taxonomy( self::NAME, array( HRIS_Leave_Post_Type::NAME ), $args );
	}
}
