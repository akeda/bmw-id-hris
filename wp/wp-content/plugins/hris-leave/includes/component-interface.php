<?php

/**
 * Interface that plugin-component's class MUST implement.
 *
 * @package HRIS Leave
 * @since 0.1.0
 * @author Akeda Bagus <admin@gedex.web.id>
 */
interface HRIS_Leave_Component_Interface {
	public function load();
}
