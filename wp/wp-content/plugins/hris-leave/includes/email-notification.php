<?php

class HRIS_Leave_Email_Notification implements HRIS_Leave_Component_Interface {

	/**
	 * Called by plugin's main-file in plugins_load action.
	 *
	 * @since 0.1.0
	 * @return void
	 */
	public function load() {
		add_action( 'updated_approver', array( $this, 'updated_approver' ), 99, 4 );
	}

	/**
	 * On updated approver send mail notification to requestor and approver.
	 *
	 * @since  0.1.0
	 * @action updated_approver
	 * @param string|int $approver
	 * @param string $old_status
	 * @param string $new_status
	 * @param int $post_id Post ID
	 * @return void
	 */
	public function updated_approver( $approver,  $old_status, $new_status, $post_id ) {

		$excluded_status = array( 'auto-draft', 'draft', 'future', 'private', 'inherit', 'trash' );
		if ( in_array( $new_status, $excluded_status ) )
			return;

		$subject = __( 'HRIS Workflow', 'hris-leave' );

		$post = get_post( $post_id );

		$author          = get_user_by( 'id', $post->post_author );
		$requestor_email = $author->user_email;
		$requestor_by    = sprintf( __( 'by %s', 'hris-leave' ), $author->display_name );

		$approver_to = '';
		if ( $approver ) {
			$approver       = get_user_by( 'id', intval( $approver ) );
			$approver_email = $approver->user_email;
			$approver_to    = sprintf( __( 'to %s', 'hris-leave' ), $approver->display_name );
			$approver_by    = sprintf( __( 'by %s', 'hris-leave' ), $approver->display_name );
		}

		// Filter mail from
		add_filter( 'wp_mail_from',      '__return_empty_string' );
		add_filter( 'wp_mail_from_name', function() { return 'HRIS'; } );

		// From other than approved to approved.
		if ( HRIS_Approval_Setting::APPROVED === $new_status
		     &&
		     HRIS_Approval_Setting::APPROVED !== $old_status
		   )
		{
			$message =
			"Leave entry {$post->post_title} {$requestor_by} has been approved.\r\n" .

			"If you have any problems logging on, Please contact your system administrator."
			;
			$recipents = array( $requestor_email );
			if ( isset( $approver_email ) ) {
				$recipents[] = $approver_email;
			}
			wp_mail( $recipents, $subject, $message );

		} else if ( $old_status !== $new_status ) {
			// Rejection
			if ( HRIS_Approval_Setting::REJECTED === $new_status ) {

				// Send to requestor.
				$message =
				"Your leave request, {$post->post_title}, has been rejected {$approver_by}.\r\n";
				wp_mail( $requestor_email, $subject, $message );

			} else { // Next approver
				// Send to requestor.
				$message =
				"Your leave request, {$post->post_title}, has been sent {$approver_to} to be reviewed.\r\n";
				wp_mail( $requestor_email, $subject, $message );

				// Send to approver.
				$message =
				"There are new work items, {$post->post_title}, in your Workflow inbox in the HRIS.\r\n" .
				"For Approver, Log on to HRIS, Go to Leave --> Approval --> Approve/Reject.\r\n\r\n" .

				"If you have any problems logging on, Please contact your system administrator."
				;
				wp_mail( $approver_email, $subject, $message );
			}
		}

	}
}
