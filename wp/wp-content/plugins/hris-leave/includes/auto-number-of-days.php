<?php
/**
 * Set auto number of days when requesting new leave.
 *
 * @package HRIS Leave
 * @since 0.1.0
 * @author Akeda Bagus <admin@gedex.web.id>
 */
class HRIS_Leave_Auto_Number_Of_Days implements HRIS_Leave_Component_Interface {

	/**
	 * Called by plugin's main-file in plugins_load action.
	 *
	 * @since 0.1.0
	 * @return void
	 */
	public function load() {
		add_action( 'load-post-new.php', array( $this, 'register_js' ) );

		add_action( 'wp_ajax_check_number_of_days', array( $this, 'check_number_of_days' ) );
		add_action( 'wp_ajax_nopriv_check_number_of_days', array( $this, 'check_number_of_days' ) );
	}


	public function register_js() {
		$screen = get_current_screen();
		if ( $screen->id !== HRIS_Leave_Post_Type::NAME && $screen->action !== 'add' )
			return;

		wp_enqueue_script( 'hris-leave-auto-number-of-days', HRIS_LEAVE_URI . '/js/auto-number-of-days.js', array( 'jquery', ), HRIS_LEAVE_VERSION );
	}

	public function check_number_of_days() {
		$start_date     = $_REQUEST['start_date'];
		$end_date       = $_REQUEST['end_date'];
		$number_of_days = hris_get_number_of_days( $start_date, $end_date );

		$number_of_days['start_date'] = $start_date;
		$number_of_days['end_date'] = $end_date;

		wp_send_json_success( $number_of_days );
	}
}
