<?php

/**
 * Save leave as event so that it appears on calendar.
 *
 * @package HRIS Leave
 * @since 0.1.0
 * @author Akeda Bagus <admin@gedex.web.id>
 */
class HRIS_Leave_Save_As_Event implements HRIS_Leave_Component_Interface {

	/**
	 * Called by plugin's main-file in plugins_load action.
	 *
	 * @since 0.1.0
	 * @return void
	 */
	public function load() {
		add_action( 'save_post', array( $this, 'insert_leave_as_event' ) );
	}

	/**
	 * Insert leave as event.
	 *
	 * @action save_post
	 * @param  int $post_id Post ID
	 * @return void
	 */
	public function insert_leave_as_event( $post_id ) {
		$post_obj = get_post( $post_id );
		if ( HRIS_Leave_Post_Type::NAME !== $post_obj->post_type )
			return;

		if ( HRIS_Approval_Setting::APPROVED !== $post_obj->post_status )
			return;

		$inserted_as_event = get_post_meta( $post_id, 'event_id', true );
		if ( $inserted_as_event )
			return;

		$start_date_ts = strtotime( get_field( 'start_date', $post_id ) );
		$end_date_ts   = strtotime( get_field( 'end_date', $post_id ) );

		$start_date = date( 'd/m/Y', $start_date_ts );
		$end_date   = date( 'd/m/Y', $end_date_ts );
		$employee   = get_user_by( 'id', $post_obj->post_author );

		if ( $start_date === $end_date ) {
			$leave_date = 'on ' . $start_date;
		} else {
			$leave_date = 'from ' . $start_date . ' until ' . $end_date;
		}

		$EM_Event = new EM_Event();
		$EM_Event->force_status     = 'publish';
		$EM_Event->event_name       = sprintf( '%s takes leave %s', $employee->display_name, $leave_date );
		$EM_Event->event_all_day    = 1;
		$EM_Event->event_start_date = date( 'Y-m-d', $start_date_ts );
		$EM_Event->event_end_date   = date( 'Y-m-d', $end_date_ts );
		$EM_Event->event_owner      = $post_obj->post_author;

		if ( $EM_Event->save() ) {
			update_post_meta( $post_id, 'event_id', $EM_Event->ID );
		}
	}
}
