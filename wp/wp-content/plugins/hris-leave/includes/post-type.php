<?php

class HRIS_Leave_Post_Type implements HRIS_Leave_Component_Interface {

	const NAME = 'hris_leave_record';

	public function load() {
		// Register custom post type on the 'init' hook.
		add_action( 'init', array( $this, 'register_post_type' ) );

		// When saving_post, do the followings:
		// - Generatea unique title that serves as leave code.
		// - Sets appropriate post_status
		add_action( 'save_post', array( $this, 'save_post' ) );

		add_action( 'add_meta_boxes_' . self::NAME, array( $this, 'remove_meta_boxes' ) );
	}

	/**
	 * Registers post type needed by this plugin.
	 *
	 * @since 0.1.0
	 * @access public
	 * @return void
	 */
	public function register_post_type() {
		// Set up the arguments for the portfolio item post type.
		$args = array(
			'description'         => '',
			'public'              => true,
			'publicly_queryable'  => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'exclude_from_search' => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'menu_position'       => 5,
			// 'menu_icon'           => HRIS_LEAVE_URI . 'images/menu-icon.png',
			'can_export'          => true,
			'delete_with_user'    => true,
			'hierarchical'        => false,
			'has_archive'         => false,
			'query_var'           => false,
			'capability_type'     => self::NAME,
			'map_meta_cap'        => true,

			'capabilities' => array(

				// meta caps (don't assign these to roles)
				'edit_post'              => 'edit_'   . self::NAME,
				'read_post'              => 'read_'   . self::NAME,
				'delete_post'            => 'delete_' . self::NAME,

				// primitive/meta caps
				'create_posts'           => 'create_hris_leave_records',

				// primitive caps used outside of map_meta_cap()
				'edit_posts'             => 'edit_hris_leave_records',
				'edit_others_posts'      => 'manage_hris_leave',
				'publish_posts'          => 'manage_hris_leave',
				'read_private_posts'     => 'read',

				// primitive caps used inside of map_meta_cap()
				'read'                   => 'read',
				'delete_posts'           => 'manage_hris_leave',
				'delete_private_posts'   => 'manage_hris_leave',
				'delete_published_posts' => 'manage_hris_leave',
				'delete_others_posts'    => 'manage_hris_leave',
				'edit_private_posts'     => 'edit_hris_leave_records',
				'edit_published_posts'   => 'edit_hris_leave_records'
			),

			'rewrite' => false,

			// What features the post type supports.
			'supports' => array(
				'author',
			),

			// Labels used when displaying the posts.
			'labels' => array(
				'name'               => __( 'Leave',                   'hris-leave' ),
				'singular_name'      => __( 'Leave',                   'hris-leave' ),
				'menu_name'          => __( 'Leave',                   'hris-leave' ),
				'name_admin_bar'     => __( 'Leave',                   'hris-leave' ),
				'add_new'            => __( 'Add New',                 'hris-leave' ),
				'add_new_item'       => __( 'Add New Leave',           'hris-leave' ),
				'edit_item'          => __( 'Edit Leave',              'hris-leave' ),
				'new_item'           => __( 'New Leave',               'hris-leave' ),
				'view_item'          => __( 'View Leave',              'hris-leave' ),
				'search_items'       => __( 'Search Leave',            'hris-leave' ),
				'not_found'          => __( 'No leave found',          'hris-leave' ),
				'not_found_in_trash' => __( 'No leave found in trash', 'hris-leave' ),
				'all_items'          => __( 'Leave',                   'hris-leave' ),
			)
		);

		// Register the post type.
		register_post_type( 'hris_leave_record', $args );
	}

	public function save_post( $post_id ) {
		if ( ! isset( $_POST['post_type'] ) ) {
			return;
		}

		// If this isn't 'hris_leave_record', don't update it.
		if ( self::NAME !== $_POST['post_type'] ) {
			return;
		}

		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
			return;

		if ( wp_is_post_revision( $post_id ) )
			return;

		// If this is a revision, get real post ID.
		// if ( $parent_id = wp_is_post_revision( $post_id ) ) {
		// 	$post_id = $parent_id;
		// }

		// Unhook this method so it doesn't loop infinitely.
		remove_action( 'save_post', array( $this, 'save_post' ) );

		$post_args = array(
			'ID' => $post_id,
		);

		// Check if post_title is empty
		if ( empty( $_POST['post_title'] ) ) {
			$code = $this->_generate_code( $post_id );

			$post_args['post_title'] = $code;
		}
		$old_status = get_post_status( $post_id );

		// Gets post_status to update;
		$post_args['post_status'] = $this->_get_post_status_to_update( $post_id );

		$new_status = $post_args['post_status'];

		// Updates the post status and/or title
		wp_update_post( $post_args );

		// Gets post object
		$post_obj = get_post( $post_id );

		// Updates approver in post meta based on next status.
		$approver = hris_get_approver_by_status( self::NAME, $post_obj->post_author, $post_args['post_status'] );
		if ( $approver ) {
			update_post_meta( $post_id, 'approver', $approver );
		} else {
			delete_post_meta( $post_id, 'approver' );
		}
		do_action( 'updated_approver', $approver, $old_status, $new_status, $post_id );

		// Re-hook this method.
		add_action( 'save_post', array( $this, 'save_post' ) );
	}

	protected function _generate_code( $post_id ) {
		$current_date = date( 'Ymd', current_time( 'timestamp', 0 ) );

		return 'L' . $current_date . str_pad( $post_id, 10, '0', STR_PAD_LEFT );
	}

	protected function _get_post_status_to_update( $post_id ) {
		$post_obj    = get_post( $post_id );
		$post_type   = get_post_type( $post_id );
		$post_status = get_post_status( $post_id );
		$next_status = 'draft';

		// Get the next post status and updates it.
		$next_status = hris_get_next_post_status( $post_type, $post_obj->post_author, $post_status );
		if ( ! $next_status )
			$next_status = 'draft';

		return $next_status;
	}

	public function remove_meta_boxes() {
		global $post_type;

		if ( HRIS_Leave_Post_Type::NAME !== $post_type )
			return;

		$removed_meta_boxes = array(
			HRIS_Leave_Type_Taxonomy::NAME . 'div' => array(
				'screen'  => self::NAME,
				'context' => 'side',
			),
			'submitdiv' => array(
				'screen'  => self::NAME,
				'context' => 'side',
			),
		);

		foreach ( $removed_meta_boxes as $id => $param ) {
			if ( ! isset( $param['screen'] ) || ! isset( $param['context'] ) ) {
				continue;
			}

			if ( is_array( $param['context'] ) ) {
				foreach ( $param['context'] as $context ) {
					remove_meta_box( $id, $param['screen'], $context );
				}
			} else {
				remove_meta_box( $id, $param['screen'], $param['context'] );
			}
		}
	}
}
