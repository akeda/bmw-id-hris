<?php

/**
 * Any customizations need to be done when editing the post.
 *
 * @package HRIS Leave
 * @since 0.1.0
 * @author Akeda Bagus <admin@gedex.web.id>
 */
class HRIS_Leave_Edit_Post implements HRIS_Leave_Component_Interface {

	/**
	 * Called by plugin's main-file in plugins_load action.
	 *
	 * @since 0.1.0
	 * @return void
	 */
	public function load() {
		add_filter( 'post_updated_messages', array( $this, 'alter_updated_messages' ) );

		add_filter( 'ef_custom_status_list', array( $this, 'alter_post_status_list' ), 10, 2 );

		add_action( 'load-post-new.php', array( $this, 'scripts_and_styles' ) );
		add_action( 'load-post.php', array( $this, 'scripts_and_styles' ) );

		// Redirect for hris_requestor, otherwise check for custom actions.
		add_action( 'load-post.php', array( $this, 'post_actions' ) );
	}

	/**
	 * Alter messages when a post updated.
	 *
	 * @since 0.1.0
	 * @filter post_updated_messages
	 * @param array $messages Post updated messages.
	 * @return array Altered messages
	 */
	public function alter_updated_messages( $messages ) {
		global $post_type;

		if ( HRIS_Leave_Post_Type::NAME !== $post_type ) {
			return $messages;
		}

		// Messages
		$updated_message   =  __( 'Leave entry updated.', 'hris-leave' );
		$approved_message  = __( 'Leave entry approved.', 'hris-leave' );
		$saved_message     = __( 'Leave entry saved.', 'hris-leave' );
		$submitted_message = __( 'Leave entry submitted.', 'hris-leave' );

		// Update 'Post updated. View post.' message.
		if ( isset( $messages['post'][1] ) ) {
			$messages['post'][1] = $updated_message;
		}

		// Update 'Post restored to revision from %s'.
		if ( isset( $messages['post'][5] ) ) {
			$messages['post'][5] = $updated_message;
		}

		// Update 'Post published. View post.' message.
		if ( isset( $messages['post'][6] ) ) {
			$messages['post'][6] = $approved_message;
		}

		// Update 'Post saved.' message.
		if ( isset( $messages['post'][7] ) ) {
			$messages['post'][7] = $saved_message;
		}

		// Update 'Post submitted.' message.
		if ( isset( $messages['post'][8] ) ) {
			$messages['post'][8] = $submitted_message;
		}

		// Update 'Post draft updated.' message.
		if ( isset( $messages['post'][10] ) ) {
			$messages['post'][10] = $updated_message;
		}

		return $messages;
	}

	public function alter_post_status_list( $custom_statuses, $post ) {

		if ( HRIS_Leave_Post_Type::NAME !== $post->post_type ) {
			return $custom_statuses;
		}

		if ( current_user_can( 'manage_hris_leave' ) ) {
			return $custom_statuses;
		}

		$removed_statuses = array( 'approved', 'director-review', 'supervisor-review' );
		foreach ( $custom_statuses as $key => $status ) {
			if ( in_array( $status->slug, $removed_statuses ) ) {
				unset( $custom_statuses[ $key ] );
			}
		}

		return $custom_statuses;
	}

	public function scripts_and_styles() {
		if ( ! isset( $_GET['post_type'] ) && ! isset( $_GET['post'] ) )
			return;

		if ( isset( $_GET['post_type'] ) && HRIS_Leave_Post_Type::NAME !== $_GET['post_type'] )
			return;

		if ( isset( $_GET['post'] ) ) {
			$post = get_post( $_GET['post'] );
			if ( ! $post )
				return;

			if ( HRIS_Leave_Post_Type::NAME !== $post->post_type )
				return;
		}

		wp_enqueue_script( 'hris-leave-edit-post', HRIS_LEAVE_URI . '/js/edit-post.js', array( 'jquery', ), HRIS_LEAVE_VERSION );
	}

	public function post_actions() {
		if ( ! isset( $_GET['post'] ) )
			return;

		$post = get_post( $_GET['post'] );
		if ( HRIS_Leave_Post_Type::NAME !== $post->post_type )
			return;

		if ( ! hris_check_user_role( 'administrator' ) ) {
			wp_redirect( admin_url( 'edit.php?post_type=' . HRIS_Leave_Post_Type::NAME ) );
		}

		if ( ! isset( $_GET['action'] ) )
			return;

		if ( 'approve' === $_GET['action'] )
			$this->_do_approve( $post->ID );
		else if ( 'reject' === $_GET['action'] )
			$this->_do_reject( $post->ID );
	}

	protected function _do_approve( $post_id ) {
		check_admin_referer( 'approve-post_' . $post_id );

		// Gets post object
		$post_obj = get_post( $post_id );

		$current_status = get_post_status( $post_id );
		$next_status    = hris_get_next_post_status( HRIS_Leave_Post_Type::NAME, $post_obj->post_author, $current_status );
		if ( ! $next_status )
			return;

		wp_update_post( array(
			'ID'          => $post_id,
			'post_status' => $next_status,
		) );

		// Updates approver in post meta based on next status.
		$approver = hris_get_approver_by_status( HRIS_Leave_Post_Type::NAME, $post_obj->post_author, $next_status );
		if ( $approver ) {
			update_post_meta( $post_id, 'approver', $approver );
		} else {
			delete_post_meta( $post_id, 'approver' );
		}
		do_action( 'updated_approver', $approver, $current_status, $next_status, $post_id );

		wp_redirect( add_query_arg( 'approved', 1, wp_get_referer() ) );
		exit();
	}

	protected function _do_reject( $post_id ) {
		check_admin_referer( 'reject-post_' . $post_id );

		$old_status = get_post_status( $post_id );

		wp_update_post( array(
			'ID'          => $post_id,
			'post_status' => HRIS_Approval_Setting::REJECTED,
		) );

		$approver = get_post_meta( $post_id, 'approver', true );

		do_action( 'updated_approver', $approver, $old_status, HRIS_Approval_Setting::REJECTED, $post_id );

		delete_post_meta( $post_id, 'approver' );


		wp_redirect( add_query_arg( 'rejected', 1, wp_get_referer() ) );
		exit();
	}
}
