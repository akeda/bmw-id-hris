<?php

/**
 * Report for this post type.
 *
 * @package HRIS Leave
 * @since 0.1.0
 * @author Akeda Bagus <admin@gedex.web.id>
 */
class HRIS_Leave_Report implements HRIS_Leave_Component_Interface {

	const SLUG = 'hris-leave-report';

	/**
	 * Called by plugin's main-file in plugins_load action.
	 *
	 * @since 0.1.0
	 * @return void
	 */
	public function load() {
		add_action( 'admin_menu', array( $this, 'register_report_menu' ) );

		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_statics' ) );
	}

	/**
	 * Register report menu.
	 *
	 * @since 0.1.0
	 * @action admin_menu
	 * @return void
	 */
	public function register_report_menu() {
		if ( hris_check_user_role( 'hris_requestor' ) || hris_check_user_role( 'hris_approver' ) )
			return;

		add_submenu_page(
			'edit.php?post_type=' . HRIS_Leave_Post_Type::NAME,
			__( 'Report', 'hris-leave' ),
			__( 'Report', 'hris-leave' ),
			'edit_hris_leave_records',
			self::SLUG,
			array( $this, 'report_page_callback' )
		);
	}

	/**
	 * Render report page. Callback for 'add_submenu_page'.
	 *
	 * @since 0.1.0
	 * @return void
	 */
	public function report_page_callback() {
		$users   = get_users( array(
			'exclude' => array( 1, ),
		) );
		$year    = intval( date( 'Y' ) );
		$balance = hris_get_component_from_module( 'leave', 'Balance' );
		?>
		<table class="widefat fixed hris-leave-report">
			<thead>
				<tr>
					<th class="manage-column"><?php _e( 'No', 'hris-leave' ); ?></th>
					<th class="manage-column"><?php _e( 'Employee', 'hris-leave' ); ?></th>
					<th class="manage-column"><?php printf( __( 'Vacation for year %s', 'hris-leave' ), $year ); ?></th>
					<?php foreach ( range( 1, 12 ) as $month ) : ?>
						<th><?php echo esc_html( date( 'M', mktime(0, 0, 0, $month, 10) ) ); ?></th>
					<?php endforeach; ?>
					<th><?php _e( 'Total vacation taken', 'hris-leave' ); ?></th>
					<th><?php printf( __( 'Vacatation balance will be expired by Dec %d', 'hris-leave' ), $year ); ?></th>
				</tr>
			</thead>
			<tbody>
			<?php $counter = 0; ?>
			<?php foreach ( $users as $user ) : ?>
				<tr<?php echo ( ++$counter % 2 === 0 ) ? ' class="alternate"' : ''; ?>>
					<?php
					$user_balance   = $balance->get_leave_balance( $user->ID, $year );
					$total_approved = 0;
					?>
					<td><?php echo esc_html( $counter ); ?></td>
					<td valign="top">
						<?php echo esc_html( $user->display_name ); ?>
					</td>
					<td>
						<?php echo esc_html( $user_balance['balance'] ); ?>
					</td>
					<?php foreach ( range( 1, 12 ) as $month ) : ?>
						<td>
							<?php
							$approved_this_month = $user_balance['approved_per_month'][ $month ];
							echo esc_html( $approved_this_month );
							$total_approved += $approved_this_month;
							?>
						</td>
					<?php endforeach; ?>
					<td><?php echo esc_html( $total_approved ); ?>
					<td>
					<?php
					$remaining = $user_balance['balance'] - $total_approved;
					if ( $remaining ) {
						echo esc_html( $remaining );
					}
					?>
				</td>
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
		<?php
	}

	public function enqueue_statics( $hook ) {
		$expected_hook = HRIS_Leave_Post_Type::NAME . '_page_' . self::SLUG;
		if ( $expected_hook !== $hook )
			return;

		wp_enqueue_style( 'hris_report_setting_style', HRIS_LEAVE_URI . '/css/balance.css' );
	}
}
