<?php

class HRIS_Articles_Rename_Labels {

	/**
	 * Label text in singular form.
	 *
	 * @var string
	 * @access protected
	 */
	protected $singular;

	/**
	 * Label text in plural form.
	 *
	 * @var string
	 * @access protected
	 */
	protected $plural;

	public function load() {
		$this->singular = apply_filters( 'rename_default_post_type_labels_singular', __( 'article', 'grammys-articles' ) );
		$this->plural   = apply_filters( 'rename_default_post_type_labels_plural', __( 'articles', 'grammys-articles' ) );

		add_action( 'admin_init',     array( $this, '_rename_default_post_type_labels' ) );
		add_action( 'admin_bar_init', array( $this, '_rename_default_post_type_labels' ) );
		add_action( 'admin_menu',     array( $this, '_rename_default_post_type_menus'  ) );
	}

	/**
	 * Renames the default post type's labels to article.
	 *
	 * @action admin_init
	 * @action admin_bar_init
	 */
	public function _rename_default_post_type_labels() {
		global $wp_post_types;

		$default_labels = array(
			'name'               => _x( '{Plural}', 'post type general name', 'grammys-articles' ),
			'singular_name'      => _x( '{Singular}', 'post type singular name', 'grammys-articles' ),
			'add_new'            => __( 'Add New', 'grammys-articles' ),
			'add_new_item'       => __( 'Add New {Singular}', 'grammys-articles' ),
			'edit_item'          => __( 'Edit {Singular}', 'grammys-articles' ),
			'new_item'           => __( 'New {Singular}', 'grammys-articles' ),
			'view_item'          => __( 'View {Singular}', 'grammys-articles' ),
			'search_items'       => __( 'Search {Plural}', 'grammys-articles' ),
			'not_found'          => __( 'No {plural} found', 'grammys-articles' ),
			'not_found_in_trash' => __( 'No {plural} found in Trash', 'grammys-articles' ),
			'parent_item_colon'  => '',
			'all_items'          => __( 'All {Plural}', 'grammys-articles' ),
			'menu_name'          => '{Plural}',
			'name_admin_bar'     => '{Singular}',
		);

		$replacements = array(
			'{Singular}' => ucwords( $this->singular ),
			'{singular}' => $this->singular,
			'{Plural}'   => ucwords( $this->plural ),
			'{plural}'   => $this->plural,
		);

		$labels = &$wp_post_types['post']->labels;

		foreach ( $default_labels as $key => &$value )
			$labels->$key = str_replace( array_keys( $replacements ), array_values( $replacements ), $value );
	}

	/**
	 * Renames the default post type's admin menu labels.
	 *
	 * @action admin_menu
	 */
	public function _rename_default_post_type_menus() {
		global $menu, $submenu;

		if ( isset( $menu[5][0] ) )
			$menu[5][0] = esc_html( ucwords( $this->plural ) );

		if ( isset( $submenu['edit.php'][5][0] ) )
			$submenu['edit.php'][5][0] = esc_html( sprintf( '%s %s', __( 'All', 'grammys-articles' ), ucwords( $this->plural ) ) );
	}
}