(function($){

	var self = {
		buttonTemplate: '<div><input type="submit" class="button primary" value="Submit"></div>',

		init: function() {
			this.addSubmitButton();
			this.setFiedldsToReadOnly();
			this.removePostBoxContainer();
		},

		addSubmitButton: function() {
			$('.acf_postbox .inside').append(this.buttonTemplate);
		},

		setFiedldsToReadOnly: function() {
			$('#acf-field-ref_no').attr('readonly', 'readonly');
			$('input', '#acf-request_date').attr('readonly', 'readonly');
			$('#acf-field-employee_name').attr('readonly', 'readonly');
			$('#acf-field-department').attr('readonly', 'readonly');
		},

		removePostBoxContainer: function() {
			$('#postbox-container-1').remove();
		}
	};

	// Init!
	$(function() {self.init();});

}(jQuery));
