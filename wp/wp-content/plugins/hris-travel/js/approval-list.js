(function($) {

	var self = {
		init: function() {
			this.appendApprovalModeLink();
		},

		appendApprovalModeLink: function() {
			$('.row-title').each(function() {
				var $el = $(this),
						href = $el.attr('href');

				$el.attr( 'href', href + '&mode=approval' );
			});
		}
	};

	$(function() {
		self.init();
	});

}(jQuery));
