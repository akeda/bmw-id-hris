<?php

class HRIS_Travel_UE_Taxonomy implements HRIS_Travel_Component_Interface {
	const NAME = 'hris_travel_ue';

	public function load() {
		// Register custom post type on the 'init' hook.
		add_action( 'init', array( $this, 'register_taxonomy' ) );
	}

	public function register_taxonomy() {
		$args = array(
			'public'            => true,
			'show_ui'           => true,
			'show_in_nav_menus' => true,
			'show_tagcloud'     => false,
			'show_admin_column' => true,
			'hierarchical'      => true,
			'query_var'         => false,

			'capabilities' => array(
				'manage_terms' => 'manage_hris_travel',
				'edit_terms'   => 'edit_hris_travel_ue',
				'delete_terms' => 'manage_hris_travel',
				'assign_terms' => 'edit_hris_travel_ue',
			),

			'rewrite' => false,

			/* Labels used when displaying taxonomy and terms. */
			'labels' => array(
				'name'                       => __( 'Travel UE',                           'hris-travel' ),
				'singular_name'              => __( 'Travel UE',                           'hris-travel' ),
				'menu_name'                  => __( 'Travel UE',                           'hris-travel' ),
				'name_admin_bar'             => __( 'Travel UE',                           'hris-travel' ),
				'search_items'               => __( 'Search Travel UE',                    'hris-travel' ),
				'popular_items'              => __( 'Popular Travel UE',                   'hris-travel' ),
				'all_items'                  => __( 'All Travel UE',                       'hris-travel' ),
				'edit_item'                  => __( 'Edit Travel UE',                      'hris-travel' ),
				'view_item'                  => __( 'View Travel UE',                      'hris-travel' ),
				'update_item'                => __( 'Update Travel UE',                    'hris-travel' ),
				'add_new_item'               => __( 'Add New Travel UE',                   'hris-travel' ),
				'new_item_name'              => __( 'New Travel UE Name',                  'hris-travel' ),
				'separate_items_with_commas' => __( 'Separate travel UE with commas',      'hris-travel' ),
				'add_or_remove_items'        => __( 'Add or remove travel UE',             'hris-travel' ),
				'choose_from_most_used'      => __( 'Choose from the most used travel UE', 'hris-travel' ),
			)
		);

		// Register the taxonomy.
		register_taxonomy( self::NAME, array( HRIS_Travel_Post_Type::NAME ), $args );
	}
}
