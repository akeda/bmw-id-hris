<?php

class HRIS_Travel_Type_Taxonomy implements HRIS_Travel_Component_Interface {
	const NAME = 'hris_travel_type';

	public function load() {
		// Register custom post type on the 'init' hook.
		add_action( 'init', array( $this, 'register_taxonomy' ) );
	}

	public function register_taxonomy() {
		$args = array(
			'public'            => true,
			'show_ui'           => true,
			'show_in_nav_menus' => true,
			'show_tagcloud'     => false,
			'show_admin_column' => true,
			'hierarchical'      => true,
			'query_var'         => false,

			'capabilities' => array(
				'manage_terms' => 'manage_hris_travel',
				'edit_terms'   => 'edit_hris_travel_types',
				'delete_terms' => 'manage_hris_travel',
				'assign_terms' => 'edit_hris_travel_types',
			),

			'rewrite' => false,

			/* Labels used when displaying taxonomy and terms. */
			'labels' => array(
				'name'                       => __( 'Travel Types',                           'hris-travel' ),
				'singular_name'              => __( 'Travel Type',                            'hris-travel' ),
				'menu_name'                  => __( 'Travel Types',                           'hris-travel' ),
				'name_admin_bar'             => __( 'Travel Type',                            'hris-travel' ),
				'search_items'               => __( 'Search Travel Types',                    'hris-travel' ),
				'popular_items'              => __( 'Popular Travel Types',                   'hris-travel' ),
				'all_items'                  => __( 'All Travel Types',                       'hris-travel' ),
				'edit_item'                  => __( 'Edit Travel Type',                       'hris-travel' ),
				'view_item'                  => __( 'View Travel Type',                       'hris-travel' ),
				'update_item'                => __( 'Update Travel Type',                     'hris-travel' ),
				'add_new_item'               => __( 'Add New Travel Type',                    'hris-travel' ),
				'new_item_name'              => __( 'New Travel Types Name',                  'hris-travel' ),
				'separate_items_with_commas' => __( 'Separate travel types with commas',      'hris-travel' ),
				'add_or_remove_items'        => __( 'Add or remove travel types',             'hris-travel' ),
				'choose_from_most_used'      => __( 'Choose from the most used travel types', 'hris-travel' ),
			)
		);

		// Register the taxonomy.
		register_taxonomy( self::NAME, array( HRIS_Travel_Post_Type::NAME ), $args );
	}
}
