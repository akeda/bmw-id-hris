<?php

class HRIS_Travel_Post_Type implements HRIS_Travel_Component_Interface {

	const NAME = 'hris_travel_record';

	public function load() {
		// Register custom post type on the 'init' hook.
		add_action( 'init', array( $this, 'register_post_type' ) );

		// When saving_post, do the followings:
		// - Generatea unique title that serves as travel code.
		// - Sets appropriate post_status
		add_action( 'save_post', array( $this, 'save_post' ) );

		add_action( 'add_meta_boxes_' . self::NAME, array( $this, 'remove_meta_boxes' ) );
	}

	/**
	 * Registers post type needed by this plugin.
	 *
	 * @since 0.1.0
	 * @access public
	 * @return void
	 */
	public function register_post_type() {
		// Set up the arguments for the portfolio item post type.
		$args = array(
			'description'         => '',
			'public'              => true,
			'publicly_queryable'  => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'exclude_from_search' => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'menu_position'       => 5,
			'can_export'          => true,
			'delete_with_user'    => true,
			'hierarchical'        => false,
			'has_archive'         => false,
			'query_var'           => false,
			'capability_type'     => self::NAME,
			'map_meta_cap'        => true,

			'capabilities' => array(

				// meta caps (don't assign these to roles)
				'edit_post'              => 'edit_'   . self::NAME,
				'read_post'              => 'read_'   . self::NAME,
				'delete_post'            => 'delete_' . self::NAME,

				// primitive/meta caps
				'create_posts'           => 'create_hris_travel_records',

				// primitive caps used outside of map_meta_cap()
				'edit_posts'             => 'edit_hris_travel_records',
				'edit_others_posts'      => 'approve_hris_travel_records',
				'publish_posts'          => 'approve_hris_travel_records',
				'read_private_posts'     => 'read',

				// primitive caps used inside of map_meta_cap()
				'read'                   => 'read',
				'delete_posts'           => 'manage_hris_travel',
				'delete_private_posts'   => 'manage_hris_travel',
				'delete_published_posts' => 'manage_hris_travel',
				'delete_others_posts'    => 'manage_hris_travel',
				'edit_private_posts'     => 'approve_hris_travel_records',
				'edit_published_posts'   => 'approve_hris_travel_records'
			),

			'rewrite' => false,

			// What features the post type supports.
			'supports' => array(
				'author',
			),

			// Labels used when displaying the posts.
			'labels' => array(
				'name'               => __( 'Travel',                  'hris-travel' ),
				'singular_name'      => __( 'Travel',                  'hris-travel' ),
				'menu_name'          => __( 'Travel',                  'hris-travel' ),
				'name_admin_bar'     => __( 'Travel',                  'hris-travel' ),
				'add_new'            => __( 'Add New',                 'hris-travel' ),
				'add_new_item'       => __( 'Add New Travel',          'hris-travel' ),
				'edit_item'          => __( 'Edit Travel',             'hris-travel' ),
				'new_item'           => __( 'New Travel',              'hris-travel' ),
				'view_item'          => __( 'View Travel',             'hris-travel' ),
				'search_items'       => __( 'Search Travel',           'hris-travel' ),
				'not_found'          => __( 'No travel found',          'hris-travel' ),
				'not_found_in_trash' => __( 'No travel found in trash', 'hris-travel' ),
				'all_items'          => __( 'Travel',                  'hris-travel' ),
			)
		);

		// Register the post type.
		register_post_type( 'hris_travel_record', $args );
	}

	public function save_post( $post_id ) {
		if ( ! isset( $_POST['post_type'] ) ) {
			return;
		}

		// If this isn't 'hris_travel_record', don't update it.
		if ( self::NAME !== $_POST['post_type'] ) {
			return;
		}

		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
			return;

		if ( wp_is_post_revision( $post_id ) )
			return;

		// If this is a revision, get real post ID.
		// if ( $parent_id = wp_is_post_revision( $post_id ) ) {
		// 	$post_id = $parent_id;
		// }

		// Unhook this method so it doesn't loop infinitely.
		remove_action( 'save_post', array( $this, 'save_post' ) );

		$post_args = array(
			'ID' => $post_id,
		);

		$old_status = get_post_status( $post_id );

		$reviewer_status = 'approve'; // default to approved
		switch ( $old_status ) {
			case 'hr-review':
				$reviewer_status = get_field( 'hr_review_status', $post_id );
				break;
			case 'supervisor-review':
				$reviewer_status = get_field( 'supervisor_review_status', $post_id );
				break;
			case 'director-review':
				$reviewer_status = get_field( 'director_review_status', $post_id );
				break;
		}

		// Check if post_title is empty
		if ( empty( $_POST['post_title'] ) ) {
			$code = $this->_generate_code( $post_id );

			$post_args['post_title'] = $code;
		}

		// Gets post_status to update;
		if ( 'approve' === $reviewer_status ) {
			$post_args['post_status'] = $this->_get_post_status_to_update( $post_id );
		} else if ( 'reject' === $reviewer_status ) {
			$post_args['post_status'] = HRIS_Approval_Setting::REJECTED;
		}
		$new_status = $post_args['post_status'];

		// Updates the post status and/or title
		wp_update_post( $post_args );

		// Gets post object
		$post_obj = get_post( $post_id );

		// Updates approver in post meta based on next status.
		$approver = hris_get_approver_by_status( self::NAME, $post_obj->post_author, $post_args['post_status'] );
		if ( $approver ) {
			update_post_meta( $post_id, 'approver', $approver );
		} else {
			delete_post_meta( $post_id, 'approver' );
		}
		do_action( 'updated_approver', $approver, $old_status, $new_status, $post_id );

		// Re-hook this method.
		add_action( 'save_post', array( $this, 'save_post' ) );
	}

	protected function _generate_code( $post_id ) {
		$current_date = date( 'Ymd', current_time( 'timestamp', 0 ) );

		return 'TR' . $current_date . str_pad( $post_id, 10, '0', STR_PAD_LEFT );
	}

	protected function _get_post_status_to_update( $post_id ) {
		$post_obj    = get_post( $post_id );
		$post_type   = get_post_type( $post_id );
		$post_status = get_post_status( $post_id );
		$next_status = 'draft';

		// Get the next post status and updates it.
		$next_status = hris_get_next_post_status( $post_type, $post_obj->post_author, $post_status );
		if ( ! $next_status )
			$next_status = 'draft';

		return $next_status;
	}

	public function remove_meta_boxes() {
		global $post_type;

		if ( HRIS_Travel_Post_Type::NAME !== $post_type )
			return;

		$removed_meta_boxes = array(
			HRIS_Travel_Type_Taxonomy::NAME . 'div' => array(
				'screen'  => self::NAME,
				'context' => 'side',
			),
			HRIS_Travel_Country_Taxonomy::NAME . 'div' => array(
				'screen'  => self::NAME,
				'context' => 'side',
			),
			HRIS_Travel_UE_Taxonomy::NAME . 'div' => array(
				'screen'  => self::NAME,
				'context' => 'side',
			),
			'submitdiv' => array(
				'screen'  => self::NAME,
				'context' => 'side',
			),
		);

		foreach ( $removed_meta_boxes as $id => $param ) {
			if ( ! isset( $param['screen'] ) || ! isset( $param['context'] ) ) {
				continue;
			}

			if ( is_array( $param['context'] ) ) {
				foreach ( $param['context'] as $context ) {
					remove_meta_box( $id, $param['screen'], $context );
				}
			} else {
				remove_meta_box( $id, $param['screen'], $param['context'] );
			}
		}
	}
}
