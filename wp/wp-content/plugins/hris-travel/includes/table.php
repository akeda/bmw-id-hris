<?php

if ( ! class_exists( 'WP_Posts_List_Table' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/class-wp-posts-list-table.php' );
}

/**
 * @todo make this generic for other module.
 */
class HRIS_Travel_Table extends WP_Posts_List_Table {

	public function __construct() {
		parent::__construct( array(
			'screen'   => get_current_screen(),
		) );
	}

	public function prepare_items() {
		global $avail_post_stati, $wp_the_query;

		$setting  = hris_get_component_from_module( 'approval', 'Setting' );

		$statuses = $setting->get_post_statuses( HRIS_Travel_Post_Type::NAME );
		$user     = wp_get_current_user();

		$avail_post_stati = $statuses;

		$params   = array(
			'post_type'     => HRIS_Travel_Post_Type::NAME,
			'orderby'       => 'date',
			'order'         => 'DESC',
			'post_per_page' => -1,
			'meta_key'      => 'approver',
			'meta_value'    => $user->ID,
			'meta_compare'  => '=',
		);

		if ( hris_check_user_role( 'administrator' ) ) {
			wp( $params );
		} else {
			$wp_the_query->query( $params );
		}
	}

	public function display_tablenav() {}

	public function get_columns() {
		$columns = array(
			'title'          => __( 'Ref. No',     'hris-travel' ),
			'author'         => __( 'Employee',     'hris-travel' ),

			'taxonomy-' . HRIS_Leave_Type_Taxonomy::NAME => __( 'Travel Type',       'hris-travel' ),


		);

		return $columns;
	}

	public function get_column_info() {
		return array( $this->get_columns(), array(), array() );
	}
}
