<?php

class HRIS_Travel_UE_Taxonomy_Meta_Fields implements HRIS_Travel_Component_Interface {
	public function load() {
		add_action( HRIS_Travel_UE_Taxonomy::NAME . '_add_form_fields', array( $this, 'on_add_new' ) );
		add_action( HRIS_Travel_UE_Taxonomy::NAME . '_edit_form_fields', array( $this, 'on_edit' ) );

		add_action( 'edited_term', array( $this, 'on_save' ) );
		add_action( 'create_term', array( $this, 'on_save' ) );
	}

	public function on_add_new() {
		?>
		<div class="form-field">
			<label for="term_meta[code]"><?php _e( 'Code', 'hris-travel' ); ?>
			<input type="text" name="term_meta[code]" id="term_meta[code]" value="">
			</label>
			<p>
			<?php _e( 'UE Code', 'hris-travel' ); ?>
			</p>
		</div>
		<?php
	}

	public function on_edit( $term ) {
		$term_id = $term->term_id;
		$meta    = get_option( 'taxonomy_' . $term_id );
		?>
		<tr class="form-field">
			<th scope="row" valign="top"><label for="term_meta[custom_tecoderm_meta]">
				<?php _e( 'Code', 'hris-travel' ); ?></label>
			</th>
			<td>
				<input
					type="text"
					name="term_meta[code]"
					id="term_meta[code]"
					value="<?php echo esc_attr( $meta['code'] ); ?>">
			</td>
		</tr>
		<?php
	}

	public function on_save( $term_id ) {
		if ( isset( $_POST['term_meta'] ) ) {
			$meta = get_option( 'taxonomy_' . $term_id );
			foreach ( array_keys( $_POST['term_meta'] ) as $key ) {
				if ( isset( $_POST['term_meta'][ $key ] ) ) {
					$meta[ $key ] = $_POST['term_meta'][ $key ];
				}
			}
			update_option( 'taxonomy_' . $term_id, $meta );
		}
	}
}
