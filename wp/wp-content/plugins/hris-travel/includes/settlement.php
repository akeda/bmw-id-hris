<?php

class HRIS_Travel_Settlement {

	public function load() {
		add_filter( 'acf/get_field_groups', array( $this, 'filter_field_groups' ) );

	}

	public function filter_field_groups( array $groups ) {
		global $post;

		$post = get_post( $post );
		if ( HRIS_Travel_Post_Type::NAME !== get_post_type( $post ) ) {
			return $groups;
		}

		if ( HRIS_Approval_Setting::APPROVED === get_post_status( $post->ID ) ) {
			$excluded_groups = array( 'HRIS Travel' );
		} else {
			$excluded_groups = array(
				'HRIS Travel Settlement',
				'HRIS Travel Daily Allowance',
				'HRIS Travel Accomodation',
				'HRIS Travel Cost',
			);
		}

		// Removed excluded groups.
		foreach ( $groups as $key => $group ) {
			if ( in_array( $group['title'], $excluded_groups ) ) {
				unset( $groups[ $key ] );
			}
		}

		return $groups;
	}

}
