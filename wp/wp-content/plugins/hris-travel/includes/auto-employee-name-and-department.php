<?php
/**
 * Set employee name and department fields on post-new screen.
 *
 * @package HRIS Travel
 * @since 0.1.0
 * @author Akeda Bagus <admin@gedex.web.id>
 */
class HRIS_Travel_Auto_Employee_Name_And_Department implements HRIS_Travel_Component_Interface {

	/**
	 * Called by plugin's main-file in plugins_load action.
	 *
	 * @since 0.1.0
	 * @return void
	 */
	public function load() {
		add_filter( 'acf/load_value', array( $this, 'default_value_for_employee_and_department' ), 99, 3 );
	}

	/**
	 * Set employee's name and department fields on post-new screen.
	 *
	 * @action acf/load_value
	 * @param string $value
	 * @param int $post_id
	 * @param array $field
	 * @return string
	 */
	public function default_value_for_employee_and_department( $value, $post_id, $field ) {
		$screen = get_current_screen();
		if ( $screen->id !== HRIS_Travel_Post_Type::NAME )
			return $value;

		if ( $field['name'] !== 'employee_name' && $field['name'] !== 'department' )
			return $value;

		$post_obj  = get_post( $post_id );
		$author_id = $post_obj->post_author;
		$author    = get_user_by( 'id', $author_id );

		if ( $field['name'] === 'employee_name' )
			return $author->display_name;

		if ( $field['name'] === 'department' ) {
			return get_user_meta( $author_id, 'department', true );
		}

		return $value;
	}
}
