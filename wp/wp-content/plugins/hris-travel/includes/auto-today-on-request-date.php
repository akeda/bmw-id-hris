<?php
/**
 * Set current date on request date field on post-new screen.
 *
 * @package HRIS Travel
 * @since 0.1.0
 * @author Akeda Bagus <admin@gedex.web.id>
 */
class HRIS_Travel_Auto_Today_On_Request_Date implements HRIS_Travel_Component_Interface {

	/**
	 * Called by plugin's main-file in plugins_load action.
	 *
	 * @since 0.1.0
	 * @return void
	 */
	public function load() {
		add_filter( 'acf/load_value', array( $this, 'default_value_for_request_date' ), 99, 3 );
	}

	/**
	 * Set current date on request date field on post-new screen.
	 *
	 * @action acf/load_value
	 * @param string $value
	 * @param int $post_id
	 * @param array $field
	 * @return string
	 */
	public function default_value_for_request_date( $value, $post_id, $field ) {
		$screen = get_current_screen();
		if ( $screen->id !== HRIS_Travel_Post_Type::NAME && $screen->action !== 'add' )
			return $value;

		if ( $field['name'] !== 'request_date' )
			return $value;

		if ( empty( $value ) )
			return date( str_replace( array( 'dd', 'mm', 'yy' ), array( 'd', 'm', 'Y' ), $field['date_format'] ), current_time( 'timestamp', 0 ) );

		return $value;
	}
}
