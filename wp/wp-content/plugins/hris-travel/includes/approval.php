<?php

class HRIS_Travel_Approval implements HRIS_Travel_Component_Interface {

	const SLUG = 'hris-travel-approval';

	private $_list_table;

	public function load() {
		$hook = sprintf( 'load-%s_page_%s', HRIS_Travel_Post_Type::NAME, self::SLUG );
		add_action( $hook, array( $this, 'on_page_load' ) );

		add_action( 'admin_menu', array( $this, 'register_approval_menu' ) );
	}

	public function on_page_load() {
		if ( ! isset( $_REQUEST['post_type'] ) )
			return;

		if ( HRIS_Travel_Post_Type::NAME !== $_REQUEST['post_type'] )
			return;

		if ( ! isset( $_REQUEST['page'] ) )
			return;

		if ( self::SLUG !== $_REQUEST['page'] )
			return;

		extract( array(
			'handle'    => 'approval-list',
			'src'       => HRIS_TRAVEL_URI . 'js/approval-list.js',
			'deps'      => array(),
			'ver'       => HRIS_TRAVEL_VERSION,
			'in_footer' => true,
		) );
		wp_enqueue_script( $handle, $src, $deps, $ver, $in_footer );

		require_once HRIS_TRAVEL_INCLUDES . '/table.php';
		$this->_list_table = new HRIS_Travel_Table();
		$this->_list_table->prepare_items();
	}

	/**
	 * Register approval menu.
	 *
	 * @since 0.1.0
	 * @action admin_menu
	 * @return void
	 */
	public function register_approval_menu() {
		add_submenu_page(
			'edit.php?post_type=' . HRIS_Travel_Post_Type::NAME,
			__( 'Approval', 'hris-travel' ),
			__( 'Approval', 'hris-travel' ),
			'approve_hris_travel_records',
			self::SLUG,
			array( $this, 'approval_page_callback' )
		);
	}

	/**
	 * Render approval page. Callback for 'add_submenu_page'.
	 *
	 * @since 0.1.0
	 * @return void
	 */
	public function approval_page_callback() {
		?>
		<div class="wrap">
			<?php screen_icon(); ?>
			<form id="posts-filter" action="" method="get">
			<h2><?php _e( 'Needs approval', 'hris-travel' ); ?></h2>
			<?php $this->_list_table->display(); ?>
			</form>
		</div>
		<?php
	}

}
