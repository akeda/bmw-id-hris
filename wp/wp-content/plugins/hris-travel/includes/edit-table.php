<?php

/**
 * Any customizations need to be done in edit table.
 *
 * @package HRIS Travel
 * @since 0.1.0
 * @author Akeda Bagus <admin@gedex.web.id>
 */
class HRIS_Travel_Edit_Table implements HRIS_Travel_Component_Interface {

	/**
	 * Called by plugin's main-file in plugins_load action.
	 *
	 * @since 0.1.0
	 * @return void
	 */
	public function load() {
		// Customizes columns header.
		add_filter( 'manage_edit-' . HRIS_Travel_Post_Type::NAME . '_columns', array( $this, 'custom_columns_header' ) );

		// Remove actions on edit.php.
		// This needs to be on its own hook because of late priority of 'Approve' / 'Reject' buttons.
		add_filter( 'manage_edit-' . HRIS_Travel_Post_Type::NAME . '_columns', array( $this, 'remove_actions_col' ), 100 );

		// Fills customized columns.
		add_action( 'manage_' . HRIS_Travel_Post_Type::NAME . '_posts_custom_column', array( $this, 'custom_columns_row' ), 10, 2 );

		// Shows avatar in author col.
		add_filter( 'the_author', array( $this, 'add_avatar' ) );

		// Hides subsub top navigation above the table if user is not an administrator.
		// All (x) | Draft (x).
		add_filter( 'views_edit-' . HRIS_Travel_Post_Type::NAME, array( $this, 'hide_subsubsub' ) );

		// Shows travel entries by author only if an user doesn't have 'manage_hris_travel' cap.
		add_filter( 'pre_get_posts', array( $this, 'shows_travel_entries_by_author_only' ) );

		// Enqueues scripts and styles.
		add_action( 'load-edit.php', array( $this, 'scripts_and_styles' ) );

	}

	/**
	 * Customizes columns header of travel post type edit table.
	 *
	 * @since 0.1.0
	 * @filter manage_edit-{post_type}_columns
	 * @param array $columns Default columns header
	 * @return array Customized columns header
	 */
	public function custom_columns_header( $columns ) {
		// Columns to remove.
		$remove_columns = array( 'date', 'taxonomy-' . HRIS_Travel_Country_Taxonomy::NAME );

		// New and/or renamed columns.
		$new_columns = array(
			'author'             => __( 'Employee',       'hris-travel' ),
			'request_date'       => __( 'Request date',   'hris-travel' ),
			'start_date'         => __( 'Start date',     'hris-travel' ),
			'end_date'           => __( 'End date',       'hris-travel' ),
			'travel_from'        => __( 'From',           'hris-travel' ),
			'travel_destination' => __( 'To',             'hris-travel' ),
			'reason_for_travel'  => __( 'Reason',         'hris-travel' ),
			'title'              => __( 'Ref. No',        'hris-travel' ),
			'settlement'         => __( 'Settlement',     'hris-travel' ),
		);

		// No need to show author for 'hris_requestor' role because the columns shows his/her name all the time.
		if ( ! current_user_can( 'manage_hris_travel' ) ) {
			$remove_columns[] = 'author';
			unset( $new_columns['author'] );
		}

		foreach ( $remove_columns as $col ) {
			if ( isset( $columns[ $col ] ) ) {
				unset( $columns[ $col ] );
			}
		}

		foreach ( $new_columns as $k => $v ) {
			$columns[ $k ] = $v;
		}

		return $columns;
	}

	public function remove_actions_col( $columns ) {
		if ( isset( $columns['actions'] ) )
			unset( $columns['actions'] );

		return $columns;
	}

	/**
	 * Customizes row of travel post type edit table.
	 *
	 * @since 0.1.0
	 * @action manage_{post_type}_posts_custom_column
	 * @param string $column Column being passed
	 * @param int $post_id Post ID
	 * @return void
	 */
	public function custom_columns_row( $column, $post_id ) {
		$expected_columns = array(
			'request_date',
			'start_date',
			'end_date',
			'travel_from',
			'travel_destination',
			'reason_for_travel',
			'settlement',
		);

		$date_types = array( 'request_date', 'start_date', 'end_date' );

		// Functions defined by advanced-custom-fields plugin.
		if ( ! function_exists( 'the_field' ) || ! function_exists( 'get_field' ) ) {
			return;
		}

		if ( in_array( $column, $expected_columns ) ) {
			if ( in_array( $column, $date_types ) ) {
				echo esc_html( date( 'd/m/Y', strtotime( get_field( $column ) ) ) );
			} else if ( in_array( $column, array( 'travel_from', 'travel_destination' ) ) ) {

				$taxonomy_object = get_taxonomy( HRIS_Travel_Country_Taxonomy::NAME );
				$term_id         = get_field( $column );
				if ( is_array( $term_id ) && isset( $term_id[0] ) ) {
					$term_id = $term_id[0];
				}

				$term = get_term( $term_id, HRIS_Travel_Country_Taxonomy::NAME );
				if ( $term && ! is_wp_error( $term ) ) {
					$posts_in_term_qv = array();

					$posts_in_term_qv['post_type'] = HRIS_Travel_Post_Type::NAME;
					if ( $taxonomy_object->query_var ) {
						$posts_in_term_qv[ $taxonomy_object->query_var ] = $term->slug;
					} else {
						$posts_in_term_qv['taxonomy'] = HRIS_Travel_Country_Taxonomy::NAME;
						$posts_in_term_qv['term'] = $term->slug;
					}

					// printf( '<a href="%s">%s</a>',
					printf( '%s',
						// esc_url( add_query_arg( $posts_in_term_qv, 'edit.php' ) ),
						esc_html( sanitize_term_field( 'name', $term->name, $term->term_id, HRIS_Travel_Country_Taxonomy::NAME, 'display' ) )
					);
				}

			} else if ( 'print' === $column ) {
				if ( HRIS_Approval_Setting::APPROVED === get_post_status( $post_id ) ) {
					$tpl = '<a href="%s" class="button">%s</a>';
					echo sprintf( $tpl, hris_get_action_link_in_post( $post_id, 'settlement' ), __( 'Print', 'hris-medical' ) );
				}
			} else {
				the_field( $column );
			}
		}
	}

	/**
	 * Shows travel entries by current user, except adminstrator.
	 *
	 * @since 0.1.0
	 * @filter pre_get_posts
	 * @param object $query Instance of WP_Query
	 * @return object
	 */
	public function shows_travel_entries_by_author_only( $query ) {
		if ( ! is_admin() )
			return $query;

		if ( HRIS_Travel_Post_Type::NAME !== $query->get( 'post_type' ) )
			return $query;

		// We only need to filter travel records in edit base.
		$screen = get_current_screen();
		if ( 'edit' !== $screen->base ) {
			return $query;
		}

		// If a user is adminstrator  then no need to filter the records.
		if ( hris_check_user_role( 'administrator' ) ) {
			return $query;
		}

		// Filter records by current user.
		$user = wp_get_current_user();
		$query->set( 'author', $user->ID );

		return $query;
	}

	/**
	 * Adds avatar in adition of user name.
	 *
	 * @filter the_author
	 * @param string $name
	 * @return string
	 */
	public function add_avatar( $name ) {
		global $post;

		// Makes sure this is in admin page.
		if ( ! is_admin() )
			return $name;

		$screen = get_current_screen();

		$expected_screen_ids = array(
			'edit-' . HRIS_Travel_Post_Type::NAME,
			HRIS_Travel_Post_Type::NAME . '_page_' . HRIS_Travel_Approval::SLUG,
		);

		if ( ! in_array( $screen->id, $expected_screen_ids ) )
			return $name;

		return get_avatar( $post->post_author, 32 ) . ' ' . $name;
	}

	/**
	 * Hides subsubsub top nav "All (x) | Draft (x)" for non-adminstrator.
	 *
	 * @since 0.1.0
	 * @filter views_edit-{post_type}
	 * @param array $views
	 * @return array
	 */
	public function hide_subsubsub( $views ) {
		if ( ! hris_check_user_role( 'administrator' ) ) {
			return array();
		}
		return $views;
	}

	/**
	 * Enqueues scripts and styles on edit.php for this post type.
	 *
	 * @since 0.1.0
	 * @action
	 */
	public function scripts_and_styles() {
		if ( ! isset( $_GET['post_type'] ) )
			return;

		if ( HRIS_Travel_Post_Type::NAME !== $_GET['post_type'] )
			return;

		wp_enqueue_script( 'hris-travel-edit-table', HRIS_TRAVEL_URI . '/js/edit-table.js', array( 'jquery', ), HRIS_TRAVEL_VERSION );

		// Enqueue variables
		wp_localize_script(
			'hris-travel-edit-table',
			'HRIS_TRAVEL_EDIT_TABLE',
			array(
				'remove_title_anchor' => ! hris_check_user_role( 'administrator' ),
				'remove_checkbox'     => ! hris_check_user_role( 'administrator' ),
			)
		);
	}

}
