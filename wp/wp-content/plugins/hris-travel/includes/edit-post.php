<?php

/**
 * Any customizations need to be done when editing the post.
 *
 * @package HRIS Travel
 * @since 0.1.0
 * @author Akeda Bagus <admin@gedex.web.id>
 */
class HRIS_Travel_Edit_Post implements HRIS_Travel_Component_Interface {

	/**
	 * Called by plugin's main-file in plugins_load action.
	 *
	 * @since 0.1.0
	 * @return void
	 */
	public function load() {
		add_filter( 'post_updated_messages', array( $this, 'alter_updated_messages' ) );

		add_filter( 'ef_custom_status_list', array( $this, 'alter_post_status_list' ), 10, 2 );

		add_action( 'load-post-new.php', array( $this, 'scripts_and_styles' ) );
		add_action( 'load-post.php', array( $this, 'scripts_and_styles' ) );

		// Redirect for hris_requestor, otherwise check for custom actions.
		add_action( 'load-post.php', array( $this, 'maybe_redirect' ) );

		add_filter( 'acf/load_field', array( $this, 'hide_show_acf_approval' ), 10, 2 );
	}

	/**
	 * Alter messages when a post updated.
	 *
	 * @since 0.1.0
	 * @filter post_updated_messages
	 * @param array $messages Post updated messages.
	 * @return array Altered messages
	 */
	public function alter_updated_messages( $messages ) {
		global $post_type;

		if ( HRIS_Travel_Post_Type::NAME !== $post_type ) {
			return $messages;
		}

		// Messages
		$updated_message   = __( 'Travel entry updated.',   'hris-travel' );
		$approved_message  = __( 'Travel entry approved.',  'hris-travel' );
		$saved_message     = __( 'Travel entry saved.',     'hris-travel' );
		$submitted_message = __( 'Travel entry submitted.', 'hris-travel' );

		// Update 'Post updated. View post.' message.
		if ( isset( $messages['post'][1] ) ) {
			$messages['post'][1] = $updated_message;
		}

		// Update 'Post restored to revision from %s'.
		if ( isset( $messages['post'][5] ) ) {
			$messages['post'][5] = $updated_message;
		}

		// Update 'Post published. View post.' message.
		if ( isset( $messages['post'][6] ) ) {
			$messages['post'][6] = $approved_message;
		}

		// Update 'Post saved.' message.
		if ( isset( $messages['post'][7] ) ) {
			$messages['post'][7] = $saved_message;
		}

		// Update 'Post submitted.' message.
		if ( isset( $messages['post'][8] ) ) {
			$messages['post'][8] = $submitted_message;
		}

		// Update 'Post draft updated.' message.
		if ( isset( $messages['post'][10] ) ) {
			$messages['post'][10] = $updated_message;
		}

		return $messages;
	}

	public function alter_post_status_list( $custom_statuses, $post ) {

		if ( HRIS_Travel_Post_Type::NAME !== $post->post_type ) {
			return $custom_statuses;
		}

		if ( current_user_can( 'manage_hris_travel' ) ) {
			return $custom_statuses;
		}

		$removed_statuses = array( 'approved', 'director-review', 'supervisor-review' );
		foreach ( $custom_statuses as $key => $status ) {
			if ( in_array( $status->slug, $removed_statuses ) ) {
				unset( $custom_statuses[ $key ] );
			}
		}

		return $custom_statuses;
	}

	public function scripts_and_styles() {
		if ( ! isset( $_GET['post_type'] ) && ! isset( $_GET['post'] ) )
			return;

		if ( isset( $_GET['post_type'] ) && HRIS_Travel_Post_Type::NAME !== $_GET['post_type'] )
			return;

		if ( isset( $_GET['post'] ) ) {
			$post = get_post( $_GET['post'] );
			if ( ! $post )
				return;

			if ( HRIS_Travel_Post_Type::NAME !== $post->post_type )
				return;
		}

		wp_enqueue_script( 'hris-travel-edit-post', HRIS_TRAVEL_URI . '/js/edit-post.js', array( 'jquery', ), HRIS_TRAVEL_VERSION );
	}

	public function maybe_redirect() {
		if ( ! isset( $_GET['post'] ) )
			return;

		$post = get_post( $_GET['post'] );
		if ( HRIS_Travel_Post_Type::NAME !== $post->post_type )
			return;

		if ( ! hris_check_user_role( 'administrator' ) && ! hris_check_user_role( 'hris_approver' ) ) {
			wp_redirect( admin_url( 'edit.php?post_type=' . HRIS_Travel_Post_Type::NAME ) );
		}

		$approver_id = absint( hris_get_approver_by_status(
			HRIS_Travel_Post_Type::NAME,
			$post->post_author,
			$post->post_status
		) );

		$current_user = wp_get_current_user();

		$referer       = parse_url( wp_get_referer() );
		$from_approval = (
			isset( $referer['query'] )
			&&
			preg_match('/model=approval/gi', $referer['query'] )
		);

		if ( (int)$current_user->ID !== $approver_id ) {
			if ( $from_approval ) {
				wp_redirect( admin_url( 'edit.php?post_type=' . HRIS_Travel_Post_Type::NAME . '&page=' . HRIS_Travel_Approval::SLUG ) );
			} else {
				wp_redirect( admin_url( 'edit.php?post_type=' . HRIS_Travel_Post_Type::NAME ) );
			}
		}
	}

	public function hide_show_acf_approval( $field ) {
		$screen      = get_current_screen();
		$post_obj    = get_post();
		$post_status = $post_obj->post_status;

		if ( HRIS_Travel_Post_Type::NAME === $screen->post_type && 'edit' === $screen->parent_base ) {
			$excluded_fields = array();
			switch ( $post_status ) {
				case 'hr-review':
					$excluded_fields = array( 'supervisor_review_status', 'director_review_status' );
					break;
				case 'supervisor-review':
					$excluded_fields = array( 'hr_review_status', 'director_review_status' );
					break;
				case 'director-review':
					$excluded_fields = array( 'hr_review_status', 'supervisor_review_status' );
					break;
				case 'approved':
					$excluded_fields = array( 'hr_review_status', 'supervisor_review_status' );
					break;
				case 'draft':
				case 'rejected':
				default:
					$excluded_fields = array( 'hr_review_status', 'supervisor_review_status', 'director_review_status' );
					break;
			}

			if ( in_array( $field['name'], $excluded_fields ) ) {
				return false;
			}
		}
		return $field;
	}
}
