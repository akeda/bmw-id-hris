<?php

class HRIS_Travel_Country_Taxonomy implements HRIS_Travel_Component_Interface {
	const NAME = 'hris_travel_country';

	public function load() {
		// Register custom post type on the 'init' hook.
		add_action( 'init', array( $this, 'register_taxonomy' ) );
	}

	public function register_taxonomy() {
		$args = array(
			'public'            => true,
			'show_ui'           => true,
			'show_in_nav_menus' => true,
			'show_tagcloud'     => false,
			'show_admin_column' => true,
			'hierarchical'      => true,
			'query_var'         => false,

			'capabilities' => array(
				'manage_terms' => 'manage_hris_travel',
				'edit_terms'   => 'edit_hris_travel_countries',
				'delete_terms' => 'manage_hris_travel',
				'assign_terms' => 'edit_hris_travel_countries',
			),

			'rewrite' => false,

			/* Labels used when displaying taxonomy and terms. */
			'labels' => array(
				'name'                       => __( 'Travel Countries',                           'hris-travel' ),
				'singular_name'              => __( 'Travel Country',                             'hris-travel' ),
				'menu_name'                  => __( 'Travel Countries',                           'hris-travel' ),
				'name_admin_bar'             => __( 'Travel Country',                             'hris-travel' ),
				'search_items'               => __( 'Search Travel Countries',                    'hris-travel' ),
				'popular_items'              => __( 'Popular Travel Countries',                   'hris-travel' ),
				'all_items'                  => __( 'All Travel Countries',                       'hris-travel' ),
				'edit_item'                  => __( 'Edit Travel Country',                        'hris-travel' ),
				'view_item'                  => __( 'View Travel Country',                        'hris-travel' ),
				'update_item'                => __( 'Update Travel Country',                      'hris-travel' ),
				'add_new_item'               => __( 'Add New Travel Country',                     'hris-travel' ),
				'new_item_name'              => __( 'New Travel Countries Name',                  'hris-travel' ),
				'separate_items_with_commas' => __( 'Separate travel countries with commas',      'hris-travel' ),
				'add_or_remove_items'        => __( 'Add or remove travel countries',             'hris-travel' ),
				'choose_from_most_used'      => __( 'Choose from the most used travel countries', 'hris-travel' ),
			)
		);

		// Register the taxonomy.
		register_taxonomy( self::NAME, array( HRIS_Travel_Post_Type::NAME ), $args );
	}
}
