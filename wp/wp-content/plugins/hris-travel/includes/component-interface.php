<?php

/**
 * Interface that plugin-component's class MUST implement.
 *
 * @package HRIS Travel
 * @since 0.1.0
 * @author Akeda Bagus <admin@gedex.web.id>
 */
interface HRIS_Travel_Component_Interface {
	public function load();
}
