module.exports = function(grunt) {

	require('matchdep').filterDev('grunt-*').forEach( grunt.loadNpmTasks );

	// Project configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		sass: {
			hris_admin_style: {
				options: {
					style: 'compact',
					lineNumbers: false,
				},
				expand: true,
				cwd: '.',
				dest: '.',
				ext: '.css',
				src: [
					'hris-admin-style.scss'
				]
			}
		},
		watch: {
			sass: {
				files: ['hris-admin-style.scss', ],
				tasks: ['sass:hris_admin_style']
			}
		}

	});

	// Default task(s).
	grunt.registerTask('default', ['sass']);

};
