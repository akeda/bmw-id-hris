<?php
/**
 * Plugin Name: HRIS Admin Style
 * Plugin URI: http://gedex.web.id
 * Description: Admin style for HRIS.
 * Author: Akeda Bagus
 * Author URI: http://gedex.web.id/
 * Version: 0.1.0
 *
 * Admin style for HRIS.
 *
 * @license MIT License.
 */

/**
 * Enqueue admin scripts to override default admin style.
 *
 * @since 0.1.0
 * @action admin_enqueue_scripts
 * @return void
 */
function hris_admin_style_admin_scripts() {
	wp_enqueue_style( 'hris_admin_style', plugin_dir_url( __FILE__ ) . '/hris-admin-style.css' );
}
add_action( 'admin_enqueue_scripts', 'hris_admin_style_admin_scripts' );
