<?php

/**
 * Lineup Settings page
 *
 * @package Lineup
 * @subpackage Admin
 * @since 0.1
 * @author X-Team
 * @author Dzikri Aziz <kucrut@x-team.com>
 * @copyright Copyright (c) 2013, X-Team
 */
class XTeam_Lineup_Admin {

	/**
	 * Settings page hook
	 *
	 * @since 0.1
	 * @access private
	 */
	private static $_settings_page_hook;

	/**
	 * Available post types
	 *
	 * @since 0.1
	 * @access private
	 */
	private static $_post_types;


	/**
	 * Register settings page menu
	 *
	 * Settings page will be available under Settings &raquo; Lineup
	 *
	 * @since 0.1
	 * @access private
	 * @action admin_menu
	 *
	 * @return void
	 */
	public static function _register_menu() {
		self::$_settings_page_hook = add_options_page(
			__('Lineup Settings', 'xteam-lineup'), // Page title
			__('Lineup', 'xteam-lineup'), // Menu title
			apply_filters( 'xteam_lineup_settings_page_capability', 'manage_options' ), // capability
			'lineup-settings', // menu slug
			array( __CLASS__, '_settings_page' ) // callback function
		);
	}


	/**
	 * Settings page content
	 *
	 * @since 0.1
	 * @access private
	 */
	public static function _settings_page() {
		self::$_post_types = XTeam_Lineup::get_post_types();
		?>
			<div class="wrap">
				<?php screen_icon(); ?>
				<h2><?php esc_html_e('Lineup Settings', 'xteam-lineup') ?></h2>

				<?php if ( empty(self::$_post_types) ) : ?>
					<p><?php esc_html_e('No post types found', 'xteam-lineup') ?></p>
				<?php else : ?>
					<?php self::_settings_form() ?>
				<?php endif; ?>
			</div>
		<?php
	}


	/**
	 * Settings form
	 *
	 * @since 0.1
	 * @access private
	 */
	private static function _settings_form() {
		$settings = XTeam_Lineup::get( 'settings' );
		$fields   = array(
			'featured' => __('Featured lineup', 'xteam-lineup'),
			'terms'    => __('Taxonomy terms lineup', 'xteam-lineup'),
		);
		?>
			<form action="options.php" method="post" id="xteam-lineup-settings">
				<?php settings_fields( XTeam_Lineup::OPTION_KEY ); ?>
				<input type="hidden" name="<?php echo esc_attr( $settings->get_field_name( array( 'settings-part' ) ) ) ?>" value="options" />

				<table class="form-table">
					<tbody>
						<?php foreach ( self::$_post_types as $post_type => $post_type_object ) : ?>
							<tr valign="top">
								<th scope="row"><?php echo esc_html( $post_type_object->labels->name ) ?></th>
								<td>
									<?php foreach ( $fields as $key => $label ) : ?>
										<?php $field_keys = array( 'options', 'post_types', $post_type, $key ); ?>
										<label for="<?php echo esc_attr( $settings->get_field_id( $field_keys ) ) ?>">
											<?php printf(
												'<input type="checkbox" id="%s" name="%s" value="1" %s/>',
												esc_attr( $settings->get_field_id( $field_keys ) ),
												esc_attr( $settings->get_field_name( $field_keys ) ),
												checked( (bool) $settings->get_field_value( $field_keys ), true, false )
											) ?>
											<?php echo esc_html( $label ) ?>
										</label><br />
									<?php endforeach; ?>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>

				<?php submit_button() ?>
			</form>
		<?php
	}
}
