/* global jQuery, postboxes, findPosts */
(function($) {
	'use strict';

	var $form     = $('form.xteam-lineup-manager');
	var $target   = null;
	var postIDs   = null;
	var $finder   = $('#find-posts');
	var $finderResult  = $('#find-posts-response');
	var $finderSearch  = $('#find-posts-search');
	var $finderInput   = $('#find-posts-input');
	var $finderSpinner = $finder.find('span.spinner');
	var $finderSubmit  = $('#find-posts-submit')

	// Initilize metaboxes
	postboxes.add_postbox_toggles( pagenow );

	// Disable dropping a metabox onto other holders
	$('div.meta-box-sortables', $form).each(function() {
		$(this).sortable( 'option', 'connectWith', this );
	});

	// Make posts inside metboxes sortable.
	$('ul.posts-list', $form).sortable({
		handle: '.handle'
	});

	$('a.nav-tab').on('click', function(e, data) {
		e.preventDefault();

		var $tab = $(this).blur();
		if ( $tab.is('.nav-tab-active') ) {
			return;
		}

		var tabURL  = $tab.attr('href');
		var $target = $( tabURL );

		if ( $target.length ) {
			$tab.addClass('nav-tab-active')
				.siblings().removeClass('nav-tab-active');

			$target.show()
				.siblings().hide();
		}

		if ( data === undefined || data.save ) {
			$.post(
				ajaxurl,
				{
					'action': 'xteam_lineup_save_last_tab',
					'ajax_nonce': xteamLineup.ajaxNonce,
					'page': pagenow,
					'tab': tabURL
				}
			);
		}
	})
		.filter('.last-active')
			.removeClass('last-active')
			.triggerHandler('click', {save: false} );


	$('body')
		.on('click', 'a.add-new-h2', function(e) { // Add new featured lineup
			e.preventDefault();

			var $button = $(this).blur();
			var $group  = $button.closest('div.metabox-holder');
			var $boxes  = $group.find('div.postbox');
			var names   = [];

			$boxes.not('.hidden').each(function() {
				names.push( $(this).find('h3.hndle').text() );
			});

			var newName = prompt( xteamLineup.texts.addNewFeaturedTitle, '' );
			newName = newName.replace( /\W/g, '' ).toLowerCase();
			if ( newName == null || newName == '' ) {
				alert( xteamLineup.texts.onlyAlphanumeric );
				return false;
			}

			if ( $.inArray( newName, names ) > -1 ) {
				alert( xteamLineup.texts.featuredNameIsUsed );
				return false;
			}

			var $new  = $boxes.first().clone(true).addClass('hidden');
			var newID = $new.attr('id').replace( /(\w+)$/, newName );

			$new.attr('id', newID);
			$new.attr('data-lineup-id', newName);
			$new.find('h3.hndle span').text( newName );
			$new.find('ul.posts-list').remove();
			$new.appendTo( $group.find('div.meta-box-sortables') ).fadeIn(function() {
				$new.removeClass('hidden').removeAttr('style');
			});
		})
		.on('click', 'a.remove-lineup', function(e) { // Remove featured lineup
			e.preventDefault();

			var sure = confirm( xteamLineup.texts.confrimRemove );
			if ( false === sure )
				return false;

			var $el  = $(this);
			var $box = $el.closest('div.postbox');

			if ( $box.siblings().length ) {
				$box.fadeOut(function() {
					$box.remove();
				});
			}
			// If this is the only metabox, just hide it, we need it as a 'template'
			else {
				$box.fadeOut(function() {
					$box.find('ul.posts-list').remove();
					$box.addClass('hidden').removeAttr('style');
				});
			}
		})
		.on('click', 'a.add-post', function(e) { // Add post to lineup
			e.preventDefault();

			// Collect already added post IDs to exclude from our search query
			postIDs = [];
			$target = $(this).closest('div.inside');
			$('input.post-id', $target).each(function() {
				if ( this.value !== '' )
					postIDs.push( this.value );
			});

			// Cleanup old search
			$finderInput.val('');
			$finderResult.children().remove();
			$finderSubmit.triggerHandler('setState');

			// Open the finder box
			findPosts.open();
		})
		.on('click', 'a.edr-post', function(e) { // Add/remove/enable/disable post to/from lineup
			e.preventDefault();
			e.stopPropagation();

			var $el   = $(e.target);
			var $item = $el.closest('li.item');

			// Inside finder: add post to lineup
			if ( $el.is('.add') ) {
				$item.addClass('selected')
					.siblings().removeClass('selected');

				$('#find-posts-submit', $finder).click();
			}
			else {
				// Inside featured lineup: remove post
				if ( $el.is('.remove') ) {
					$item.fadeOut(function() {
						$item.remove();
					});
				}
				// Inside term lineup: enable/disable
				else {
					var $box         = $item.closest('div.postbox');
					var doEnable     = $item.is('.disabled');
					var $lastEnabled = $item.siblings().not('.disabled').last();

					$item.toggleClass('disabled');
					$.post(
						ajaxurl,
						{
							'action'     : 'xteam_lineup_enable_disable_post',
							'ajax_nonce' : xteamLineup.ajaxNonce,
							'enable'     : doEnable + false,
							'post_id'    : $item.find('input').val(),
							'term_slug'  : $box.data('lineup-id'),
							'success'    : function() {
								if ( $lastEnabled.length ) {
									$item.insertAfter( $lastEnabled );
								}
								else if ( doEnable ) {
									$item.prependTo( $item.parent() );
								}
							}
						}
					);
				}
			}
		})
		.on('click', '#find-posts-response li.item', function() {
			var $item = $(this).toggleClass('selected');
			$finderSubmit.triggerHandler('setState');
		});


	// Override default post finder action
	$finderSearch.unbind( 'click', findPosts.send );

	findPosts.send = function() {
		$finderSpinner.show();
		$finderResult.children().remove();
		$finderSubmit.prop('disabled', true);

		var args = {
			'post_type'  : $('#xteam_lineup-current-post-type').val(),
			'group_type' : 'featured',
			'group_id'   : '__featured__',
			'lineup_id'  : $target.closest('div.postbox').data('lineup-id')
		};

		var extraArgs  = {
			'post_type' : $('#find-posts-type').val() || 'any'
		};
		var searchTerm = $finderInput.val();
		if ( '' !== searchTerm ) {
			extraArgs.s = searchTerm
		}

		if ( postIDs.length ) {
			args.post__not_in = postIDs;
		}

		$.post(
			ajaxurl,
			{
				'action'     : 'xteam_lineup_get_items',
				'ajax_nonce' : xteamLineup.ajaxNonce,
				'args'       : args,
				'extraArgs'  : extraArgs
			},
			function( result ) {
				$finderSpinner.hide();
				$finderResult.append( result );
			}
		);
	};

	$finderSubmit.on('click', function(e) {
		e.preventDefault();

		var $newItems = $finderResult.find('li.selected');
		if ( !$newItems.length )
			return false;

		$newItems.each(function() {
			postIDs.push( $(this).find('input').val() );
		})

		var $list = $target.children('ul.posts-list');
		if ( $list.length ) {
			$list.append( $newItems.removeClass('selected') )
				.sortable( 'refresh' );
		}
		else {
			$('<ul />', {'class': 'posts-list'} )
				.append( $newItems.removeClass('selected') )
				.prependTo( $target )
				.sortable( {handle: '.handle'} );
		}

		$finderSubmit.triggerHandler('setState');
	})
		.on('setState', function() {
			var disable = $finderResult.find('li.selected').length;
			$finderSubmit.prop('disabled', !disable);
		});
}(jQuery));
