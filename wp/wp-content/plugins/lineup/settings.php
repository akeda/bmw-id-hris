<?php

/**
 * Plugin Settings helper
 *
 * @package Lineup
 * @subpackage Settings
 * @since 0.1
 * @author X-Team
 * @author Dzikri Aziz <kucrut@x-team.com>
 * @copyright Copyright (c) 2013, X-Team
 *
 */
class XTeam_Plugin_Settings {

	/**
	 * Object properties, holds the option name and value
	 *
	 * @since 0.1
	 * @access private
	 * @var array
	 */
	private $_data = array();


	/**
	 * Constructor
	 *
	 * @since 0.1
	 * @access public
	 *
	 * @param string $option_name   Option name
	 * @param mixed  $default_value Default value if the option doesn't exist
	 *                              in the DB yet
	 *
	 * @return void
	 */
	public function __construct( $option_name, $default_value = false ) {
		$this->_data['key']   = $option_name;
		$this->_data['value'] = get_option( $option_name ) ?: $default_value;
	}


	/**
	 * Property getter
	 *
	 * @since 0.1
	 *
	 * @param string $name Property name
	 *
	 * @return mixed NULL if property doesn't exist
	 */
	public function __get( $name ) {
		if ( isset( $this->_data[ $name ] ) ) {
			return $this->_data[ $name ];
		}

		return null;
	}


	/**
	 * Get settings field id attribute
	 *
	 * @since 0.1
	 * @access public
	 *
	 * @param array $keys Field keys to construct the ID
	 */
	public function get_field_id( array $keys ) {
		array_unshift( $keys, $this->_data['key'] );
		$format = implode( '-', $keys );

		return $this->get_field_attr( $format, $keys );
	}


	/**
	 * Get settings field name attribute
	 *
	 * @since 0.1
	 * @access public
	 *
	 * @param array $keys Field keys to construct the name
	 */
	public function get_field_name( array $keys ) {
		$format = '%s';
		foreach ( $keys as $key )
			$format .= '[%s]';
		array_unshift( $keys, $this->_data['key'] );

		return $this->get_field_attr( $format, $keys );
	}


	/**
	 * Get settings field ID attribute
	 *
	 * @since 0.1
	 * @access public
	 *
	 * @param string $format Attribute format
	 * @param array  $keys Field keys to construct the ID
	 */
	public function get_field_attr( $format, array $keys ) {
		return call_user_func_array(
			'sprintf',
			array_merge(
				array( $format ),
				$keys
			)
		);
	}


	/**
	 * Get settings field value
	 *
	 * This method will loop trough each key from $keys and find their value
	 * from $value
	 *
	 * @since 0.1
	 * @access public
	 *
	 * @param array $keys  Field keys to construct the ID
	 * @param mixed $value If null, the instance's option value will be used
	 *
	 * @return mixed
	 */
	public function get_field_value( array $keys, $value = null ) {
		if ( is_null( $value ) ) {
			$value = $this->_data['value'];
		}

		foreach ( $keys as $index => $key ) {
			unset( $keys[ $index ] );

			if ( ! empty($value[ $key ] ) ) {
				if ( is_array( $value[ $key ] ) && ! empty( $keys ) ) {
					return $this->get_field_value( $keys, $value[ $key ] );
				}
				else {
					return $value[ $key ];
				}
			}
			else {
				return false;
			}
		}
	}

	/**
	 * Cleanup empty array recursively
	 *
	 * @since 0.1
	 * @access public
	 *
	 * @param array $haystack Array to cleanup
	 *
	 * @return array Clean array
	 */
	public function remove_empty( array $haystack ) {
		foreach ( $haystack as $key => $value ) {
			if ( is_array( $value ) ) {
				$haystack[ $key ] = $this->remove_empty( $haystack[ $key ] );
			}

			if ( empty( $haystack[ $key ] ) ) {
				unset( $haystack[ $key ] );
			}
		}

		return $haystack;
	}
}
