<?php

/**
 * Plugin name: Lineup
 * Description: Create and manage posts lineups.
 * Plugin URI: http://x-team.com/wordpress/
 * Author: X-Team
 * Author URI: http://x-team.com/
 * Version: 0.3
 * text-domain: xteam-lineup
 *
 * Lineup management plugin, for custom ordering posts within taxonomy terms,
 * and custom featured lineups.
 *
 * @package Lineup
 * @subpackage Plugin
 * @since 0.1
 * @author X-Team
 * @author Dzikri Aziz <kucrut@x-team.com>
 * @copyright Copyright (c) 2013, X-Team
 */
final class XTeam_Lineup {

	/**
	 * Plugin version.
	 *
	 * @since 0.1
	 * @var string
	 */
	const VERSION = '0.3';

	/**
	 * Option name.
	 *
	 * @since 0.1
	 * @var string
	 */
	const OPTION_KEY = 'xteam_lineup';

	/**
	 * Plugin data.
	 *
	 * @access private
	 * @since 0.1
	 * @var array
	 */
	private static $_data = array(
		'post_types_args'    => array(
			'public' => true,
		),
		'disabed_post_types' => array(
			'attachment' => true,
			'page'       => true,
		),
	);


	/**
	 * Plugin instantiation
	 *
	 * @since 0.1
	 * @access public
	 * @action plugins_loaded
	 *
	 * @return void
	 */
	public static function setup() {
		// Store plugin directory URL for easy access
		self::$_data['plugin_dir_url'] = plugin_dir_url( __FILE__ );

		// Initialize plugin settings
		if ( ! class_exists( 'XTeam_Plugin_Settings' ) ) {
			require_once trailingslashit( dirname( __FILE__ ) ) . 'settings.php';
		}
		self::$_data['settings'] = new XTeam_Plugin_Settings( self::OPTION_KEY, array() );

		// Register plugin option name
		add_action( 'admin_init', array( __CLASS__, '_register_option' ) );

		// Initialize settings page
		self::_setup_settings_page();

		// Initialize Lineup Manager pages
		self::_setup_manager_page();

		// Initialize global hooks
		require_once trailingslashit( dirname( __FILE__ ) ) . 'hooks.php';
		XTeam_Lineup_Hooks::setup();
	}


	/**
	 * Class properties getter
	 *
	 * @since 0.1
	 * @param string $name Property name
	 * @access public
	 *
	 * @return mixed NULL if property doesn't exists.
	 */
	public static function get( $name ) {
		if ( isset( self::$_data[ $name ] ) ) {
			return self::$_data[ $name ];
		}

		return null;
	}


	/**
	 * Register plugin option name
	 *
	 * @since 0.1
	 * @access public
	 * @action admin_init
	 *
	 * @return void
	 */
	public static function _register_option() {
		register_setting( self::OPTION_KEY, self::OPTION_KEY, array( __CLASS__, '_save_settings' ) );
	}


	/**
	 * Save plugin settings
	 *
	 * Everything is saved in one database row
	 *
	 * @since 0.1
	 * @param array  $values  New setting values
	 * @access public
	 *
	 * @return array New setting values merged with the old ones
	 */
	public static function _save_settings( $values ) {
		// Get settings part (options or lineups)
		$settings_part = $values['settings-part'];

		// Get current setting values
		$all_values = self::get( 'settings' )->value;

		// Remove empty values from new setting values
		if ( ! empty( $values[ $settings_part ] ) ) {
			$new_values = $values[ $settings_part ];
		}
		else {
			$new_values = null;
		}

		// Save lineups
		if ( 'lineups' === $settings_part && ! empty( $new_values ) ) {
			$post_type = $values['current-post-type'];
			$all_values['lineups'][ $post_type ] = $new_values[ $post_type ];

			// Save lineup history
			$all_values['history'][ $post_type ] = array(
				'timestamp' => current_time( 'timestamp' ),
				'user_id'   => wp_get_current_user()->ID,
			);
		}
		else {
			// Save plugin settings
			$all_values[ $settings_part ] = $new_values;
		}

		// Cleanup empty values
		$all_values = self::get( 'settings' )->remove_empty( $all_values );

		return $all_values;
	}


	/**
	 * Register settings page menu
	 *
	 * @since 0.1
	 * @access public
	 * @action admin_menu
	 *
	 * @return void
	 */
	public static function _setup_settings_page() {
		require_once trailingslashit( dirname( __FILE__ ) ) . 'admin.php';
		add_action( 'admin_menu', array( 'XTeam_Lineup_Admin', '_register_menu' ) );
	}


	/**
	 * Initialize Lineup Manager pages
	 *
	 * Lineup manager pages for each post type will only be initialized if
	 * they're enabled from the plugin settings page
	 *
	 * @since 0.1
	 * @access public
	 *
	 * @return void
	 */
	public static function _setup_manager_page() {
		$post_types = self::get( 'settings' )->get_field_value( array( 'options', 'post_types' ) );
		if ( ! empty( $post_types ) ) {
			require_once trailingslashit( dirname( __FILE__ ) ) . 'manager.php';
			XTeam_Lineup_Manager::setup();
		}
	}


	/**
	 * Get disabled post types
	 *
	 * Some post types could be blacklisted from the lineups, the defaults are
	 * 'attachment' and 'page'.
	 *
	 * @since 0.1
	 * @access public
	 * @uses apply_filters() Calls 'xteam_lineup_disabled_post_types'.
	 *
	 * @return array Associative array of disabled post types
	 */
	protected static function get_disabed_post_types() {
		$disabled_post_types = apply_filters(
			'xteam_lineup_disabled_post_types',
			self::$_data['disabed_post_types']
		);

		return array_filter( $disabled_post_types );
	}


	/**
	 * Get available post types for lineups
	 *
	 * This method will return all public post types, excluding the ones
	 * disabled by the 'xteam_lineup_disabled_post_types' filter.
	 *
	 * @since 0.1
	 * @access public
	 *
	 * @return object Post type objects
	 */
	public static function get_post_types() {
		$post_types = get_post_types( self::$_data['post_types_args'], OBJECT );
		foreach ( array_keys( self::get_disabed_post_types() ) as $post_type_name ) {
			unset( $post_types[ $post_type_name ] );
		}

		return $post_types;
	}


	/**
	 * Get post type label
	 *
	 * @since 0.2
	 * @acess public
	 *
	 * @param string $name     Post type name
	 * @param bool   $singular Whether to get the singular or plural label
	 *
	 * @return string Post type label
	 */
	public static function get_post_type_label( $name, $singular = true ) {
		static $post_types = array();

		$key = $singular ? 'singular' : 'plural';

		if ( isset( $post_types[ $name ] ) ) {
			return $post_types[ $name ][ $key ];
		}

		$post_type_object = get_post_type_object( $name );
		if ( empty( $post_type_object ) || is_wp_error( $post_type_object ) ) {
			return false;
		}

		$post_types[ $name ] = array(
			'plural'   => $post_type_object->labels->name,
			'singular' => $post_type_object->labels->singular_name,
		);

		return $post_types[ $name ][ $key ];
	}


	/**
	 * Get featured lineup query
	 *
	 * @since 0.1
	 * @access public
	 *
	 * @param string $post_type Post type name
	 * @param string $lineup_id Featured Lineup ID.
	 * @param array  $wp_query_args Additional WP_Query args
	 *
	 * @return object|false WP_Query object or FALSE on failure
	 */
	public static function get_featured_lineup( $post_type, $lineup_id, $wp_query_args = array() ) {
		$post_ids = self::get_featured_lineup_post_ids( $post_type, $lineup_id );
		if ( false === $post_ids ) {
			return false;
		}

		$query = new WP_Query(
			wp_parse_args(
				$wp_query_args,
				array(
					'post_type'      => 'any',
					'posts_per_page' => -1,
					'post__in'       => $post_ids,
					'orderby'        => 'post__in',
					'order'          => 'ASC',
				)
			)
		);

		return $query;
	}


	/**
	 * Get featured lineup post IDs
	 *
	 * @since 0.1
	 * @access public
	 *
	 * @param string $post_type Post type name
	 * @param string $lineup_id Featured Lineup ID.
	 *
	 * @return array|false Array of posts IDs or FALSE on failure
	 */
	public static function get_featured_lineup_post_ids( $post_type, $lineup_id ) {
		$lineup = self::$_data['settings']->get_field_value(
			array(
				'lineups',
				$post_type,
				'__featured__',
				$lineup_id,
			)
		);

		if ( ! empty( $lineup ) && is_array( $lineup ) ) {
			return $lineup;
		}
		else {
			return false;
		}
	}


	/**
	 * Get term lineup query
	 *
	 * @since 0.1
	 * @access public
	 *
	 * @param string $post_type Post type name
	 * @param string $taxonomy  Taxonomy name
	 * @param string $term_slug Taxonomy term slug
	 * @param array  $wp_query_args Additional WP_Query args
	 *
	 * @return object|false WP_Query object or FALSE on failure
	 */
	public static function get_term_lineup( $post_type, $taxonomy, $term_slug, $wp_query_args = array() ) {
		// If the manager is not loaded, then there's no term lineup yet. So, bail out.
		if ( ! class_exists( 'XTeam_Lineup_Manager' ) ) {
			return false;
		}

		// We need to exclude the disabled posts
		$meta_query = array(
			'key'     => sprintf( 'xteam_lineup_disabled_in_%s', $term_slug ),
			'value'   => '1', // Bug work-around. See http://codex.wordpress.org/Function_Reference/WP_Query#Custom_Field_Parameters
			'compare' => 'NOT EXISTS',
		);

		if ( ! empty($wp_query_args['meta_query']) ) {
			$wp_query_args['meta_query'][] = $meta_query;
		}
		else {
			$wp_query_args['meta_query'] = array( $meta_query );
		}

		$query = XTeam_Lineup_Manager::get_items(
			array(
				'post_type'  => $post_type,
				'group_type' => 'terms',
				'group_id'   => $taxonomy,
				'term_slug'  => $term_slug,
			),
			$wp_query_args
		);

		return $query;
	}
}

add_action( 'plugins_loaded', array( 'XTeam_Lineup', 'setup' ) );
