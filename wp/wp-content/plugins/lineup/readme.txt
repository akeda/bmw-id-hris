=== Lineup ===
Contributors: kucrut, x-team
Donate link: http://x-team.com/wordpress/
Tags: posts, taxonomy terms, featured posts
Requires at least: 3.6
Tested up to: 3.6.1
Stable tag: 0.3
License: GPLv2

Lineup management plugin, for custom ordering posts within taxonomy terms, and custom featured lineups.

== Description ==

This plugin will help you create custom order of posts, based on categories, tags, custom taxonomy terms, and also cutom featured areas.

Here's how you pull the lineups:

= Featured Lineup =
```
$post_type = 'post';
$featured_lineup_id  = 'myid';
$other_wp_query_args = array();

$lineup = XTeam_Lineup::get_featured_lineup( $post_type, $featured_lineup_id, $other_wp_query_args );
if ( $lineup && $lineup->have_posts() ) {
	while ( $lineup->have_posts() ) {
		$lineup->the_post();
		printf( '<h2>%s</h2>', get_the_title() );
	}
}
wp_reset_postdata();
```

= Categories/Tags/Terms Lineup =
```
$post_type = 'post';
$taxonomy  = 'category';
$term_slug = 'uncategorized';
$other_wp_query_args = array();

$lineup = XTeam_Lineup::get_term_lineup( $post_type, $taxonomy, $term_slug, $other_wp_query_args );
if ( $lineup && $lineup->have_posts() ) {
	while ( $lineup->have_posts() ) {
		$lineup->the_post();
		printf( '<h2>%s</h2>', get_the_title() );
	}
}
wp_reset_postdata();
```

== Installation ==

1. Use standard WordPress plugin installation or upload the `lineup` directory to your `wp-content/plugins` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Go to Settings &raquo; Lineup and enable the lineup for each post types
4. If you enabled lineup for "Posts", then you can go to Posts &raquo; Lineup to manage the lineups

== Frequently Asked Questions ==

= Why don't I see my categories/tags meta boxes inside lineup manager page? =

Either you haven't created categories/tags, or there are no posts assigned to them.

= Does this plugin provide a widget? =

No, not this time. You will have to run [custom queries](http://codex.wordpress.org/Function_Reference/WP_Query) in your theme to pull the lineup posts.

== Screenshots ==

1. Featured Lineups Manager
2. Category Lineups Manager

== Changelog ==

= 0.3 =
* Supports multiple post types in each lineup

= 0.2 =
* Supports all searchable post types in featured lineups

= 0.1 =
* First release
