<?php

/**
 * Lineup Hooks
 *
 * @package Lineup
 * @subpackage Hooks
 * @since 0.1
 * @author X-Team
 * @author Dzikri Aziz <kucrut@x-team.com>
 * @copyright Copyright (c) 2013, X-Team
 */
class XTeam_Lineup_Hooks {

	/**
	 * Add actions and filters
	 *
	 * @since 0.1
	 * @access public
	 * @return void
	 */
	public static function setup() {
		add_filter( 'get_terms', array( __CLASS__, '_reorder_terms' ), 10, 3 );
		add_filter( 'posts_orderby', array( __CLASS__, '_filter_posts_orderby' ), 10, 2 );
	}


	/**
	 * Filter terms order
	 *
	 * We need to filter the terms order from get_terms() if:
	 * 1. 'orderby' is set to '_xteam_lineup'
	 * 2. '_xteam_lineup' arg is set and it contains:
	 *    - post_type
	 *
	 * @param   array  $_terms     Term objects return by get_terms()
	 * @param   array  $taxonomies Taxonomy names passed to get_terms()
	 * @param   array  $args       Arguments passed to get_terms()
	 * @wp_hook filter get_terms
	 *
	 * @return array $terms Sorted terms based on their orders in the lineup config
	 */
	public static function _reorder_terms( $_terms, $taxonomies, $args ) {
		if ( empty( $_terms ) ) {
			return $_terms;
		}

		// Check orderby
		if ( empty( $args['orderby'] ) || $args['orderby'] !== '_xteam_lineup' ) {
			return $_terms;
		}

		if ( empty( $args['_xteam_lineup']['post_type'] ) ) {
			return $_terms;
		}

		$post_type = $args['_xteam_lineup']['post_type'];
		$groups    = XTeam_Lineup::get( 'settings' )->get_field_value( array( 'lineups', $post_type ) );

		if ( empty( $groups ) ) {
			return $_terms;
		}

		$terms = array();
		foreach ( $taxonomies as $taxonomy ) {
			if ( ! empty($groups[ $taxonomy ]) ) {
				$terms = array_merge(
					array_fill_keys( array_keys( $groups[ $taxonomy ] ), false ),
					$terms
				);
			}
		}

		if ( ! empty($terms) ) {
			foreach ( $_terms as $index => $term_object ) {
				$terms[ $term_object->slug ] = $term_object;
				unset( $_terms[ $index ] );
			}
		}

		$terms = array_merge( $terms, $_terms );

		return array_values( array_filter( $terms ) );
	}


	/**
	 * Filter posts order
	 *
	 * We need to filter the posts order if:
	 * 1. 'orderby' is set to '_xteam_lineup'
	 * 2. '_xteam_lineup' query arg is set and it contains:
	 *    - post_type
	 *    - taxonomy
	 *    - term_slug
	 *
	 * @param string $orderby
	 * @param object $query
	 *
	 * @return string Modified ORDER BY if all conditions are met
	 * @filter posts_orderby
	 */
	public static function _filter_posts_orderby( $orderby, $query ) {
		$qv = $query->query_vars;

		// Check orderby
		if ( empty($qv['orderby']) || $qv['orderby'] !== '_xteam_lineup' ) {
			return $orderby;
		}

		$keys = array( 'post_type', 'taxonomy', 'term_slug' );
		foreach ( $keys as $key ) {
			if ( empty( $qv['_xteam_lineup'][ $key ] ) ) {
				return $orderby;
			}
		}

		$keys = array_merge(
			array( 'lineups' ),
			array_values( $qv['_xteam_lineup'] )
		);
		$lineup_post_ids = XTeam_Lineup::get( 'settings' )->get_field_value( $keys );
		if ( empty( $lineup_post_ids ) ) {
			return $orderby;
		}

		global $wpdb;
		$posts_order = implode( ',', $lineup_post_ids );
		$orderby     = "FIELD( {$wpdb->posts}.ID, {$posts_order} ) ASC, post_date DESC";

		return $orderby;
	}
}
