<?php

/**
 * Lineup Manager pages
 *
 * @package Lineup
 * @subpackage Manager
 * @since 0.1
 * @author X-Team
 * @author Dzikri Aziz <kucrut@x-team.com>
 * @copyright Copyright (c) 2013, X-Team
 */
class XTeam_Lineup_Manager {

	/**
	 * Nonce action for ajax action
	 *
	 * @since 0.1
	 */
	const NONCE_ACTION = 'xteam_lineup_nonce';

	/**
	 * Admin pages hooks
	 *
	 * @since 0.1
	 * @access private
	 */
	private static $_admin_pages = array();

	/**
	 * Current page data, holds the post type, lineup groups
	 *
	 * @since 0.1
	 * @access private
	 */
	private static $_current_page;

	/**
	 * Plugin settings instance
	 *
	 * @since 0.1
	 * @access private
	 */
	private static $_settings;

	/**
	 * Post meta keys
	 *
	 * @since 0.1
	 * @access private
	 */
	private static $_meta_keys = array(
		'disabled' => 'xteam_lineup_disabled_in',
	);


	/**
	 * Initialize Lineup Manager pages
	 *
	 * @since 0.1
	 * @access private
	 *
	 * @return void
	 */
	public static function setup() {
		self::$_settings = XTeam_Lineup::get( 'settings' );

		// Register lineup manager pages for each post type
		add_action( 'admin_menu', array( __CLASS__, '_register_menus' ) );

		// Register load callback for each lineup manager pages
		add_action( 'admin_init', array( __CLASS__, '_register_load_actions' ) );

		// Protect meta keys
		add_filter( 'is_protected_meta', array( __CLASS__, '_protect_meta_keys' ), 10, 3 );

		// Ajax
		// Get posts for featured lineups, injected into the post finder box
		add_action( 'wp_ajax_xteam_lineup_get_items', array( __CLASS__, '_the_items' ) );

		// Save last accessed tab, per-user
		add_action( 'wp_ajax_xteam_lineup_save_last_tab', array( __CLASS__, '_save_last_tab' ) );

		// Enable/disable post from term lineups
		add_action( 'wp_ajax_xteam_lineup_enable_disable_post', array( __CLASS__, '_enable_disable_post' ) );
	}


	/**
	 * Store lineup manager pages hook names
	 *
	 * @since 0.1
	 * @access private
	 *
	 * @param string $hook_name  Output of add_submenu_page()
	 * @param string $page_title Title of admin page
	 *
	 * @return void
	 */
	private static function _add_admin_page( $hook_name, $page_data ) {
		self::$_admin_pages[ $hook_name ] = $page_data;
	}


	/**
	 * Protect meta keys
	 *
	 * This will prevent the meta keys from appearing inside post custom fields
	 * box
	 *
	 * @since 0.1
	 *
	 * @param bool   $protected Protection status
	 * @param string $meta_key  Meta key
	 * @param string $meta_type Meta type (post, user, term)
	 *
	 * @filter is_protected_meta
	 *
	 * @return bool Meta key protection status
	 */
	public static function _protect_meta_keys( $protected, $meta_key, $meta_type ) {
		if ( 'post' !== $meta_type ) {
			return $protected;
		}

		foreach ( self::$_meta_keys as $key_prefix ) {
			if ( $meta_key === $key_prefix || false !== strpos( $meta_key, $key_prefix ) ) {
				$protected = true;
				break;
			}
		}

		return $protected;
	}


	/**
	 * Register lineup manager pages for each post type
	 *
	 * Each enabled post type will have its own lineup manager page which can
	 * be accessed under `Post type label` &raquo; Lineup
	 *
	 * @since 0.1
	 * @access private
	 *
	 * @todo Consider a situation where a custom post type is registered
	 *       without having its own parent menu.
	 *
	 * @action admin_menu
	 *
	 * @return void
	 */
	public static function _register_menus() {
		$post_types = array_keys( self::$_settings->get_field_value( array( 'options', 'post_types' ) ) );
		foreach ( $post_types as $post_type_name ) {
			$post_type_object = get_post_type_object( $post_type_name );
			if ( empty($post_type_object) ) {
				continue;
			}

			$menu_location = ( $post_type_name == 'post' ) ? 'edit.php' : sprintf( 'edit.php?post_type=%s', $post_type_name );
			$page_title    = sprintf( __( '%s Lineup Manager', 'xteam-lineup' ), $post_type_object->label );
			$menu_title    = __( 'Lineup', 'xteam-lineup' );
			$hook_name     = add_submenu_page(
				$menu_location,
				$page_title,
				$menu_title,
				'edit_posts',
				sprintf( '%s-lineup', $post_type_name ),
				array( __CLASS__, '_manager_page' )
			);

			self::_add_admin_page(
				$hook_name,
				array(
					'post_type' => $post_type_name,
					'title'     => $page_title,
				)
			);
		}
	}


	/**
	 * Register load callback for each lineup manager pages
	 *
	 * @since 0.1
	 * @access private
	 *
	 * @return void
	 */
	public static function _register_load_actions() {
		foreach ( array_keys( self::$_admin_pages ) as $hook_name ) {
			add_action( sprintf( 'load-%s', $hook_name ), array( __CLASS__, '_page_init' ) );
		}
	}


	/**
	 * Load callback for each lineup manager pages
	 *
	 * @since 0.1
	 * @access private
	 *
	 * @action load-*
	 *
	 * @return void
	 */
	public static function _page_init() {
		$screen_id = get_current_screen()->id;
		self::$_current_page = self::$_admin_pages[ $screen_id ];
		self::$_current_page['id']     = $screen_id;
		self::$_current_page['groups'] = self::_get_groups( self::$_current_page['post_type'] );

		add_action( 'admin_enqueue_scripts', array( __CLASS__, '_enqueue_assets' ) );
		add_action( 'admin_footer', array( __CLASS__, '_append_post_finder_box' ), 99 );
	}


	/**
	 * Enqueue scripts and styles for lineup manager pages
	 *
	 * @since 0.1
	 * @access private
	 *
	 * @action admin_enqueue_scripts
	 *
	 * @return void
	 */
	public static function _enqueue_assets() {
		wp_enqueue_style(
			'xteam-lineup-manager',
			trailingslashit( XTeam_Lineup::get( 'plugin_dir_url' ) ) . 'assets/manager.css',
			false,
			XTeam_Lineup::VERSION
		);

		wp_enqueue_script(
			'xteam-lineup-manager',
			trailingslashit( XTeam_Lineup::get( 'plugin_dir_url' ) ) . 'assets/manager.js',
			array( 'postbox', 'wp-ajax-response', 'media' ),
			XTeam_Lineup::VERSION,
			true
		);

		$js_data = array(
			'ajaxNonce' => wp_create_nonce( self::NONCE_ACTION ),
			'texts'     => array(
				'addNewFeaturedTitle' => __( 'Featured lineup name:', 'xteam-lineup' ),
				'featuredNameIsUsed'  => __( 'Name is already used.', 'xteam-lineup' ),
				'noItemsFound'        => __( 'No posts found.', 'xteam-lineup' ),
				'onlyAlphanumeric'    => __( 'Please only use alphanumeric characters.', 'xteam-lineup' ),
				'confrimRemove'       => __( 'Are you sure you want to remove this lineup?', 'xteam-lineup' ),
			),
		);
		wp_localize_script( 'xteam-lineup-manager', 'xteamLineup', $js_data );
	}


	/**
	 * Lineup manager for each post type
	 *
	 * Each admin page will contains groups that are enabled in the config.
	 * A group could be a taxonomy or '__featured__' for custom featured lineup.
	 *
	 * A taxonomy group will have its terms as lineups, displayed as metaboxes
	 *
	 * A featured lineup group will have one lineup by default, called '__default__'.
	 * New featured lineup(s) can be added.
	 *
	 * @since 0.1
	 * @access private
	 *
	 * @return void
	 */
	public static function _manager_page() {
		$hidden_fields = array(
			'settings-part'     => 'lineups',
			'current-post-type' => self::$_current_page['post_type'],
		);
		?>
			<div class="wrap">
				<?php screen_icon(); ?>
				<h2 class="nav-tab-wrapper">
					<?php foreach ( self::$_current_page['groups'] as $group ) : ?>
						<?php self::_group_tab( $group ) ?>
					<?php endforeach; ?>
				</h2>

				<form action="options.php" method="post" class="xteam-lineup-manager">
					<?php settings_fields( XTeam_Lineup::OPTION_KEY ); ?>
					<?php wp_nonce_field( 'closedpostboxes', 'closedpostboxesnonce', false ); ?>
					<?php wp_nonce_field( 'meta-box-order', 'meta-box-order-nonce', false ); ?>
					<?php foreach ( $hidden_fields as $key => $value ) : ?>
						<?php printf(
							'<input type="hidden" id="%s" name="%s" value="%s" />',
							esc_attr( self::$_settings->get_field_id( array( $key ) ) ),
							esc_attr( self::$_settings->get_field_name( array( $key ) ) ),
							esc_attr( $value )
						) ?>
					<?php endforeach; ?>

					<?php self::_the_history() ?>

					<div class="groups-wrap">
						<?php foreach ( self::$_current_page['groups'] as $group ) : ?>
							<?php self::_group_content( $group ) ?>
						<?php endforeach; ?>
					</div>
				</form>
			</div>
		<?php
	}


	/**
	 * Print lineup history
	 *
	 * @since 0.1
	 * @access private
	 *
	 * @return void
	 */
	private static function _the_history() {
		$history = self::$_settings->get_field_value( array( 'history', self::$_current_page['post_type'] ) );
		if ( empty( $history ) ) {
			return null;
		}

		$date_format = sprintf(
			_x( '%1$s \a\t %2$s', 'lineup history datetime format', 'xteam-lineup' ),
			get_option( 'date_format' ),
			get_option( 'time_format' )
		);
		$user        = get_user_by( 'id', $history['user_id'] );
		$date_time   = date_i18n( $date_format, $history['timestamp'] );
		?>
			<?php if ( $user ) : ?>
				<p><?php printf(
					__( 'Last updated: %1$s by %2$s', 'xteam-lineup' ),
					'<em>'. $date_time .'</em>',
					'<strong>'. $user->data->user_nicename .'</strong>'
				) ?></p>
			<?php else : ?>
				<p><?php printf(
					__( 'Last updated: %1$s', 'xteam-lineup' ),
					'<em>'. $date_time .'</em>'
				) ?></p>
			<?php endif; ?>
		<?php
	}


	/**
	 * Get lineup groups of a post type
	 *
	 * @since 0.1
	 * @access private
	 *
	 * @param string $post_type Post type name
	 *
	 * @return array|false Array of lineup groups, or false on failure
	 */
	private static function _get_groups( $post_type ) {
		$group_types = array_filter(
			self::$_settings->get_field_value(
				array(
					'options',
					'post_types',
					self::$_current_page['post_type'],
				)
			)
		);

		if ( empty( $group_types ) ) {
			return false;
		}

		$groups = array();
		foreach ( array_keys( $group_types ) as $group_type ) {
			if ( 'featured' === $group_type ) {
				$groups[] = array(
					'id'      => '__featured__',
					'title'   => __( 'Featured Areas', 'xteam-lineup' ),
					'type'    => $group_type,
					'lineups' => self::_get_featured_lineups( $post_type ),
				);
			}
			elseif ( 'terms' === $group_type ) {
				$taxonomies = get_object_taxonomies( $post_type, 'object' );
				$taxonomies = apply_filters( 'lineup_taxonomies_for_post_type', $taxonomies, $post_type );

				foreach ( $taxonomies as $taxonomy_name => $taxonomy_object ) {
					$lineups = self::_get_taxonomy_lineups( $post_type, $taxonomy_name );
					if ( ! empty( $lineups ) ) {
						$groups[] = array(
							'id'      => $taxonomy_name,
							'title'   => $taxonomy_object->label,
							'type'    => $group_type,
							'lineups' => $lineups,
						);
					}
				}
			}
		}

		return $groups;
	}


	/**
	 * Print lineup history
	 *
	 * @since 0.1
	 * @access private
	 *
	 * @return void
	 */
	private static function _get_featured_lineups( $post_type ) {
		$current_lineups = self::$_settings->get_field_value( array( 'lineups', $post_type, '__featured__' ) );
		$lineups = array();

		if ( ! empty( $current_lineups ) ) {
			foreach ( $current_lineups as $lineup_id => $post_ids ) {
				$items = self::get_items(
					array(
						'post_type'  => 'any',
						'group_type' => 'featured',
						'group_id'   => '__featured__',
						'post__in'   => $post_ids,
					)
				);

				if ( $items->have_posts() ) {
					$lineups[] = array(
						'id'     => $lineup_id,
						'title'  => $lineup_id,
						'hidden' => false,
						'items'  => $items,
					);
				}
			}
		}
		else {
			$lineups[] = array(
				'id'     => '__default__',
				'title'  => '',
				'hidden' => true,
				'items'  => array(),
			);
		}

		return $lineups;
	}


	/**
	 * Get taxonomy lineups
	 *
	 * @since 0.1
	 * @access private
	 *
	 * @param string $post_type Post type name
	 * @param string $taxonomy  Taxonomy name
	 *
	 * @return array Array of taxonomy lineups
	 */
	private static function _get_taxonomy_lineups( $post_type, $taxonomy ) {
		$lineups = array();

		if ( 'post_format' === $taxonomy ) {
			$terms = self::_get_post_format_terms( $post_type );
		}
		else {
			$terms = get_terms(
				$taxonomy,
				array(
					'orderby'       => '_xteam_lineup',
					'_xteam_lineup' => array( 'post_type' => $post_type ),
				)
			);
			if ( empty( $terms ) ) {
				return $lineups;
			}
		}

		foreach ( $terms as $term ) {
			$items = self::get_items(
				array(
					'post_type'  => $post_type,
					'group_type' => 'terms',
					'group_id'   => $taxonomy,
					'term_slug'  => $term->slug,
				)
			);

			if ( $items->have_posts() ) {
				$lineups[] = array(
					'id'     => $term->slug,
					'title'  => $term->name,
					'hidden' => false,
					'items'  => $items,
				);
			}
		}

		return $lineups;
	}


	/**
	 * Get post formats as objects just like get_terms()
	 *
	 * @since 0.1
	 * @access private
	 *
	 * @param string $post_type Post type name, used for sorting the result based on saved lineups
	 *
	 * @return array Array of post format objects
	 */
	private static function _get_post_format_terms( $post_type = '' ) {
		$post_formats = get_post_format_strings();
		$terms = array();
		foreach ( $post_formats as $slug => $name ) {
			$new_slug = sprintf( 'post-format-%s', $slug );
			$terms[ $new_slug ] = (object) array(
				'slug' => $new_slug,
				'name' => $name,
			);
		}

		$post_format_lineups = self::$_settings->get_field_value( array( 'lineups', $post_type, 'post_format' ) );
		if ( ! empty($post_type) && ! empty( $post_format_lineups ) ) {
			$terms = array_merge(
				array_fill_keys( array_keys( $post_format_lineups ), false ),
				$terms
			);
		}

		return $terms;
	}


	/**
	 * Display lineup group's tab
	 *
	 * @since 0.1
	 * @access private
	 *
	 * @param array $group Group data, contains 'id' and 'title'
	 *
	 * @return void
	 */
	private static function _group_tab( $group ) {
		static $index = 0;
		static $last_active_tab = null;

		if ( is_null( $last_active_tab ) ) {
			$last_active_tab = self::_get_last_active_tab();
		}

		$hash  = sprintf( '#lineup-group-%s', $group['id'] );
		$class = 'nav-tab';
		if ( ( $hash === $last_active_tab ) || ( empty( $last_active_tab ) && 0 === $index ) ) {
			$class .= ' last-active';
		}

		$tab = printf(
			'<a href="%s" class="%s">%s</a>',
			esc_url( $hash ),
			esc_attr( $class ),
			esc_html( $group['title'] )
		); // xss ok

		$index++;
	}


	/**
	 * Get user's last accesed tab
	 *
	 * @since 0.1
	 * @access private
	 *
	 * @return string Last active tab hash
	 */
	private static function _get_last_active_tab() {
		$last_active_tab = get_user_meta(
			wp_get_current_user()->ID,
			sprintf( '%s_last_tab', get_current_screen()->id ),
			true
		);

		return $last_active_tab;
	}


	/**
	 * Save user's last accessed tab
	 *
	 * @since 0.1
	 * @access private
	 *
	 * @action wp_ajax_xteam_lineup_save_last_tab
	 *
	 * @return void
	 */
	public static function _save_last_tab() {
		check_ajax_referer( self::NONCE_ACTION, 'ajax_nonce' );

		if ( ! $user = wp_get_current_user() ) {
			wp_die( -1 );
		}

		if ( ! empty($_POST['tab']) ) {
			update_user_option(
				$user->ID,
				sprintf( '%s_last_tab', $_POST['page'] ),
				$_POST['tab'],
				true
			);
		}

		wp_die( 1 );
	}


	/**
	 * Display lineup group content
	 *
	 * @since 0.1
	 * @access private
	 *
	 * @param array $group Group data
	 *
	 * @return void
	 */
	private static function _group_content( $group ) {
		?>
			<div id="<?php echo esc_attr( sprintf( 'lineup-group-%s', $group['id'] ) ) ?>" class="metabox-holder" data-group-id="<?php echo esc_attr( $group['id'] ) ?>">
				<?php if ( 'featured' === $group['type'] ) : ?>
					<a href="#" class="add-new-h2"><?php _e( 'Add New', 'xteam-lineup' ) ?></a>
				<?php endif; ?>
				<div class="postbox-container">
					<div class="meta-box-sortables" id="<?php echo esc_attr( sprintf( 'xteam-lineup-%s', $group['id'] ) ) ?>">
						<?php if ( ! is_array( $group['lineups'] ) ) : ?>
							<p class="description"><?php echo $group['lineups']; // xss ok ?></p>
						<?php else : ?>
							<?php foreach ( $group['lineups'] as $lineup ) : ?>
								<?php self::_the_lineup_box( $group['id'], $group['type'], $lineup ) ?>
							<?php endforeach; ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		<?php
	}


	/**
	 * Lineup meta box
	 *
	 * @since 0.1
	 * @access private
	 *
	 * @param string $group_id   Group ID ( '__featured__' or taxonomy name )
	 * @param string $group_type Group type ( 'featured' or 'terms' )
	 * @param array  $lineup     Lineup properties
	 *
	 * @return void
	 */
	private static function _the_lineup_box( $group_id, $group_type, $lineup ) {
		$box_id = sprintf(
			'xteam-lineup-%s-%s-%s',
			self::$_current_page['post_type'],
			$group_id,
			$lineup['id']
		);

		$box_classes = sprintf(
			'%s-lineup %s postbox widefat %s',
			$group_type,
			( ! empty( $lineup['hidden'] ) ) ? 'hidden' : '',
			postbox_classes( $box_id, self::$_current_page['id'] )
		);
		?>
			<div class="<?php echo esc_attr( $box_classes ) ?>" id="<?php echo esc_attr( $box_id ) ?>" data-lineup-id="<?php echo esc_attr( $lineup['id'] ) ?>">
				<div title="<?php esc_attr_e( 'Click to toggle', 'xteam-lineup' ) ?>" class="handlediv"><br /></div>
				<h3 class="hndle"><span><?php echo esc_html( $lineup['title'] ) ?></span></h3>
				<div class="inside">
					<?php if ( ! $lineup['hidden'] ) : ?>
						<?php self::_the_items(
							$lineup['items'],
							array(
								'post_type'  => self::$_current_page['post_type'],
								'group_id'   => $group_id,
								'group_type' => $group_type,
								'lineup_id'  => $lineup['id'],
							)
						) ?>
					<?php endif; ?>
					<div class="lineup-actions">
						<?php if ( 'featured' === $group_type ) : ?>
							<div class="alignleft submitbox">
								<?php
									printf(
										'<a href="#add-post" class="add-post button" title="%1$s">%2$s</a>',
										esc_attr__( 'Add posts to this lineup', 'xteam-lineup' ),
										esc_html__( 'Add posts', 'xteam-lineup' )
									)
								?>
								<?php
									printf(
										'<a href="#remove-lineup" class="remove-lineup submitdelete" title="%1$s">%2$s</a>',
										esc_attr__( 'Remove this featured lineup', 'xteam-lineup' ),
										esc_html__( 'Remove Lineup', 'xteam-lineup' )
									);
								?>
							</div>
						<?php endif; ?>
						<div class="alignright">
							<input class="button button-primary" type="submit" value="<?php esc_attr_e( 'Update', 'xteam-lineup' ) ?>" />
						</div>
						<br class="clear" />
					</div>
				</div>
			</div>
		<?php
	}


	/**
	 * Get URL of post list table screen
	 *
	 * @since 0.1
	 * @access private
	 *
	 * @param string $post_type Post type name
	 *
	 * @return string URL
	 */
	private static function _get_posts_screen_url( $post_type ) {
		$url = esc_url(
			add_query_arg(
				array( 'post_type' => $post_type ),
				admin_url( 'edit.php' )
			)
		);

		return $url;
	}


	/**
	 * Get URL of taxonomy terms list table screen
	 *
	 * @since 0.1
	 * @access private
	 *
	 * @param string $taxonomy  Taxonomy name
	 * @param string $post_type Post type name
	 *
	 * @return string URL
	 */
	private static function _get_taxonomy_screen_url( $taxonomy, $post_type ) {
		$url = esc_url(
			add_query_arg(
				array(
					'taxonomy'  => $taxonomy,
					'post_type' => $post_type,
				),
				admin_url( 'edit-tags.php' )
			)
		);

		return $url;
	}


	/**
	 * Get lineup posts
	 *
	 * $args must contain 'post_type', 'group_id'. For a taxonomy term lineup,
	 * $args must contain 'term_slug', and for featured lineup, $args must
	 * contain either 'post__in' to get the posts of existing feature lineup,
	 * or 'post__not_in' to get posts for filling up the post finder box.
	 *
	 * @since 0.1
	 * @access public
	 *
	 * @param array $args Arguments
	 *
	 * @return object WP_Query object
	 */
	public static function get_items( $args = array(), $wp_query_args = array() ) {
		$query_args = array(
			'post_type'      => $args['post_type'],
			'posts_per_page' => apply_filters( 'lineup_posts_per_page', -1, $args ),
			'no_found_rows'  => true,
		);

		if ( 'terms' === $args['group_type'] ) {
			$taxonomy = $args['group_id'];
			if ( ! taxonomy_exists( $taxonomy ) ) {
				return false;
			}

			$query_args['orderby']       = '_xteam_lineup';
			$query_args['_xteam_lineup'] = array(
				'post_type' => $args['post_type'],
				'taxonomy'  => $taxonomy,
				'term_slug' => $args['term_slug'],
			);

			$wp_query_args = wp_parse_args(
				$wp_query_args,
				array( 'post_type' => get_taxonomy( $taxonomy )->object_type )
			);

			$tax_query = array(
				'taxonomy' => $args['group_id'],
				'field'    => 'slug',
			);

			if ( 'post_format' === $args['group_id'] && 'post-format-standard' === $args['term_slug'] ) {
				$post_formats = self::_get_post_format_terms( $args['post_type'] );
				unset( $post_formats['post-format-standard'] );
				$tax_query['terms']    = wp_list_pluck( $post_formats, 'slug' );
				$tax_query['operator'] = 'NOT IN';
			}
			else {
				$tax_query['terms'] = array( $args['term_slug'] );
			}

			$query_args['tax_query'] = array( $tax_query );
		}
		else {
			if ( ! empty( $args['post__in'] ) ) {
				$query_args['post__in'] = $args['post__in'];
				$query_args['orderby']  = 'post__in';
				$query_args['order']    = 'ASC';
			}
			elseif ( ! empty( $args['post__not_in'] ) ) {
				$query_args['post__not_in'] = $args['post__not_in'];
				$query_args['orderby']      = 'date';
				$query_args['order']        = 'DESC';
			}
		}

		return new WP_Query( wp_parse_args( $wp_query_args, $query_args ) );
	}


	/**
	 * Get lineup items
	 *
	 * This method is also used as the ajax callback to add new item(s)
	 * into a featured lineup.
	 *
	 * @since 0.1
	 * @access private
	 *
	 * @param null|object $query WP_Query object returned by get_items() or NULL if called by ajax action
	 * @param array  $args See {XTeam_Lineup_Manager::get_items()}
	 *
	 * @wp_hook action wp_ajax_xteam_lineup_get_items
	 *
	 * @return void
	 */
	public static function _the_items( $query = null, $args = array() ) {
		$is_ajax = (
			'wp_ajax_xteam_lineup_get_items' === current_filter()
			&& ! empty( $_POST['args'] )
			&& check_ajax_referer( self::NONCE_ACTION, 'ajax_nonce' )
		);
		if ( $is_ajax ) {
			$args       = $_POST['args'];
			$extra_args = ! empty( $_POST['extraArgs'] ) ? $_POST['extraArgs'] : array();

			$query = self::get_items( $args, $extra_args );
			if ( ! $query || ! $query->have_posts() ) {
				wp_die( '<p>'. __( 'No posts found.', 'xteam-lineup' ) .'.</p>' );
			}
		}

		if ( ! $query || ! $query->have_posts() ) {
			return false;
		}

		$post_statuses = get_post_statuses();
		$input_name    = self::$_settings->get_field_name(
			array(
				'lineups',
				$args['post_type'],
				$args['group_id'],
				$args['lineup_id'],
				'',
			)
		);

		if ( $is_ajax ) {
			ob_start();
		}
		?>
		<ul class="posts-list">
			<?php while ( $query->have_posts() ) : ?>
				<?php $query->the_post(); ?>
				<li class="<?php echo esc_attr( self::get_item_class( $args ) ) ?>">
					<?php the_post_thumbnail( 'thumbnail' ); ?>
					<h4 class="title"><?php the_title() ?></h4>
					<p class="props">
						<span class="post-date"><?php the_time( 'Y/m/d' ) ?></span>
						<span class="post-type"><?php echo esc_html( XTeam_Lineup::get_post_type_label( get_post_type(), false ) ) ?></span>
						<em class="post-status"><?php echo esc_html( $post_statuses[ get_post_status() ] ) ?></em>
					</p>
					<div class="row-actions">
						<a class="edit" href="<?php echo esc_url( get_edit_post_link() ) ?>" target="_blank"><?php esc_html_e( 'Edit', 'xteam-lineup' ) ?></a>
						| <a class="view" href="<?php the_permalink() ?>" target="_blank"><?php esc_html_e( 'View', 'xteam-lineup' ) ?></a>
						| <a class="edr-post" href="#">
							<span class="enable"><?php esc_html_e( 'Enable', 'xteam-lineup' ) ?></span>
							<span class="disable"><?php esc_html_e( 'Disable', 'xteam-lineup' ) ?></span>
							<span class="remove"><?php esc_html_e( 'Remove from lineup', 'xteam-lineup' ) ?></span>
							<span class="add"><?php esc_html_e( 'Add to lineup', 'xteam-lineup' ) ?></span>
						</a>
					</div>
					<input type="hidden" class="post-id" value="<?php echo esc_attr( get_the_ID() ) ?>" name="<?php echo esc_attr( $input_name ) ?>" />
					<span class="handle"><span class="screen-reader-shortcut"><?php esc_html_e( 'Drag and drop to reorder', 'xteam-lineup' ) ?></span></span>
				</li>
			<?php endwhile; ?>
		</ul>
		<?php wp_reset_postdata(); ?>
		<?php

		if ( $is_ajax ) {
			wp_die( ob_get_clean() );
		}
	}


	/**
	 * Get post list item HTML classes
	 *
	 * @since 0.2
	 * @access private
	 * @param array $args See XTeam_Lineup_Manager::_the_items()
	 * @return string HTML classes
	 */
	public static function get_item_class( $args ) {
		$item_class = 'item';
		if ( 'terms' === $args['group_type'] && self::_is_post_disabled( get_the_ID(), $args['lineup_id'] ) ) {
			$item_class .= ' disabled';
		}
		if ( has_post_thumbnail() ) {
			$item_class .= ' has-thumbnail';
		}

		return $item_class;
	}


	/**
	 * Append post finder box to current page
	 *
	 * A 'fork' of find_posts_div()
	 *
	 * @since 0.1
	 * @access private
	 *
	 * @action admin_footer
	 *
	 * @return void
	 */
	public static function _append_post_finder_box() {
		?>
		<div id="find-posts" class="find-box" style="display:none;">
			<div id="find-posts-head" class="find-box-head"><?php echo esc_html__( 'Find posts', 'xteam-lineup' ); ?></div>
			<div class="find-box-inside">
				<div class="find-box-search">
					<input type="hidden" name="affected" id="affected" value="" />
					<label class="screen-reader-text" for="find-posts-input"><?php _e( 'Search', 'xteam-lineup' ); ?></label>
					<input type="text" id="find-posts-input" name="ps" value="" />
					<span class="spinner"></span>
					<?php self::_the_post_types_dropdown() ?>
					<input type="button" id="find-posts-search" value="<?php esc_attr_e( 'Search', 'xteam-lineup' ); ?>" class="button" />
				</div>
				<div id="find-posts-response"></div>
			</div>
			<div class="find-box-buttons">
				<input id="find-posts-close" type="button" class="button alignleft" value="<?php esc_attr_e( 'Close', 'xteam-lineup' ); ?>" />
				<?php submit_button( __( 'Add', 'xteam-lineup' ), 'button-primary alignright', 'find-posts-submit', false ); ?>
			</div>
		</div>
		<?php
	}


	/**
	 * Post types dropdown for the posts finder box
	 *
	 * @since 0.2
	 * @access private
	 * @return void
	 */
	private static function _the_post_types_dropdown() {
		?>
			<select id="find-posts-type" class="extra-args">
				<option value=""><?php esc_html_e( 'All post types', 'xteam-lineup' ) ?></option>
				<?php foreach ( XTeam_Lineup::get_post_types()as $post_type_object ) : ?>
					<?php printf(
						'<option value="%s"%s>%s</option>',
						esc_attr( $post_type_object->name ),
						selected( $post_type_object->name, self::$_current_page['post_type'], false ),
						esc_html( $post_type_object->labels->name )
					) ?>
				<?php endforeach; ?>
			</select>
		<?php
	}


	/**
	 * Check if post is either enabled or disabled in a taxonomy term lineup.
	 *
	 * @since 0.1
	 * @access private
	 *
	 * @param int    $post_id   Post ID
	 * @param string $term_slug Lineup ID (term slug)
	 *
	 * @return bool True if post is disabled
	 */
	private static function _is_post_disabled( $post_id, $term_slug ) {
		return get_post_meta( $post_id, sprintf( '%s_%s', self::$_meta_keys['disabled'], $term_slug ), true );
	}


	/**
	 * Ajax callback: Enable/disable post from a term lineup
	 *
	 * Arguments:
	 * - action:    xteam_lineup_enable_disable_post
	 * - post_id:   Post ID
	 * - term_slug: Lineup term slug
	 *
	 * @since 0.1
	 * @access private
	 *
	 * @action wp_ajax_xteam_lineup_enable_disable_post
	 *
	 * @return void
	 */
	public static function _enable_disable_post() {
		check_ajax_referer( self::NONCE_ACTION, 'ajax_nonce' );

		if ( empty( $_POST['post_id'] ) || empty( $_POST['term_slug'] ) ) {
			wp_die( -1 );
		}

		$post_id  = absint( $_POST['post_id'] );
		$meta_key = sprintf( '%s_%s', self::$_meta_keys['disabled'], $_POST['term_slug'] );

		if ( empty( $_POST['enable'] ) ) {
			// Disable
			add_post_meta( $post_id, $meta_key, 1 );
		}
		else {
			// Enable
			delete_post_meta( $post_id, $meta_key );
		}

		wp_die( 1 );
	}
}
