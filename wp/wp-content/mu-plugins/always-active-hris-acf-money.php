<?php

add_filter( 'auto_activated_required_plugins', function ( $plugins ) {
	$plugins[] = 'hris-acf-money/hris-acf-money.php';
	return $plugins;
});
