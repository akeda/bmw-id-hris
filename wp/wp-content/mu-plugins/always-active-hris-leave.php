<?php

add_filter( 'auto_activated_required_plugins', function ( $plugins ) {
	$plugins[] = 'hris-leave/hris-leave.php';
	return $plugins;
});
