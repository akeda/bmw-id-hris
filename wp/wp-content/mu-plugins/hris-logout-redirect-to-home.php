<?php

function _hris_change_logout_redirect_to_home() {
	check_admin_referer('log-out');
	wp_logout();

	wp_safe_redirect( home_url() );
	exit();
}
add_action( 'login_form_logout', '_hris_change_logout_redirect_to_home' );
