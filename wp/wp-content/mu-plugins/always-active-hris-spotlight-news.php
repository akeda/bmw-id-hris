<?php

add_filter( 'auto_activated_required_plugins', function ( $plugins ) {
	$plugins[] = 'hris-spotlight-news/hris-spotlight-news.php';
	return $plugins;
});
