<?php
/**
 * Removes post row actions (which appears when user hovers the row in post list table).
 *
 * @param array $actions
 * @param object $post Post object
 * @return void
 */
function _hris_removes_post_row_actions( $actions, $post ) {
	$expected_post_types = array(
		HRIS_Leave_Post_Type::NAME,
		HRIS_Medical_Post_Type::NAME,
	);

	$post_type = get_post_type( $post->ID );
	if ( ! in_array( $post_type, $expected_post_types ) )
		return $actions;

	return array();
}
add_action( 'post_row_actions', '_hris_removes_post_row_actions', 10, 2 );
