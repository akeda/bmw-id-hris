<?php

add_filter( 'auto_activated_required_plugins', function ( $plugins ) {
	$plugins[] = 'hris-medical/hris-medical.php';
	return $plugins;
});
