<?php

function _hris_change_lost_password_label( $links ) {
	if ( isset( $links['lost_password']['text'] ) ) {
		$links['lost_password']['text'] = __( 'Forgot Password', 'hris' );
	}

	return $links;
}
add_filter( 'sidebar_login_widget_logged_out_links', '_hris_change_lost_password_label' );

function _hris_changes_login_label( $labels ) {
	if ( isset( $labels['label_log_in'] ) ) {
		$labels['label_log_in'] = __( 'Login', 'hris' );
	}

	return $labels;
}
add_filter( 'sidebar_login_widget_form_args', '_hris_changes_login_label' );
