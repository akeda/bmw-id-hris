<?php

add_filter( 'auto_activated_required_plugins', function ( $plugins ) {
	$plugins[] = 'hris-profile/hris-profile.php';
	return $plugins;
});
