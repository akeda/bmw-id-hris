<?php

add_filter( 'auto_activated_required_plugins', function ( $plugins ) {
	$plugins[] = 'hris-approval/hris-approval.php';
	return $plugins;
});
