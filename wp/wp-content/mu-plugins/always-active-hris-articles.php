<?php

add_filter( 'auto_activated_required_plugins', function ( $plugins ) {
	$plugins[] = 'hris-articles/hris-articles.php';
	return $plugins;
});
