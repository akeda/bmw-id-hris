<?php

add_filter( 'auto_activated_required_plugins', function ( $plugins ) {
	$plugins[] = 'hris-admin-style/hris-admin-style.php';
	$plugins[] = 'hris-acf-style/hris-acf-style.php';

	return $plugins;
});
