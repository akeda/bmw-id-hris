<?php

// Hides footer text and WordPress version in admin page.
add_filter( 'admin_footer_text', '__return_empty_string' );
add_filter( 'update_footer',     '__return_empty_string', 12 );

/**
 * Removes WordPress menu in admin bar.
 *
 * @action wp_before_admin_bar_render
 * @return void
 */
function _hris_hide_wp_menu_in_admin_bar() {
	global $wp_admin_bar;

	$wp_admin_bar->remove_menu( 'wp-logo' );
	$wp_admin_bar->remove_menu( 'comments' );
}
add_action( 'wp_before_admin_bar_render', '_hris_hide_wp_menu_in_admin_bar' );

/**
 * Replaces WordPress logo in login page with expected logo in
 * the root directory of active theme.
 *
 * @action login_head
 * @return void
 */
function _hris_replace_wp_logo_with_theme_logo() {
	$expected_logo = get_stylesheet_directory_uri() . '/logo.png';
	?>
	<style type="text/css">
	#login h1 a {
		background-image: url(<?php echo $expected_logo ?>) !important;
	}
	</style>
	<?php
}
add_action( 'login_head', '_hris_replace_wp_logo_with_theme_logo' );

// Replaces URL of custom logo and the title.
add_filter( 'login_headerurl', function() { return home_url(); } );
add_filter( 'login_headertitle', function() { return get_bloginfo( 'title' ); } );
