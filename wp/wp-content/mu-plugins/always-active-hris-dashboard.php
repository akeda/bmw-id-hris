<?php

add_filter( 'auto_activated_required_plugins', function ( $plugins ) {
	$plugins[] = 'hris-dashboard/hris-dashboard.php';
	return $plugins;
});
