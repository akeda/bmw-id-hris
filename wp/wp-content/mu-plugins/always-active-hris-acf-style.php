<?php

add_filter( 'auto_activated_required_plugins', function ( $plugins ) {
	$plugins[] = 'hris-acf-style/hris-acf-style.php';
	return $plugins;
});
