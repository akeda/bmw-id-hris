<?php

add_filter( 'auto_activated_required_plugins', function ( $plugins ) {
	$plugins[] = 'lineup/lineup.php';
	return $plugins;
});
