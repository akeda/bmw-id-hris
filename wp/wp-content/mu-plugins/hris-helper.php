<?php

/**
 * Checks if a particular user has a role.
 * Returns true if a match was found.
 *
 * @param string $role Role name.
 * @param int $user_id (Optional) The ID of a user. Defaults to the current user.
 * @return bool
 * @link http://docs.appthemes.com/tutorials/wordpress-check-user-role-function/
 */
function hris_check_user_role( $role, $user_id = null ) {
	if ( is_numeric( $user_id ) )
		$user = get_userdata( $user_id );
	else
		$user = wp_get_current_user();

	if ( empty( $user ) )
		return false;

	return in_array( $role, (array) $user->roles );
}

/**
 * Gets HRIS component from a module.
 *
 * @param string $module Module's name
 * @param string $component Component's name
 * @return null|object Instance of the component
 */
function hris_get_component_from_module( $module, $component ) {
	if ( ! isset( $GLOBALS['hris_' . $module . '_components'][ $component ] ) )
		return  null;

	$component = $GLOBALS['hris_' . $module . '_components'][ $component ];
	if ( ! is_object( $component ) )
		return null;

	return $component;
}

/**
 * Gets next post status from a given post type and status.
 *
 * @param string $post_type Post type
 * @param int $user_id User ID
 * @param string $post_status Post status
 * @return string Post status
 */
function hris_get_next_post_status( $post_type, $user_id, $post_status ) {
	$setting = $GLOBALS['hris_approval_components']['Setting'];
	if ( ! is_object( $setting ) )
		return null;

	return $setting->get_next_post_status( $post_type, $user_id, $post_status );
}

/**
 * Gets approver by post status.
 *
 * @param string $post_type Post type
 * @param int $user_id User ID who creates the post
 * @param string $post_status Post status
 * @return null|int User ID of the approver
 */
function hris_get_approver_by_status( $post_type, $user_id, $post_status ) {
	$setting = $GLOBALS['hris_approval_components']['Setting'];
	if ( ! is_object( $setting ) )
		return null;

	return $setting->get_approver_in_current_status( $post_type, $user_id, $post_status );
}

/**
 * Gets approve post link.
 *
 * @param int $post_id Post ID
 * @return string
 */
function hris_get_approve_post_link( $post_id ) {
	if ( !$post = get_post( $post_id ) )
		return;

	$post_type_object = get_post_type_object( $post->post_type );
	if ( !$post_type_object )
		return;

	// Assuming non 'hris_requestor' always have the permission to approve.
	if ( hris_check_user_role( 'hris_requestor' ) )
		return;

	$action       = 'approve';
	$approve_link = add_query_arg( 'action', $action, admin_url( sprintf( $post_type_object->_edit_link, $post->ID ) ) );

	return apply_filters( 'hris_get_approve_post_link', wp_nonce_url( $approve_link, "$action-post_{$post->ID}" ), $post->ID );
}

/**
 * Gets reject post link.
 *
 * @param int $post_id Post ID
 * @return string
 */
function hris_get_reject_post_link( $post_id ) {
	if ( !$post = get_post( $post_id ) )
		return;

	$post_type_object = get_post_type_object( $post->post_type );
	if ( !$post_type_object )
		return;

	// Assuming non 'hris_requestor' always have the permission to approve.
	if ( hris_check_user_role( 'hris_requestor' ) )
		return;

	$action      = 'reject';
	$reject_link = add_query_arg( 'action', $action, admin_url( sprintf( $post_type_object->_edit_link, $post->ID ) ) );

	return apply_filters( 'hris_get_reject_post_link', wp_nonce_url( $reject_link, "$action-post_{$post->ID}" ), $post->ID );
}

/**
 * Gets print post link.
 *
 * @param int $post_id Post ID
 * @return string
 */
function hris_get_print_post_link( $post_id ) {
	return hris_get_action_link_in_post( $post_id, 'print' );
}

function hris_get_action_link_in_post( $post_id, $action ) {
	if ( !$post = get_post( $post_id ) ) {
		return;
	}

	$post_type_object = get_post_type_object( $post->post_type );
	if ( !$post_type_object ) {
		return;
	}

	$action_link = add_query_arg( 'action', $action, admin_url( sprintf( $post_type_object->_edit_link, $post->ID ) ) );

	return apply_filters( "hris_get_{$action}_post_link", wp_nonce_url( $action_link, "$action-post_{$post->ID}" ), $post->ID );
}

/**
 * Gets number of days without holida from $start_date to $end_date.
 *
 * @param  string $start_date
 * @param  string $end_date
 * @return array With following structure:
 *   ```
 *   array(
 *     'total_holidays' => 3,
 *     'number_of_days' => 4, // Number of days from $start_date to $end_date minus total_holidays
 *   )
 *   ```
 */
function hris_get_number_of_days( $start_date, $end_date ) {
		$query = new WP_Query( array(
			'post_type'          => EM_POST_TYPE_EVENT,
			'post_status'        => 'publish',
			'posts_per_page'     => -1,
			EM_TAXONOMY_CATEGORY => 'holiday',
			'meta_query'  => array(
				array(
					'key'     => '_event_start_date',
					'value'   => $start_date,
					'compare' => '>=',
					'type'    => 'DATE',
				),
				array(
					'key'     => '_event_end_date',
					'value'   => $end_date,
					'compare' => '<=',
					'type'    => 'DATE',
				),
			),
		) );

		$total_holidays = 0;
		if ( $query->have_posts() ) {
			$total_holidays = $query->found_posts;
		}

		$start_date_obj = new DateTime( $start_date );
		$end_date_obj   = new DateTime( $end_date );
		$date_diff      = $start_date_obj->diff( $end_date_obj );
		$number_of_days = intval( $date_diff->format('%a') ) - $total_holidays;

		if ( $number_of_days >= 0 )
			$number_of_days += 1;

		return compact( 'total_holidays', 'number_of_days' );
}

/**
 * print_r() convenience function
 *
 * In terminals this will act the same as using print_r() directly, when not run on cli
 * print_r() will wrap <PRE> tags around the output of given array. Similar to debug().
 *
 * @param array $var Variable to print out
 * @return void
 * @link http://book.cakephp.org/2.0/en/core-libraries/global-constants-and-functions.html#pr
 */
function pr( $var ) {
	if ( defined('WP_DEBUG') && WP_DEBUG ) {
		$template = php_sapi_name() !== 'cli' ? '<pre>%s</pre>' : "\n%s\n";
		printf( $template, print_r( $var, true ) );
	}
}
