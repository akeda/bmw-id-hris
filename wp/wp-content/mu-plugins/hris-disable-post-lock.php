<?php

add_filter( 'show_post_locked_dialog', '__return_false' );
add_filter( 'wp_check_post_lock_window', '__return_zero' );