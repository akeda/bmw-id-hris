<?php
// ===================================================
// Load database info and local development parameters
// ===================================================
if ( file_exists( dirname( __FILE__ ) . '/local-config.php' ) ) {
	define( 'WP_LOCAL_DEV', true );
	include( dirname( __FILE__ ) . '/local-config.php' );
} else {

	define( 'WP_LOCAL_DEV', false );

	define( 'DB_NAME',     'bmw_hrd' );
	define( 'DB_USER',     'wp' );
	define( 'DB_PASSWORD', 'wp' );
	define( 'DB_HOST',     'localhost' ); // Probably 'localhost'

	define( 'SAVEQUERIES', false );
	define( 'WP_DEBUG',    false );

	// ===========
	// Hide errors
	// ===========
	ini_set( 'display_errors',  0 );
	define( 'WP_DEBUG_DISPLAY', false );

}

define( 'EMPTY_TRASH_DAYS', 0 );

// ================================================
// You almost certainly do not want to change these
// ================================================
define( 'DB_CHARSET', 'utf8' );
define( 'DB_COLLATE', '' );

// ==============================================================
// Salts, for security
// Grab these from: https://api.wordpress.org/secret-key/1.1/salt
// ==============================================================
define('AUTH_KEY',         'n0b-}UmzfRba48Rhu8F<+&V[/R5dLQ.?E{BbC$G=*!5V?Y(9_[vJzY7:5^fQuz^w');
define('SECURE_AUTH_KEY',  'At^6qXA#e.UI9@!(~,+Q^N<w)3/lJ;NQcTKrz7+VoNKBbD*CF}&+QYcdB`tE0-f+');
define('LOGGED_IN_KEY',    '>y~[zXKh `S^z 3{& q.y0x+P]K~ejMk4Pk3E(n|F*#E%IG~Mgey(u!Q_rT?dVP}');
define('NONCE_KEY',        'D+xmcy0/|K`p|Toefvh>tq*j5Q9leEPX,Y`,m*pBZjE6<mN-+y!X0[+T85W+_$O%');
define('AUTH_SALT',        'Wd+0Wc2dl].gJ]$:OSm~|wdVJK4/}{?;*w[R.GxSqySOuXTu(Z&&HqP2U.b?Rac}');
define('SECURE_AUTH_SALT', 'P}Dd,UTW/|[-|1[[NWSTR-LG}V+1cy-%s>gNfFR~*<]K]G<c!pY(}{m5Ga-[@F?]');
define('LOGGED_IN_SALT',   'rO^g!|24AN&q5+~RAry-rJ`:lKB{`]G7HA[f=Y|FOy;M-d-X1[k6qA:Ko`J{U@I!');
define('NONCE_SALT',       '++60V|^zLED*6i*gjw]-NIe)1YwE!JWJpYSG-kvq+Ih42T3+kWtG;|:.,=DIx)pR');

// ==============================================================
// Table prefix
// Change this if you have multiple installs in the same database
// ==============================================================
$table_prefix  = 'wp_';

// ================================
// Language
// Leave blank for American English
// ================================
define( 'WPLANG', '' );

// =================================================================
// Debug mode
// Debugging? Enable these. Can also enable them in local-config.php
// =================================================================
// define( 'SAVEQUERIES', true );
// define( 'WP_DEBUG', true );

// ======================================
// Load a Memcached config if we have one
// ======================================
if ( file_exists( dirname( __FILE__ ) . '/memcached.php' ) )
	$memcached_servers = include( dirname( __FILE__ ) . '/memcached.php' );

// ===========================================================================================
// This can be used to programatically set the stage when deploying (e.g. production, staging)
// ===========================================================================================
define( 'WP_STAGE', '%%WP_STAGE%%' );
define( 'STAGING_DOMAIN', '%%WP_STAGING_DOMAIN%%' ); // Does magic in WP Stack to handle staging domain rewriting

// ===================
// Bootstrap WordPress
// ===================
if ( !defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/wp/' );
require_once( ABSPATH . 'wp-settings.php' );
