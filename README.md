HRIS (Human Resource Information System)
========================================

HR App, backed with WordPress, for BMW ID. The basic flow in HRIS is:

1. Requestor requests a thing (leave, travel, etc).
2. Request goes to approver (user with role 'HRIS Manager' and/or 'HRIS Approver')
3. Approvers either approves or rejects requsted thing from requestor.

## Install locally

1. Use vvv
2. Put this cloned repo inside vvv's www.
3. Everything should be set up.

## Post Install

1. Make sure roles and capabiliries are set.
2. Follows prerequisite on each module

## Architect

There are four modules, implemented as plugins, used in HRIS:

### HRIS Leave

Implemented as plugin `hris-leave`.

### HRIS Medical

TODO

### HRIS Travel Request

TODO

### HRIS Purchase Request

TODO

## User Credentials

* `hris_admin`:`plokijuh1234`   (adminstrator)
* `test_hris_requestor`:`123`   (HRIS Requestor)
* `test_hris_requestor_2`:`123` (HRIS Requestor)
* `test_hris_manager`:`123`     (HRIS Manager)
* `test_hris_approver`:`123`    (HRIS Approver)

## User Roles and Capabilities

Please note that there four custom roles used by HRIS. Custom roles creation
is provided by members plugin. Please follow roles and capabilties listed below:

* HRIS Requestor

* HRIS Manager

* HRIS Approver

